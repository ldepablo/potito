package com.ldepablo.poker.educa.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.HashSet;

import org.junit.Test;

public class PocketTest {

	private Card card1 = new Card(CardRank.SEVEN, Suit.DIAMONDS);
	private Card card2 = new Card(CardRank.KING, Suit.HEARTS);
	private Card card3 = new Card(card1.getRank(), Suit.CLUBS);
	
	@Test
	public void testGetCard1() {
		Pocket pocket = new Pocket(card1, card2);
		assertEquals(card1, pocket.getCard1());
	}
	
	@Test
	public void testGetCard2() {
		Pocket pocket = new Pocket(card1, card2);
		assertEquals(card2, pocket.getCard2());
	}
	
	@Test
	public void testIsPair() {
		
		Pocket pocket = new Pocket(card1, card2);
		assertFalse(pocket.isPair());


		pocket = new Pocket(card1, card3);
		assertTrue(pocket.isPair());
	}
	
	@Test
	public void testIsSuited() {
		
		Pocket pocket = new Pocket(card1, card2);
		assertFalse(pocket.isSuited());

		Card card3 = new Card(CardRank.QUEEN, card1.getSuit());
		pocket = new Pocket(card1, card3);
		assertTrue(pocket.isSuited());
	}
	
	@Test
	public void testGetCards() {
		Pocket pocket = new Pocket(card1, card2);
		Collection<Card> cards = new HashSet<Card>();
		cards.add(card1);
		cards.add(card2);
		
		assertEquals(pocket.getCards(), cards);
	}
	
	@Test
	public void testGetCardsIsImmutable() {
		Pocket pocket = new Pocket(card1, card2);
		Collection<Card> cards = pocket.getCards();
		cards.add(card3);
		assertEquals(2, pocket.getCards().size());
	}
	
	@Test
	public void testGetPreFlopHand() {
		Pocket pocket = new Pocket(Card.build("Ac"), Card.build("Kc"));
		PreFlopHand preFlopHand = new PreFlopHand(CardRank.ACE, CardRank.KING, true);
		assertEquals(preFlopHand, pocket.getPreFlopHand());
	}
	
	@Test
	public void testIsInList() {
		Pocket pocket = new Pocket(Card.build("Ac"), Card.build("Kc"));
		HandList list = new HandList();
		list.add(new PreFlopHand(pocket));
		assertTrue(pocket.isIn(list));
		
		assertFalse(new Pocket(Card.build("Ah"), Card.build("Jc")).isIn(list));
	}
}
