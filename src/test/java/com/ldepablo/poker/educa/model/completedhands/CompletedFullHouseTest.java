package com.ldepablo.poker.educa.model.completedhands;

import static org.junit.Assert.*;

import org.junit.Test;

import com.ldepablo.poker.educa.model.Board;
import com.ldepablo.poker.educa.model.Card;
import com.ldepablo.poker.educa.model.CardRank;
import com.ldepablo.poker.educa.model.Flop;
import com.ldepablo.poker.educa.model.GenericHandRank;
import com.ldepablo.poker.educa.model.HandCards;
import com.ldepablo.poker.educa.model.Pocket;

public class CompletedFullHouseTest {

	@Test
	public void testAcessFullOfKings() {
		Pocket pocket = new Pocket(Card.build("Ac"), Card.build("Kd"));
		Flop flop = new Flop(Card.build("Ah"), Card.build("As"), Card.build("Kc"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		CompletedFullHouse result = CompletedFullHouse.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.FULL_HOUSE, result.getGenericHandRank());
		assertEquals(CardRank.ACE, result.getFirstRank());		
		assertEquals(CardRank.KING, result.getSecondRank());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertEquals(3, result.getSecondBestPocketPosition().byteValue());
		assertTrue(result.isAtLeastBestFullHouse());
	}
	
	@Test
	public void testReturnsNullWhenNoFullHouse() {
		Pocket pocket = new Pocket(Card.build("Ac"), Card.build("Kd"));
		Flop flop = new Flop(Card.build("Ah"), Card.build("As"), Card.build("Qc"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		CompletedFullHouse result = CompletedFullHouse.build(pocket, handCards.getCards());
		assertNull(result);
	}

	@Test
	public void testKingsFullOfAces() {
		Pocket pocket = new Pocket(Card.build("Ac"), Card.build("Kd"));
		Flop flop = new Flop(Card.build("Ah"), Card.build("Ks"), Card.build("Kc"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		CompletedFullHouse result = CompletedFullHouse.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.FULL_HOUSE, result.getGenericHandRank());
		assertEquals(CardRank.KING, result.getFirstRank());		
		assertEquals(CardRank.ACE, result.getSecondRank());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertEquals(3, result.getSecondBestPocketPosition().byteValue());
		assertFalse(result.isAtLeastBestFullHouse());
	}

	@Test
	public void testKingsFullOfAcesOnlyKingInPocket() {
		Pocket pocket = new Pocket(Card.build("Jc"), Card.build("Kd"));
		Flop flop = new Flop(Card.build("Ah"), Card.build("Ks"), Card.build("Kc"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Ac")));
		CompletedFullHouse result = CompletedFullHouse.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.FULL_HOUSE, result.getGenericHandRank());
		assertEquals(CardRank.KING, result.getFirstRank());		
		assertEquals(CardRank.ACE, result.getSecondRank());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertNull(result.getSecondBestPocketPosition());
		assertFalse(result.isAtLeastBestFullHouse());
	}

	@Test
	public void testJacksFullOfEightsAndPocketJackAndSeven() {
		Pocket pocket = new Pocket(Card.build("Jc"), Card.build("7d"));
		Flop flop = new Flop(Card.build("Jh"), Card.build("Js"), Card.build("8c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("8d")));
		CompletedFullHouse result = CompletedFullHouse.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.FULL_HOUSE, result.getGenericHandRank());
		assertEquals(CardRank.JACK, result.getFirstRank());		
		assertEquals(CardRank.EIGHT, result.getSecondRank());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertNull(result.getSecondBestPocketPosition());
		assertTrue(result.isAtLeastBestFullHouse());
	}

	@Test
	public void testJacksFullOfEightsAndPocketJackAndAce() {
		Pocket pocket = new Pocket(Card.build("Jc"), Card.build("Ad"));
		Flop flop = new Flop(Card.build("Jh"), Card.build("Js"), Card.build("8c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("8d")));
		CompletedFullHouse result = CompletedFullHouse.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.FULL_HOUSE, result.getGenericHandRank());
		assertEquals(CardRank.JACK, result.getFirstRank());		
		assertEquals(CardRank.EIGHT, result.getSecondRank());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertNull(result.getSecondBestPocketPosition());
		assertTrue(result.isAtLeastBestFullHouse());
	}

	@Test
	public void testJacksFullOfEightsAndPocketJackAndUselessEight() {
		Pocket pocket = new Pocket(Card.build("Jc"), Card.build("8d"));
		Flop flop = new Flop(Card.build("Jh"), Card.build("Js"), Card.build("8c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("8h")));
		CompletedFullHouse result = CompletedFullHouse.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.FULL_HOUSE, result.getGenericHandRank());
		assertEquals(CardRank.JACK, result.getFirstRank());		
		assertEquals(CardRank.EIGHT, result.getSecondRank());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertNull(result.getSecondBestPocketPosition());
		assertTrue(result.isAtLeastBestFullHouse());
	}

	@Test
	public void testJacksFullOfEightsAndPocketKingAndEight() {
		Pocket pocket = new Pocket(Card.build("Kc"), Card.build("8d"));
		Flop flop = new Flop(Card.build("Jh"), Card.build("Js"), Card.build("8c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Jd")));
		CompletedFullHouse result = CompletedFullHouse.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.FULL_HOUSE, result.getGenericHandRank());
		assertEquals(CardRank.JACK, result.getFirstRank());		
		assertEquals(CardRank.EIGHT, result.getSecondRank());
		assertEquals(3, result.getBestPocketPosition().byteValue());
		assertNull(result.getSecondBestPocketPosition());
		assertTrue(result.isAtLeastBestFullHouse());
	}

	@Test
	public void testJacksFullOfEightsAndPocketKingAndUselessEight() {
		Pocket pocket = new Pocket(Card.build("Kc"), Card.build("8d"));
		Flop flop = new Flop(Card.build("Jh"), Card.build("Js"), Card.build("8c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Jd"), Card.build("8s")));
		CompletedFullHouse result = CompletedFullHouse.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.FULL_HOUSE, result.getGenericHandRank());
		assertEquals(CardRank.JACK, result.getFirstRank());		
		assertEquals(CardRank.EIGHT, result.getSecondRank());
		assertNull(result.getBestPocketPosition());
		assertNull(result.getSecondBestPocketPosition());
		assertTrue(result.isAtLeastBestFullHouse());
	}

	@Test
	public void testJacksFullOfEightsAndPocketPairEightsAndPAirSevensOnBoard() {
		Pocket pocket = new Pocket(Card.build("8c"), Card.build("8d"));
		Flop flop = new Flop(Card.build("Jh"), Card.build("Js"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Jd"), Card.build("7s")));
		CompletedFullHouse result = CompletedFullHouse.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.FULL_HOUSE, result.getGenericHandRank());
		assertEquals(CardRank.JACK, result.getFirstRank());		
		assertEquals(CardRank.EIGHT, result.getSecondRank());
		assertEquals(3, result.getBestPocketPosition().byteValue());
		assertEquals(3, result.getSecondBestPocketPosition().byteValue());
		assertTrue(result.isAtLeastBestFullHouse());
	}

	@Test
	public void testCompareToSuperiorHand() {
		Pocket pocket = new Pocket(Card.build("8c"), Card.build("8d"));
		Flop flop = new Flop(Card.build("Jh"), Card.build("Js"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Jd")));
		CompletedFullHouse result = CompletedFullHouse.build(pocket, handCards.getCards());
		

		Pocket pocket2 = new Pocket(Card.build("2c"), Card.build("Ad"));
		Flop flop2 = new Flop(Card.build("Ah"), Card.build("As"), Card.build("Ac"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("Qh")));
		CompletedFourOfAKind superior = CompletedFourOfAKind.build(pocket2, handCards2.getCards());
		assertTrue(result.compareTo(superior) <0);
	}

	@Test
	public void testCompareToInferiorHand() {
		Pocket pocket = new Pocket(Card.build("8c"), Card.build("8d"));
		Flop flop = new Flop(Card.build("Jh"), Card.build("Js"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Jd")));
		CompletedFullHouse result = CompletedFullHouse.build(pocket, handCards.getCards());
		
		Pocket pocket1 = new Pocket(Card.build("Ac"), Card.build("Kc"));
		Flop flop1 = new Flop(Card.build("Qc"), Card.build("Jc"), Card.build("Ts"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("9c"), Card.build("8s")));
		CompletedFlush inferior = CompletedFlush.build(pocket1, handCards1.getCards());
		assertTrue(result.compareTo(inferior)>0);
	}

	@Test
	public void testCompareToSuperiorFirstRank() {
		Pocket pocket1 = new Pocket(Card.build("8c"), Card.build("8d"));
		Flop flop1 = new Flop(Card.build("Jh"), Card.build("Js"), Card.build("7c"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("Jd")));
		CompletedFullHouse result1 = CompletedFullHouse.build(pocket1, handCards1.getCards());
		
		Pocket pocket2 = new Pocket(Card.build("8c"), Card.build("8d"));
		Flop flop2 = new Flop(Card.build("Ah"), Card.build("As"), Card.build("7c"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("Ad")));
		CompletedFullHouse result2 = CompletedFullHouse.build(pocket2, handCards2.getCards());
		
		assertTrue(result1.compareTo(result2) < 0);
	}

	@Test
	public void testCompareToInferiorFirstRank() {
		Pocket pocket1 = new Pocket(Card.build("8c"), Card.build("8d"));
		Flop flop1 = new Flop(Card.build("Jh"), Card.build("Js"), Card.build("7c"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("Jd")));
		CompletedFullHouse result1 = CompletedFullHouse.build(pocket1, handCards1.getCards());
		
		Pocket pocket2 = new Pocket(Card.build("8c"), Card.build("8d"));
		Flop flop2 = new Flop(Card.build("Ah"), Card.build("As"), Card.build("7c"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("Ad")));
		CompletedFullHouse result2 = CompletedFullHouse.build(pocket2, handCards2.getCards());
		
		assertTrue(result2.compareTo(result1) > 0);
	}

	@Test
	public void testCompareToSuperiorSecondRank() {
		Pocket pocket1 = new Pocket(Card.build("8c"), Card.build("8d"));
		Flop flop1 = new Flop(Card.build("Jh"), Card.build("Js"), Card.build("7c"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("Jd")));
		CompletedFullHouse result1 = CompletedFullHouse.build(pocket1, handCards1.getCards());
		
		Pocket pocket2 = new Pocket(Card.build("9c"), Card.build("9d"));
		Flop flop2 = new Flop(Card.build("Jh"), Card.build("Js"), Card.build("7c"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("Jd")));
		CompletedFullHouse result2 = CompletedFullHouse.build(pocket2, handCards2.getCards());
		
		assertTrue(result1.compareTo(result2) < 0);
	}

	@Test
	public void testCompareToInferiorSecondRank() {
		Pocket pocket1 = new Pocket(Card.build("8c"), Card.build("8d"));
		Flop flop1 = new Flop(Card.build("Jh"), Card.build("Js"), Card.build("7c"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("Jd")));
		CompletedFullHouse result1 = CompletedFullHouse.build(pocket1, handCards1.getCards());
		
		Pocket pocket2 = new Pocket(Card.build("9c"), Card.build("9d"));
		Flop flop2 = new Flop(Card.build("Jh"), Card.build("Js"), Card.build("7c"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("Jd")));
		CompletedFullHouse result2 = CompletedFullHouse.build(pocket2, handCards2.getCards());
		
		assertTrue(result2.compareTo(result1) > 0);
	}

	@Test
	public void testCompareToSameRanksDifferentSixthCard() {
		Pocket pocket1 = new Pocket(Card.build("8c"), Card.build("8d"));
		Flop flop1 = new Flop(Card.build("Jh"), Card.build("Js"), Card.build("2c"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("Jd")));
		CompletedFullHouse result1 = CompletedFullHouse.build(pocket1, handCards1.getCards());
		
		Pocket pocket2 = new Pocket(Card.build("8c"), Card.build("8d"));
		Flop flop2 = new Flop(Card.build("Jh"), Card.build("Js"), Card.build("7c"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("Jd")));
		CompletedFullHouse result2 = CompletedFullHouse.build(pocket2, handCards2.getCards());
		
		assertEquals(0, result2.compareTo(result1));
	}
}
