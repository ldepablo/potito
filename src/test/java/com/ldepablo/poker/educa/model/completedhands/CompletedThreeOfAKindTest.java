package com.ldepablo.poker.educa.model.completedhands;

import static org.junit.Assert.*;

import org.junit.Test;

import com.ldepablo.poker.educa.model.Board;
import com.ldepablo.poker.educa.model.Card;
import com.ldepablo.poker.educa.model.CardRank;
import com.ldepablo.poker.educa.model.Flop;
import com.ldepablo.poker.educa.model.GenericHandRank;
import com.ldepablo.poker.educa.model.HandCards;
import com.ldepablo.poker.educa.model.Pocket;

public class CompletedThreeOfAKindTest {

	@Test
	public void testNoPossibleTOAKReturnsNull() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Qd"));
		Flop flop = new Flop(Card.build("Tc"), Card.build("Ah"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		assertNull(CompletedThreeOfAKind.build(pocket, handCards.getCards()));
	}
	
	@Test
	public void testTOAK_PocketAcesPlusAceOnFlop() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Ad"));
		Flop flop = new Flop(Card.build("Tc"), Card.build("Ah"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		CompletedThreeOfAKind result = CompletedThreeOfAKind.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.THREE_OF_A_KIND, result.getGenericHandRank());
		assertEquals(CardRank.ACE, result.getRank());
		assertEquals(CardRank.TEN, result.getKicker1());
		assertEquals(CardRank.SEVEN, result.getKicker2());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertEquals(0, result.getSecondBestPocketPosition().byteValue());
	}
	
	@Test
	public void testTOAK_PocketAcePlusTwoAcesOnFlop() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Qd"));
		Flop flop = new Flop(Card.build("Ac"), Card.build("Ah"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		CompletedThreeOfAKind result = CompletedThreeOfAKind.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.THREE_OF_A_KIND, result.getGenericHandRank());
		assertEquals(CardRank.ACE, result.getRank());
		assertEquals(CardRank.QUEEN, result.getKicker1());
		assertEquals(CardRank.SEVEN, result.getKicker2());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertEquals(3, result.getSecondBestPocketPosition().byteValue());
	}
	
	@Test
	public void testTOAK_PocketKingsPlusKingOnFlop() {
		Pocket pocket = new Pocket(Card.build("Ks"), Card.build("Kd"));
		Flop flop = new Flop(Card.build("Ac"), Card.build("Kh"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		CompletedThreeOfAKind result = CompletedThreeOfAKind.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.THREE_OF_A_KIND, result.getGenericHandRank());
		assertEquals(CardRank.KING, result.getRank());
		assertEquals(CardRank.ACE, result.getKicker1());
		assertEquals(CardRank.SEVEN, result.getKicker2());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertEquals(0, result.getSecondBestPocketPosition().byteValue());
	}
	
	@Test
	public void testTOAK_TwoUsefulPocketCardsOneInSetOneNotButKicker() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Kd"));
		Flop flop = new Flop(Card.build("Kc"), Card.build("Kh"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		CompletedThreeOfAKind result = CompletedThreeOfAKind.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.THREE_OF_A_KIND, result.getGenericHandRank());
		assertEquals(CardRank.KING, result.getRank());
		assertEquals(CardRank.ACE, result.getKicker1());
		assertEquals(CardRank.SEVEN, result.getKicker2());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertEquals(3, result.getSecondBestPocketPosition().byteValue());
	}
	
	@Test
	public void testTOAK_TwoUsefulPocketCardsOneInSetOneNotButSecondKicker() {
		Pocket pocket = new Pocket(Card.build("7s"), Card.build("Kd"));
		Flop flop = new Flop(Card.build("Kc"), Card.build("Kh"), Card.build("Ac"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		CompletedThreeOfAKind result = CompletedThreeOfAKind.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.THREE_OF_A_KIND, result.getGenericHandRank());
		assertEquals(CardRank.KING, result.getRank());
		assertEquals(CardRank.ACE, result.getKicker1());
		assertEquals(CardRank.SEVEN, result.getKicker2());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertEquals(4, result.getSecondBestPocketPosition().byteValue());
	}
	
	@Test
	public void testTOAK_OneUsefulPocketCardInSetOneNot() {
		Pocket pocket = new Pocket(Card.build("3s"), Card.build("Kd"));
		Flop flop = new Flop(Card.build("Kc"), Card.build("Kh"), Card.build("Ac"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Qc")));
		CompletedThreeOfAKind result = CompletedThreeOfAKind.build(pocket, handCards.getCards());
	
		assertNotNull(result);
		assertEquals(GenericHandRank.THREE_OF_A_KIND, result.getGenericHandRank());
		assertEquals(CardRank.KING, result.getRank());
		assertEquals(CardRank.ACE, result.getKicker1());
		assertEquals(CardRank.QUEEN, result.getKicker2());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertNull(result.getSecondBestPocketPosition());
	}
	
	@Test
	public void testTOAK_OneUsefulPocketCardOutOfSetOneNot() {
		Pocket pocket = new Pocket(Card.build("3s"), Card.build("Kd"));
		Flop flop = new Flop(Card.build("As"), Card.build("Ah"), Card.build("Ac"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Qc")));
		CompletedThreeOfAKind result = CompletedThreeOfAKind.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.THREE_OF_A_KIND, result.getGenericHandRank());
		assertEquals(CardRank.ACE, result.getRank());
		assertEquals(CardRank.KING, result.getKicker1());
		assertEquals(CardRank.QUEEN, result.getKicker2());
		assertEquals(3, result.getBestPocketPosition().byteValue());
		assertNull(result.getSecondBestPocketPosition());
	}
	
	@Test
	public void testTOAK_SetOfTwos_UselessPocket() {
		Pocket pocket = new Pocket(Card.build("3s"), Card.build("5d"));
		Flop flop = new Flop(Card.build("2s"), Card.build("2h"), Card.build("2c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Qc"), Card.build("Jc")));
		CompletedThreeOfAKind result = CompletedThreeOfAKind.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.THREE_OF_A_KIND, result.getGenericHandRank());
		assertEquals(CardRank.TWO, result.getRank());
		assertEquals(CardRank.QUEEN, result.getKicker1());
		assertEquals(CardRank.JACK, result.getKicker2());
		assertNull(result.getBestPocketPosition());
		assertNull(result.getSecondBestPocketPosition());
	}
	
	@Test
	public void testTOAK_SetOfTwos_OneUsefulPocket() {
		Pocket pocket = new Pocket(Card.build("3s"), Card.build("Ad"));
		Flop flop = new Flop(Card.build("2s"), Card.build("2h"), Card.build("2c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Qc"), Card.build("Jc")));
		CompletedThreeOfAKind result = CompletedThreeOfAKind.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.THREE_OF_A_KIND, result.getGenericHandRank());
		assertEquals(CardRank.TWO, result.getRank());
		assertEquals(CardRank.ACE, result.getKicker1());
		assertEquals(CardRank.QUEEN, result.getKicker2());
		assertEquals(3, result.getBestPocketPosition().byteValue());
		assertNull(result.getSecondBestPocketPosition());
	}

	@Test
	public void testCompareWithSuperiorHand() {
		Pocket pocket1 = new Pocket(Card.build("As"), Card.build("2d"));
		Flop flop1 = new Flop(Card.build("9c"), Card.build("Jh"), Card.build("Tc"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("Qd"), Card.build("Kh")));
		CompletedStraight straight = CompletedStraight.build(pocket1, handCards1.getCards());

		Pocket pocket = new Pocket(Card.build("3s"), Card.build("Ad"));
		Flop flop = new Flop(Card.build("2s"), Card.build("2h"), Card.build("2c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Qc"), Card.build("Jc")));
		CompletedThreeOfAKind toak = CompletedThreeOfAKind.build(pocket, handCards.getCards());

		assertTrue(toak.compareTo(straight) < 0);
	}

	@Test
	public void testCompareWithInferiorHand() {
		Pocket pocket1 = new Pocket(Card.build("As"), Card.build("Ad"));
		Flop flop1 = new Flop(Card.build("Kc"), Card.build("Kh"), Card.build("7c"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1));
		CompletedTwoPair twoPair = CompletedTwoPair.build(pocket1, handCards1.getCards());

		Pocket pocket = new Pocket(Card.build("3s"), Card.build("Ad"));
		Flop flop = new Flop(Card.build("2s"), Card.build("2h"), Card.build("2c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Qc"), Card.build("Jc")));
		CompletedThreeOfAKind toak = CompletedThreeOfAKind.build(pocket, handCards.getCards());

		assertTrue(toak.compareTo(twoPair) > 0);
	}
	
	@Test
	public void testCompareByRank() {
		Pocket pocket1 = new Pocket(Card.build("3s"), Card.build("Ad"));
		Flop flop1 = new Flop(Card.build("2s"), Card.build("2h"), Card.build("2c"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("Qc"), Card.build("Jc")));
		CompletedThreeOfAKind toak1 = CompletedThreeOfAKind.build(pocket1, handCards1.getCards());
		

		Pocket pocket = new Pocket(Card.build("3s"), Card.build("Ad"));
		Flop flop = new Flop(Card.build("5s"), Card.build("5h"), Card.build("5c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Qc"), Card.build("Jc")));
		CompletedThreeOfAKind toak2 = CompletedThreeOfAKind.build(pocket, handCards.getCards());

		assertTrue(toak1.compareTo(toak2) < 0);
		assertTrue(toak2.compareTo(toak1) > 0);
	}
	
	@Test
	public void testCompareByKicker1() {
		Pocket pocket1 = new Pocket(Card.build("3s"), Card.build("Ad"));
		Flop flop1 = new Flop(Card.build("2s"), Card.build("2h"), Card.build("2c"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("Qc"), Card.build("Jc")));
		CompletedThreeOfAKind toak1 = CompletedThreeOfAKind.build(pocket1, handCards1.getCards());
		

		Pocket pocket = new Pocket(Card.build("3s"), Card.build("Kd"));
		Flop flop = new Flop(Card.build("2s"), Card.build("2h"), Card.build("2c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Qc"), Card.build("Jc")));
		CompletedThreeOfAKind toak2 = CompletedThreeOfAKind.build(pocket, handCards.getCards());

		assertTrue(toak1.compareTo(toak2) > 0);
		assertTrue(toak2.compareTo(toak1) < 0);
	}
	
	@Test
	public void testCompareByKicker2() {
		Pocket pocket1 = new Pocket(Card.build("3s"), Card.build("Ad"));
		Flop flop1 = new Flop(Card.build("2s"), Card.build("2h"), Card.build("2c"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("Kc"), Card.build("Jc")));
		CompletedThreeOfAKind toak1 = CompletedThreeOfAKind.build(pocket1, handCards1.getCards());
		

		Pocket pocket = new Pocket(Card.build("3s"), Card.build("Ad"));
		Flop flop = new Flop(Card.build("2s"), Card.build("2h"), Card.build("2c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Qc"), Card.build("Jc")));
		CompletedThreeOfAKind toak2 = CompletedThreeOfAKind.build(pocket, handCards.getCards());

		assertTrue(toak1.compareTo(toak2) > 0);
		assertTrue(toak2.compareTo(toak1) < 0);
	}
	
	@Test
	public void testCompareNotAffectedByUselessCards() {
		Pocket pocket1 = new Pocket(Card.build("3s"), Card.build("Ad"));
		Flop flop1 = new Flop(Card.build("2s"), Card.build("2h"), Card.build("2c"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("Qc"), Card.build("7c")));
		CompletedThreeOfAKind toak1 = CompletedThreeOfAKind.build(pocket1, handCards1.getCards());
		

		Pocket pocket = new Pocket(Card.build("3s"), Card.build("Ad"));
		Flop flop = new Flop(Card.build("2s"), Card.build("2h"), Card.build("2c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Qc"), Card.build("Jc")));
		CompletedThreeOfAKind toak2 = CompletedThreeOfAKind.build(pocket, handCards.getCards());

		assertEquals(0, toak1.compareTo(toak2));
	}
}
