package com.ldepablo.poker.educa.model.outs;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import org.junit.Test;

import com.ldepablo.poker.educa.model.Board;
import com.ldepablo.poker.educa.model.BoardTexture;
import com.ldepablo.poker.educa.model.Card;
import com.ldepablo.poker.educa.model.Flop;
import com.ldepablo.poker.educa.model.Pocket;

public class OvercardsOutsCalculatorTest {

	private static final double DELTA = 1e-15;

	
/**
 * OVERCARDS
 */
	
	@Test
	public void testGetOutsFor_Overcards_Dry_NonPaired() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("2h"));
		Flop flop = new Flop(Card.build("Kd"), Card.build("8d"), Card.build("3c"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.DRY, board.getTexture());
		
		OvercardsOutsCalculator calculator = new OvercardsOutsCalculator(pocket, board);
		assertEquals(2, calculator.calculate(), DELTA);

		pocket = new Pocket(Card.build("As"), Card.build("Kh"));
		flop = new Flop(Card.build("5d"), Card.build("8d"), Card.build("3c"));
		board = new Board(flop);
		assertEquals(BoardTexture.DRY, board.getTexture());
		
		calculator = new OvercardsOutsCalculator(pocket, board);
		assertEquals(4, calculator.calculate(), DELTA);
		
	}


	
	@Test
	public void testGetOutsFor_Overcards_Semi_NonPaired() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("2h"));
		Flop flop = new Flop(Card.build("Kd"), Card.build("Jd"), Card.build("3c"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.SEMICOORDINATED, board.getTexture());
		
		OvercardsOutsCalculator calculator = new OvercardsOutsCalculator(pocket, board);
		assertEquals(2, calculator.calculate(), DELTA);

		pocket = new Pocket(Card.build("As"), Card.build("Kh"));
		flop = new Flop(Card.build("7d"), Card.build("9d"), Card.build("3c"));
		board = new Board(flop);
		assertEquals(BoardTexture.SEMICOORDINATED, board.getTexture());
		
		calculator = new OvercardsOutsCalculator(pocket, board);
		assertEquals(4, calculator.calculate(), DELTA);
		
	}

	
	@Test
	public void testGetOutsFor_Overcards_Coordinated_NonPaired() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("2h"));
		Flop flop = new Flop(Card.build("Kd"), Card.build("8d"), Card.build("3d"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		
		OvercardsOutsCalculator calculator = new OvercardsOutsCalculator(pocket, board);
		assertEquals(1, calculator.calculate(), DELTA);

		pocket = new Pocket(Card.build("As"), Card.build("Kh"));
		flop = new Flop(Card.build("5d"), Card.build("8d"), Card.build("3d"));
		board = new Board(flop);
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		
		calculator = new OvercardsOutsCalculator(pocket, board);
		assertEquals(2, calculator.calculate(), DELTA);
	}

	
	@Test
	public void testGetOutsFor_Overcards_ExtremelyCoordinatedBoard_NonPaired() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("2h"));
		Flop flop = new Flop(Card.build("Kd"), Card.build("8d"), Card.build("3d"));
		Board board = new Board(flop, Card.build("7d"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		
		OvercardsOutsCalculator calculator = new OvercardsOutsCalculator(pocket, board);
		assertEquals(0, calculator.calculate(), DELTA);

		pocket = new Pocket(Card.build("As"), Card.build("Kh"));
		flop = new Flop(Card.build("5d"), Card.build("8d"), Card.build("3d"));
		board = new Board(flop, Card.build("7d"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		
		calculator = new OvercardsOutsCalculator(pocket, board);
		assertEquals(0, calculator.calculate(), DELTA);
	}

	
	@Test
	public void testGetOutsFor_Overcards_DryBoard_HighPair() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("2h"));
		Flop flop = new Flop(Card.build("Kd"), Card.build("8d"), Card.build("8c"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.DRY, board.getTexture());
		
		OvercardsOutsCalculator calculator = new OvercardsOutsCalculator(pocket, board);
		assertEquals(1, calculator.calculate(), DELTA);

		pocket = new Pocket(Card.build("As"), Card.build("Kh"));
		flop = new Flop(Card.build("5d"), Card.build("9d"), Card.build("9c"));
		board = new Board(flop);
		assertEquals(BoardTexture.DRY, board.getTexture());
		
		calculator = new OvercardsOutsCalculator(pocket, board);
		assertEquals(2, calculator.calculate(), DELTA);
		
	}

	
	@Test
	public void testGetOutsFor_Overcards_SemiBoard_HighPair() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("2h"));
		Flop flop = new Flop(Card.build("Kd"), Card.build("8d"), Card.build("8c"));
		Board board = new Board(flop, Card.build("Jh"));
		assertEquals(BoardTexture.SEMICOORDINATED, board.getTexture());
		
		OvercardsOutsCalculator calculator = new OvercardsOutsCalculator(pocket, board);
		assertEquals(1, calculator.calculate(), DELTA);

		pocket = new Pocket(Card.build("As"), Card.build("Kh"));
		flop = new Flop(Card.build("5d"), Card.build("9d"), Card.build("9c"));
		board = new Board(flop, Card.build("Jh"));
		assertEquals(BoardTexture.SEMICOORDINATED, board.getTexture());
		
		calculator = new OvercardsOutsCalculator(pocket, board);
		assertEquals(2, calculator.calculate(), DELTA);
		
	}

	
	@Test
	public void testGetOutsFor_Overcards_CoordinatedBoard_HighPair() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("2h"));
		Flop flop = new Flop(Card.build("Kd"), Card.build("8d"), Card.build("8c"));
		Board board = new Board(flop, Card.build("6d"));
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		
		OvercardsOutsCalculator calculator = new OvercardsOutsCalculator(pocket, board);
		assertEquals(0, calculator.calculate(), DELTA);

		pocket = new Pocket(Card.build("As"), Card.build("Kh"));
		flop = new Flop(Card.build("5d"), Card.build("9d"), Card.build("9c"));
		board = new Board(flop, Card.build("2d"));
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		
		calculator = new OvercardsOutsCalculator(pocket, board);
		assertEquals(0, calculator.calculate(), DELTA);
		
	}

	
	@Test
	public void testGetOutsFor_Overcards_DryBoard_LowPair() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("2h"));
		Flop flop = new Flop(Card.build("Kd"), Card.build("7d"), Card.build("7c"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.DRY, board.getTexture());
		
		OvercardsOutsCalculator calculator = new OvercardsOutsCalculator(pocket, board);
		assertEquals(2, calculator.calculate(), DELTA);

		pocket = new Pocket(Card.build("As"), Card.build("Kh"));
		flop = new Flop(Card.build("5d"), Card.build("6d"), Card.build("6c"));
		board = new Board(flop);
		assertEquals(BoardTexture.DRY, board.getTexture());
		
		calculator = new OvercardsOutsCalculator(pocket, board);
		assertEquals(4, calculator.calculate(), DELTA);
	}

	
	@Test
	public void testGetOutsFor_Overcards_SemiBoard_LowPair() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("2h"));
		Flop flop = new Flop(Card.build("8d"), Card.build("7d"), Card.build("7c"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.SEMICOORDINATED, board.getTexture());
		
		OvercardsOutsCalculator calculator = new OvercardsOutsCalculator(pocket, board);
		assertEquals(2, calculator.calculate(), DELTA);

		pocket = new Pocket(Card.build("As"), Card.build("Kh"));
		flop = new Flop(Card.build("7d"), Card.build("6d"), Card.build("6c"));
		board = new Board(flop);
		assertEquals(BoardTexture.SEMICOORDINATED, board.getTexture());
		
		calculator = new OvercardsOutsCalculator(pocket, board);
		assertEquals(4, calculator.calculate(), DELTA);
	}

	
	@Test
	public void testGetOutsFor_Overcards_CoordinatedBoard_LowPair() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("2h"));
		Flop flop = new Flop(Card.build("Kd"), Card.build("7d"), Card.build("7c"));
		Board board = new Board(flop, Card.build("6d"));
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		
		OvercardsOutsCalculator calculator = new OvercardsOutsCalculator(pocket, board);
		assertEquals(1, calculator.calculate(), DELTA);

		pocket = new Pocket(Card.build("As"), Card.build("Kh"));
		flop = new Flop(Card.build("5d"), Card.build("6d"), Card.build("6c"));
		board = new Board(flop, Card.build("2d"));
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		
		calculator = new OvercardsOutsCalculator(pocket, board);
		assertEquals(2, calculator.calculate(), DELTA);
	}

	@Test
	public void testGetOutsFor_SemiOvercards_DryBoard_NoPair() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("2h"));
		Flop flop = new Flop(Card.build("Kd"), Card.build("4d"), Card.build("8c"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.DRY, board.getTexture());

		OvercardsOutsCalculator calculator = new OvercardsOutsCalculator(pocket, board);
		assertEquals(2, calculator.calculate(), DELTA);
		
		pocket = new Pocket(Card.build("As"), Card.build("Qh"));
		assertEquals(BoardTexture.DRY, board.getTexture());

		calculator = new OvercardsOutsCalculator(pocket, board);
		assertEquals(2.5, calculator.calculate(), DELTA);
		
	}

	@Test
	public void testGetOutsFor_SemiOvercards_NonDryBoard_NoPair() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Qh"));
		Flop flop = new Flop(Card.build("Kd"), Card.build("7d"), Card.build("8c"));
		Board board = new Board(flop);
		assertNotSame(BoardTexture.DRY, board.getTexture());

		OvercardsOutsCalculator calculator = new OvercardsOutsCalculator(pocket, board);
		assertEquals(2, calculator.calculate(), DELTA);
	}

	@Test
	public void testGetOutsFor_SemiOvercards_DryBoard_NoPair_TwoInPocket() {
		Pocket pocket = new Pocket(Card.build("Js"), Card.build("Qh"));
		Flop flop = new Flop(Card.build("Kd"), Card.build("3d"), Card.build("8c"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.DRY, board.getTexture());

		OvercardsOutsCalculator calculator = new OvercardsOutsCalculator(pocket, board);
		assertEquals(1, calculator.calculate(), DELTA);
	}

	@Test
	public void testGetOutsFor_SemiOvercards_NonDryBoard_NoPair_TwoInPocket() {
		Pocket pocket = new Pocket(Card.build("Js"), Card.build("Qh"));
		Flop flop = new Flop(Card.build("Kd"), Card.build("7d"), Card.build("8c"));
		Board board = new Board(flop);
		assertNotSame(BoardTexture.DRY, board.getTexture());

		OvercardsOutsCalculator calculator = new OvercardsOutsCalculator(pocket, board);
		assertEquals(0, calculator.calculate(), DELTA);
	}
	
	//FIXE we need more tests for this, at least for semi and dry cases for half outs

}
