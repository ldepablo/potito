package com.ldepablo.poker.educa.model.completedhands;

import static org.junit.Assert.*;

import org.junit.Test;

import com.ldepablo.poker.educa.model.Board;
import com.ldepablo.poker.educa.model.Card;
import com.ldepablo.poker.educa.model.CardRank;
import com.ldepablo.poker.educa.model.Flop;
import com.ldepablo.poker.educa.model.GenericHandRank;
import com.ldepablo.poker.educa.model.HandCards;
import com.ldepablo.poker.educa.model.Pocket;

public class CompletedTwoPairTest {

	@Test
	public void testNoPossibleTwoPaitReturnsNull() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Qd"));
		Flop flop = new Flop(Card.build("Tc"), Card.build("Ah"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		assertNull(CompletedTwoPair.build(pocket, handCards.getCards()));
	}
	
	@Test
	public void testTwoPair_PocketAcesPlusKingsOnFlop() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Ad"));
		Flop flop = new Flop(Card.build("Kc"), Card.build("Kh"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		CompletedTwoPair result = CompletedTwoPair.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.TWO_PAIR, result.getGenericHandRank());
		assertEquals(CardRank.ACE, result.getTopRank());
		assertEquals(CardRank.KING, result.getSecondRank());
		assertEquals(CardRank.SEVEN, result.getKickerRank());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertEquals(0, result.getSecondBestPocketPosition().byteValue());
		assertTrue(result.isAtLeastBestTwoPair());
		
	}
	
	@Test
	public void testTwoPair_PocketKingsPlusAcesOnFlop() {
		Pocket pocket = new Pocket(Card.build("Ks"), Card.build("Kd"));
		Flop flop = new Flop(Card.build("Ac"), Card.build("Ah"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		CompletedTwoPair result = CompletedTwoPair.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.TWO_PAIR, result.getGenericHandRank());
		assertEquals(CardRank.ACE, result.getTopRank());
		assertEquals(CardRank.KING, result.getSecondRank());
		assertEquals(CardRank.SEVEN, result.getKickerRank());
		assertEquals(2, result.getBestPocketPosition().byteValue());
		assertEquals(2, result.getSecondBestPocketPosition().byteValue());
		assertTrue(result.isAtLeastBestTwoPair());
	}
	
	@Test
	public void testTwoPair_PocketAndFlopAceKing() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Kd"));
		Flop flop = new Flop(Card.build("Kc"), Card.build("Ah"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		CompletedTwoPair result = CompletedTwoPair.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.TWO_PAIR, result.getGenericHandRank());
		assertEquals(CardRank.ACE, result.getTopRank());
		assertEquals(CardRank.KING, result.getSecondRank());
		assertEquals(CardRank.SEVEN, result.getKickerRank());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertEquals(2, result.getSecondBestPocketPosition().byteValue());
		assertTrue(result.isAtLeastBestTwoPair());
	}
	
	@Test
	public void testTwoPair_AceSevenPocket_AKKFlop() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("7d"));
		Flop flop = new Flop(Card.build("Kc"), Card.build("Ah"), Card.build("Kd"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		CompletedTwoPair result = CompletedTwoPair.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.TWO_PAIR, result.getGenericHandRank());
		assertEquals(CardRank.ACE, result.getTopRank());
		assertEquals(CardRank.KING, result.getSecondRank());
		assertEquals(CardRank.SEVEN, result.getKickerRank());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertEquals(4, result.getSecondBestPocketPosition().byteValue());
		assertTrue(result.isAtLeastBestTwoPair());
	}
	
	@Test
	public void testTwoPair_PocketPairSeven_8KKFlop() {
		Pocket pocket = new Pocket(Card.build("7s"), Card.build("7d"));
		Flop flop = new Flop(Card.build("Kc"), Card.build("8h"), Card.build("Kd"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		CompletedTwoPair result = CompletedTwoPair.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.TWO_PAIR, result.getGenericHandRank());
		assertEquals(CardRank.KING, result.getTopRank());
		assertEquals(CardRank.SEVEN, result.getSecondRank());
		assertEquals(CardRank.EIGHT, result.getKickerRank());
		assertEquals(2, result.getBestPocketPosition().byteValue());
		assertEquals(2, result.getSecondBestPocketPosition().byteValue());
		assertFalse(result.isAtLeastBestTwoPair());
	}
	
	@Test
	public void testTwoPair_TopAndUselessInPocket() {
		Pocket pocket = new Pocket(Card.build("Js"), Card.build("8d"));
		Flop flop = new Flop(Card.build("8c"), Card.build("7h"), Card.build("7d"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Qs")));
		CompletedTwoPair result = CompletedTwoPair.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.TWO_PAIR, result.getGenericHandRank());
		assertEquals(CardRank.EIGHT, result.getTopRank());
		assertEquals(CardRank.SEVEN, result.getSecondRank());
		assertEquals(CardRank.QUEEN, result.getKickerRank());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertNull(result.getSecondBestPocketPosition());
		assertFalse(result.isAtLeastBestTwoPair());
	}
	
	@Test
	public void testTwoPair_SecondAndUselessInPocket() {
		Pocket pocket = new Pocket(Card.build("Js"), Card.build("7d"));
		Flop flop = new Flop(Card.build("8c"), Card.build("8h"), Card.build("7s"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Qs")));
		CompletedTwoPair result = CompletedTwoPair.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.TWO_PAIR, result.getGenericHandRank());
		assertEquals(CardRank.EIGHT, result.getTopRank());
		assertEquals(CardRank.SEVEN, result.getSecondRank());
		assertEquals(CardRank.QUEEN, result.getKickerRank());
		assertEquals(2, result.getBestPocketPosition().byteValue());
		assertNull(result.getSecondBestPocketPosition());
		assertFalse(result.isAtLeastBestTwoPair());
	}
	
	@Test
	public void testTwoPair_OnlyKickerInPocket() {
		Pocket pocket = new Pocket(Card.build("Js"), Card.build("9d"));
		Flop flop = new Flop(Card.build("8c"), Card.build("8h"), Card.build("7d"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("7s")));
		CompletedTwoPair result = CompletedTwoPair.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.TWO_PAIR, result.getGenericHandRank());
		assertEquals(CardRank.EIGHT, result.getTopRank());
		assertEquals(CardRank.SEVEN, result.getSecondRank());
		assertEquals(CardRank.JACK, result.getKickerRank());
		assertEquals(4, result.getBestPocketPosition().byteValue());
		assertNull(result.getSecondBestPocketPosition());
		assertTrue(result.isAtLeastBestTwoPair());
	}
	
	@Test
	public void testTwoPair_UselessPocket() {
		Pocket pocket = new Pocket(Card.build("2s"), Card.build("3d"));
		Flop flop = new Flop(Card.build("8c"), Card.build("8h"), Card.build("7d"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("7s"), Card.build("5s")));
		CompletedTwoPair result = CompletedTwoPair.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.TWO_PAIR, result.getGenericHandRank());
		assertEquals(CardRank.EIGHT, result.getTopRank());
		assertEquals(CardRank.SEVEN, result.getSecondRank());
		assertEquals(CardRank.FIVE, result.getKickerRank());
		assertNull(result.getBestPocketPosition());
		assertNull(result.getSecondBestPocketPosition());
		assertTrue(result.isAtLeastBestTwoPair());
	}

	@Test
	public void testCompareWithSuperiorHand() {

		Pocket pocket1 = new Pocket(Card.build("3s"), Card.build("Ad"));
		Flop flop1 = new Flop(Card.build("2s"), Card.build("2h"), Card.build("2c"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("Qc"), Card.build("Jc")));
		CompletedThreeOfAKind toak = CompletedThreeOfAKind.build(pocket1, handCards1.getCards());
		
		Pocket pocket = new Pocket(Card.build("2s"), Card.build("3d"));
		Flop flop = new Flop(Card.build("8c"), Card.build("8h"), Card.build("7d"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("7s"), Card.build("5s")));
		CompletedTwoPair twoPair = CompletedTwoPair.build(pocket, handCards.getCards());

		assertTrue(twoPair.compareTo(toak) < 0);
	}

	@Test
	public void testCompareWithInferiorHand() {
		Pocket pocket1 = new Pocket(Card.build("As"), Card.build("Ad"));
		Flop flop1 = new Flop(Card.build("Kc"), Card.build("Qh"), Card.build("7c"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1));
		CompletedPair pair = CompletedPair.build(pocket1, handCards1.getCards());
		
		Pocket pocket = new Pocket(Card.build("2s"), Card.build("3d"));
		Flop flop = new Flop(Card.build("8c"), Card.build("8h"), Card.build("7d"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("7s"), Card.build("5s")));
		CompletedTwoPair twoPair = CompletedTwoPair.build(pocket, handCards.getCards());

		assertTrue(twoPair.compareTo(pair) > 0);
	}

	@Test
	public void testCompareByFirstRank() {
		
		Pocket pocket1 = new Pocket(Card.build("2s"), Card.build("3d"));
		Flop flop1 = new Flop(Card.build("9c"), Card.build("9h"), Card.build("7d"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("7s"), Card.build("5s")));
		CompletedTwoPair twoPair1 = CompletedTwoPair.build(pocket1, handCards1.getCards());
		
		Pocket pocket = new Pocket(Card.build("2s"), Card.build("3d"));
		Flop flop = new Flop(Card.build("8c"), Card.build("8h"), Card.build("7d"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("7s"), Card.build("5s")));
		CompletedTwoPair twoPair2 = CompletedTwoPair.build(pocket, handCards.getCards());

		assertTrue(twoPair1.compareTo(twoPair2) > 0);
		assertTrue(twoPair2.compareTo(twoPair1) < 0);
	}

	@Test
	public void testCompareBySecondRank() {
		
		Pocket pocket1 = new Pocket(Card.build("2s"), Card.build("3d"));
		Flop flop1 = new Flop(Card.build("9c"), Card.build("9h"), Card.build("7d"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("7s"), Card.build("5s")));
		CompletedTwoPair twoPair1 = CompletedTwoPair.build(pocket1, handCards1.getCards());
		
		Pocket pocket = new Pocket(Card.build("2s"), Card.build("3d"));
		Flop flop = new Flop(Card.build("9c"), Card.build("9h"), Card.build("6d"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("6s"), Card.build("5s")));
		CompletedTwoPair twoPair2 = CompletedTwoPair.build(pocket, handCards.getCards());

		assertTrue(twoPair1.compareTo(twoPair2) > 0);
		assertTrue(twoPair2.compareTo(twoPair1) < 0);
	}

	@Test
	public void testCompareByKickerRank() {
		
		Pocket pocket1 = new Pocket(Card.build("2s"), Card.build("3d"));
		Flop flop1 = new Flop(Card.build("9c"), Card.build("9h"), Card.build("7d"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("7s"), Card.build("5s")));
		CompletedTwoPair twoPair1 = CompletedTwoPair.build(pocket1, handCards1.getCards());
		
		Pocket pocket = new Pocket(Card.build("2s"), Card.build("3d"));
		Flop flop = new Flop(Card.build("9c"), Card.build("9h"), Card.build("7d"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("7s"), Card.build("4s")));
		CompletedTwoPair twoPair2 = CompletedTwoPair.build(pocket, handCards.getCards());

		assertTrue(twoPair1.compareTo(twoPair2) > 0);
		assertTrue(twoPair2.compareTo(twoPair1) < 0);
	}

	@Test
	public void testCompareSameHand() {
		
		Pocket pocket1 = new Pocket(Card.build("2s"), Card.build("3d"));
		Flop flop1 = new Flop(Card.build("9c"), Card.build("9h"), Card.build("7d"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("7s"), Card.build("5s")));
		CompletedTwoPair twoPair1 = CompletedTwoPair.build(pocket1, handCards1.getCards());
		
		Pocket pocket = new Pocket(Card.build("2s"), Card.build("4d"));
		Flop flop = new Flop(Card.build("9c"), Card.build("9h"), Card.build("7d"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("7s"), Card.build("5s")));
		CompletedTwoPair twoPair2 = CompletedTwoPair.build(pocket, handCards.getCards());

		assertEquals(0, twoPair1.compareTo(twoPair2));
		assertEquals(0, twoPair2.compareTo(twoPair1));
	}
	
	@Test
	public void testTwoPair_UselessPocket_NonBestTwoPair() {
		Pocket pocket = new Pocket(Card.build("2s"), Card.build("3d"));
		Flop flop = new Flop(Card.build("8c"), Card.build("8h"), Card.build("6d"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("6s"), Card.build("7s")));
		CompletedTwoPair result = CompletedTwoPair.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.TWO_PAIR, result.getGenericHandRank());
		assertEquals(CardRank.EIGHT, result.getTopRank());
		assertEquals(CardRank.SIX, result.getSecondRank());
		assertEquals(CardRank.SEVEN, result.getKickerRank());
		assertNull(result.getBestPocketPosition());
		assertNull(result.getSecondBestPocketPosition());
		assertFalse(result.isAtLeastBestTwoPair());
	}
}
