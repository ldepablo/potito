package com.ldepablo.poker.educa.model.outs;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.ldepablo.poker.educa.model.Board;
import com.ldepablo.poker.educa.model.Card;
import com.ldepablo.poker.educa.model.Flop;
import com.ldepablo.poker.educa.model.Pocket;

public class FlushDrawOutsCalculatorTest {

	private static final double DELTA = 1e-15;

	@Test
	public void testGetOutsFor_22FD_NoPair(){
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Qs"));
		Flop flop = new Flop(Card.build("Ks"), Card.build("2d"), Card.build("8c"));
		Board board = new Board(flop, Card.build("4s"));
		FlushDrawOutsCalculator calculator = new FlushDrawOutsCalculator(pocket, board);
		assertEquals(9, calculator.calculate(), DELTA);
	}

	@Test
	public void testGetOutsFor_22FD_OnePair(){
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Qs"));
		Flop flop = new Flop(Card.build("Ks"), Card.build("2d"), Card.build("2c"));
		Board board = new Board(flop, Card.build("4s"));
		FlushDrawOutsCalculator calculator = new FlushDrawOutsCalculator(pocket, board);
		assertEquals(7, calculator.calculate(), DELTA);
	}

	@Test
	public void testGetOutsFor_22FD_TwoPair(){
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Qs"));
		Flop flop = new Flop(Card.build("Ks"), Card.build("2d"), Card.build("Kc"));
		Board board = new Board(flop, Card.build("2s"));
		FlushDrawOutsCalculator calculator = new FlushDrawOutsCalculator(pocket, board);
		assertEquals(4, calculator.calculate(), DELTA);
	}

	@Test
	public void testGetOutsFor_22FD_ThreeOfAKind(){
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Qs"));
		Flop flop = new Flop(Card.build("Ks"), Card.build("2d"), Card.build("2hc"));
		Board board = new Board(flop, Card.build("2s"));
		FlushDrawOutsCalculator calculator = new FlushDrawOutsCalculator(pocket, board);
		assertEquals(4, calculator.calculate(), DELTA);
	}

	@Test
	public void testGetOutsFor_21FD_Pair_NutsFD(){
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Qs"));
		Flop flop = new Flop(Card.build("Ks"), Card.build("2d"), Card.build("2h"));
		Board board = new Board(flop);
		FlushDrawOutsCalculator calculator = new FlushDrawOutsCalculator(pocket, board);
		assertEquals(1, calculator.calculate(), DELTA);
	}

	@Test
	public void testGetOutsFor_21FD_NoPair_NutsFD(){
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Qs"));
		Flop flop = new Flop(Card.build("Ks"), Card.build("2d"), Card.build("8h"));
		Board board = new Board(flop);
		FlushDrawOutsCalculator calculator = new FlushDrawOutsCalculator(pocket, board);
		assertEquals(2, calculator.calculate(), DELTA);
	}

	@Test
	public void testGetOutsFor_21FD_NoPair_NoNutsFD(){
		Pocket pocket = new Pocket(Card.build("Js"), Card.build("Qs"));
		Flop flop = new Flop(Card.build("Ks"), Card.build("2d"), Card.build("8h"));
		Board board = new Board(flop);
		FlushDrawOutsCalculator calculator = new FlushDrawOutsCalculator(pocket, board);
		assertEquals(1, calculator.calculate(), DELTA);
	}

	@Test
	public void testGetOutsFor_21FD_NoPair_NutsFD_OnlyAce(){
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Qs"));
		Flop flop = new Flop(Card.build("Js"), Card.build("2d"), Card.build("8h"));
		Board board = new Board(flop);
		FlushDrawOutsCalculator calculator = new FlushDrawOutsCalculator(pocket, board);
		assertEquals(2, calculator.calculate(), DELTA);
	}

	@Test
	public void testGetOutsFor_21FD_NoPair_NutsFD_OnlyKing(){
		Pocket pocket = new Pocket(Card.build("Ks"), Card.build("Qs"));
		Flop flop = new Flop(Card.build("Js"), Card.build("2d"), Card.build("8h"));
		Board board = new Board(flop);
		FlushDrawOutsCalculator calculator = new FlushDrawOutsCalculator(pocket, board);
		assertEquals(1, calculator.calculate(), DELTA);
	}

	@Test
	public void testGetOutsFor_12FD_NutsFD_AceInHand(){
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Qc"));
		Flop flop = new Flop(Card.build("Js"), Card.build("2d"), Card.build("8s"));
		Board board = new Board(flop);
		FlushDrawOutsCalculator calculator = new FlushDrawOutsCalculator(pocket, board);
		assertEquals(1, calculator.calculate(), DELTA);
	}

	@Test
	public void testGetOutsFor_12FD_NutsFD_NoAceInHand(){
		Pocket pocket = new Pocket(Card.build("Ks"), Card.build("Qc"));
		Flop flop = new Flop(Card.build("As"), Card.build("2d"), Card.build("8s"));
		Board board = new Board(flop);
		FlushDrawOutsCalculator calculator = new FlushDrawOutsCalculator(pocket, board);
		assertEquals(1, calculator.calculate(), DELTA);
	}

	@Test
	public void testGetOutsFor_12FD_NutsFD_NoAceInHand_2(){
		Pocket pocket = new Pocket(Card.build("Qs"), Card.build("Qc"));
		Flop flop = new Flop(Card.build("As"), Card.build("2d"), Card.build("Ks"));
		Board board = new Board(flop);
		FlushDrawOutsCalculator calculator = new FlushDrawOutsCalculator(pocket, board);
		assertEquals(1, calculator.calculate(), DELTA);
	}

	@Test
	public void testGetOutsFor_12FD_NoNutsFD_AceInBoard(){
		Pocket pocket = new Pocket(Card.build("Qs"), Card.build("Qc"));
		Flop flop = new Flop(Card.build("As"), Card.build("2d"), Card.build("Js"));
		Board board = new Board(flop);
		FlushDrawOutsCalculator calculator = new FlushDrawOutsCalculator(pocket, board);
		assertEquals(0, calculator.calculate(), DELTA);
	}

	@Test
	public void testGetOutsFor_12FD_NoNutsFD_NoAceInBoard(){
		Pocket pocket = new Pocket(Card.build("Qs"), Card.build("Qc"));
		Flop flop = new Flop(Card.build("Ks"), Card.build("2d"), Card.build("Js"));
		Board board = new Board(flop);
		FlushDrawOutsCalculator calculator = new FlushDrawOutsCalculator(pocket, board);
		assertEquals(0, calculator.calculate(), DELTA);
	}

	@Test
	public void testGetOutsFor_13FD_NoPair_AceInHand_NoKingOnBoard(){
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Qc"));
		Flop flop = new Flop(Card.build("Js"), Card.build("2s"), Card.build("8s"));
		Board board = new Board(flop);
		FlushDrawOutsCalculator calculator = new FlushDrawOutsCalculator(pocket, board);
		assertEquals(9, calculator.calculate(), DELTA);
	}

	@Test
	public void testGetOutsFor_13FD_NoPair_KingInHand_AceOnBoard(){
		Pocket pocket = new Pocket(Card.build("Ks"), Card.build("Qc"));
		Flop flop = new Flop(Card.build("As"), Card.build("2s"), Card.build("8s"));
		Board board = new Board(flop);
		FlushDrawOutsCalculator calculator = new FlushDrawOutsCalculator(pocket, board);
		assertEquals(9, calculator.calculate(), DELTA);
	}

	@Test
	public void testGetOutsFor_13FD_NoPair_KingInHand_NoAceOnBoard(){
		Pocket pocket = new Pocket(Card.build("Ks"), Card.build("Qc"));
		Flop flop = new Flop(Card.build("Qs"), Card.build("2s"), Card.build("8s"));
		Board board = new Board(flop);
		FlushDrawOutsCalculator calculator = new FlushDrawOutsCalculator(pocket, board);
		assertEquals(8, calculator.calculate(), DELTA);
	}


	@Test
	public void testGetOutsFor_13FD_NoPair_JackInHand_NoAceOnBoard(){
		Pocket pocket = new Pocket(Card.build("Js"), Card.build("Tc"));
		Flop flop = new Flop(Card.build("Qs"), Card.build("2s"), Card.build("8s"));
		Board board = new Board(flop);
		FlushDrawOutsCalculator calculator = new FlushDrawOutsCalculator(pocket, board);
		assertEquals(7, calculator.calculate(), DELTA);
	}

	@Test
	public void testGetOutsFor_13FD_PairInBoard_AceInHand_NoKingOnBoard(){
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Qc"));
		Flop flop = new Flop(Card.build("Js"), Card.build("2s"), Card.build("8s"));
		Board board = new Board(flop, Card.build("2h"));
		FlushDrawOutsCalculator calculator = new FlushDrawOutsCalculator(pocket, board);
		assertEquals(7, calculator.calculate(), DELTA);
	}

	@Test
	public void testGetOutsFor_13FD_PairInBoard_KingInHand_AceOnBoard(){
		Pocket pocket = new Pocket(Card.build("Ks"), Card.build("Qc"));
		Flop flop = new Flop(Card.build("As"), Card.build("2s"), Card.build("8s"));
		Board board = new Board(flop, Card.build("2h"));
		FlushDrawOutsCalculator calculator = new FlushDrawOutsCalculator(pocket, board);
		assertEquals(7, calculator.calculate(), DELTA);
	}

	@Test
	public void testGetOutsFor_13FD_PairInBoard_KingInHand_NoAceOnBoard(){
		Pocket pocket = new Pocket(Card.build("Ks"), Card.build("Qc"));
		Flop flop = new Flop(Card.build("Qs"), Card.build("2s"), Card.build("8s"));
		Board board = new Board(flop, Card.build("2h"));
		FlushDrawOutsCalculator calculator = new FlushDrawOutsCalculator(pocket, board);
		assertEquals(6, calculator.calculate(), DELTA);
	}


	@Test
	public void testGetOutsFor_13FD_PairInBoard_JackInHand_AceOnBoard(){
		Pocket pocket = new Pocket(Card.build("Js"), Card.build("Tc"));
		Flop flop = new Flop(Card.build("As"), Card.build("2s"), Card.build("8s"));
		Board board = new Board(flop, Card.build("2h"));
		FlushDrawOutsCalculator calculator = new FlushDrawOutsCalculator(pocket, board);
		assertEquals(5, calculator.calculate(), DELTA);
	}
}
