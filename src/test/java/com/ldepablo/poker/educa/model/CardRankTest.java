package com.ldepablo.poker.educa.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CardRankTest {

	@Test
	public void testDistance() {
		assertEquals(4, CardRank.FIVE.distanceTo(CardRank.NINE));
		assertEquals(4, CardRank.NINE.distanceTo(CardRank.FIVE));
		assertEquals(5, CardRank.SIX.distanceTo(CardRank.ACE));
		assertEquals(6, CardRank.SEVEN.distanceTo(CardRank.ACE));
		assertEquals(6, CardRank.EIGHT.distanceTo(CardRank.ACE));
		assertEquals(1, CardRank.ACE.distanceTo(CardRank.KING));
		assertEquals(6, CardRank.ACE.distanceTo(CardRank.EIGHT));
		assertEquals(6, CardRank.ACE.distanceTo(CardRank.SEVEN));
		assertEquals(0, CardRank.ACE.distanceTo(CardRank.ACE));
		
	}
	
	@Test
	public void testFromValue() {
		assertEquals(CardRank.ACE, CardRank.fromValue(1));
		assertEquals(CardRank.TWO, CardRank.fromValue(2));
		assertEquals(CardRank.THREE, CardRank.fromValue(3));
		assertEquals(CardRank.FOUR, CardRank.fromValue(4));
		assertEquals(CardRank.FIVE, CardRank.fromValue(5));
		assertEquals(CardRank.SIX, CardRank.fromValue(6));
		assertEquals(CardRank.SEVEN, CardRank.fromValue(7));
		assertEquals(CardRank.EIGHT, CardRank.fromValue(8));
		assertEquals(CardRank.NINE, CardRank.fromValue(9));
		assertEquals(CardRank.TEN, CardRank.fromValue(10));
		assertEquals(CardRank.JACK, CardRank.fromValue(11));
		assertEquals(CardRank.QUEEN, CardRank.fromValue(12));
		assertEquals(CardRank.KING, CardRank.fromValue(13));
		assertEquals(CardRank.ACE, CardRank.fromValue(14));
	}
}
