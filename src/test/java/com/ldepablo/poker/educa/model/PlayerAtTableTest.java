package com.ldepablo.poker.educa.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PlayerAtTableTest {
	PlayerAtTable player = new PlayerAtTable();
	
	@Test
	public void testGetStack() {
		int stack = 100;
		player.setStack(stack);
		assertEquals(stack, player.getStack());
	}
	
	@Test
	public void testGetBet() {
		int bet = 10354;
		player.setBet(bet);
		assertEquals(bet, player.getBet());
	}
	
	@Test
	public void testSubstractBetFromStack() {
		int stack = 100;
		player.setStack(stack);
		int bet = 45;
		player.substractBet(bet);
		assertEquals(stack-bet, player.getStack());
	}
	
	@Test
	public void testPosition() {
		Position position = Position.EARLY_POSITION;
		player.setPosition(position);
		assertEquals(position, player.getPosition());
	}
}
