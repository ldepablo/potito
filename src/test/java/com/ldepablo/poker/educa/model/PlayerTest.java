package com.ldepablo.poker.educa.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class PlayerTest {

	private String name = "player_name";
	private Player player = new Player(name);
	
	@Test
	public void testGetName() {
		assertEquals(name, player.getName());
	}
	
	@Test
	public void testGetStats() {
		PlayerStats stats = new PlayerStats();
		double openRaiseVaue = 44.345;
		stats.setOpenRaise(openRaiseVaue);
		player.setStats(stats);
		
		PlayerStats stats2 = new PlayerStats();
		stats2.setOpenRaise(openRaiseVaue);
		assertEquals(stats2, player.getStats());
	}
}
