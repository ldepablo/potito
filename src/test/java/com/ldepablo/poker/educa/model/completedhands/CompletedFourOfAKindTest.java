package com.ldepablo.poker.educa.model.completedhands;

import static org.junit.Assert.*;

import org.junit.Test;

import com.ldepablo.poker.educa.model.Board;
import com.ldepablo.poker.educa.model.Card;
import com.ldepablo.poker.educa.model.CardRank;
import com.ldepablo.poker.educa.model.Flop;
import com.ldepablo.poker.educa.model.GenericHandRank;
import com.ldepablo.poker.educa.model.HandCards;
import com.ldepablo.poker.educa.model.Pocket;

public class CompletedFourOfAKindTest {

	@Test
	public void testFourAcesWithAceAndUselessTwoInPocket() {
		Pocket pocket = new Pocket(Card.build("2c"), Card.build("Ad"));
		Flop flop = new Flop(Card.build("Ah"), Card.build("As"), Card.build("Ac"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Qh")));
		CompletedFourOfAKind result = CompletedFourOfAKind.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.FOUR_OF_A_KIND, result.getGenericHandRank());
		assertEquals(CardRank.ACE, result.getRank());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertNull(result.getSecondBestPocketPosition());
		assertEquals(CardRank.QUEEN, result.getFifthCardRank());
	}

	@Test
	public void testFourKingsWithKingAndAceInPocket() {
		Pocket pocket = new Pocket(Card.build("Kc"), Card.build("Ad"));
		Flop flop = new Flop(Card.build("Kh"), Card.build("Ks"), Card.build("Kd"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		CompletedFourOfAKind result = CompletedFourOfAKind.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.FOUR_OF_A_KIND, result.getGenericHandRank());
		assertEquals(CardRank.KING, result.getRank());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertEquals(4, result.getSecondBestPocketPosition().byteValue());
		assertEquals(CardRank.ACE, result.getFifthCardRank());
	}

	@Test
	public void testFourKingsWithTwoKingsInPocket() {
		Pocket pocket = new Pocket(Card.build("Kc"), Card.build("Kd"));
		Flop flop = new Flop(Card.build("Kh"), Card.build("Ks"), Card.build("Jd"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		CompletedFourOfAKind result = CompletedFourOfAKind.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.FOUR_OF_A_KIND, result.getGenericHandRank());
		assertEquals(CardRank.KING, result.getRank());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertEquals(0, result.getSecondBestPocketPosition().byteValue());
		assertEquals(CardRank.JACK, result.getFifthCardRank());
	}

	@Test
	public void testFourKingsWithUselessPocketPair() {
		Pocket pocket = new Pocket(Card.build("Jc"), Card.build("Jd"));
		Flop flop = new Flop(Card.build("Kh"), Card.build("Ks"), Card.build("Kd"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Kc"), Card.build("Ac")));
		CompletedFourOfAKind result = CompletedFourOfAKind.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.FOUR_OF_A_KIND, result.getGenericHandRank());
		assertEquals(CardRank.KING, result.getRank());
		assertNull(result.getBestPocketPosition());
		assertNull(result.getSecondBestPocketPosition());
		assertEquals(CardRank.ACE, result.getFifthCardRank());
	}

	@Test
	public void testFourKingsWithPocketPairUsingOnlyOneOfTheJacks() {
		Pocket pocket = new Pocket(Card.build("Jc"), Card.build("Jd"));
		Flop flop = new Flop(Card.build("Kh"), Card.build("Ks"), Card.build("Kd"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Kc")));
		CompletedFourOfAKind result = CompletedFourOfAKind.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.FOUR_OF_A_KIND, result.getGenericHandRank());
		assertEquals(CardRank.KING, result.getRank());
		assertEquals(4, result.getBestPocketPosition().byteValue());
		assertNull(result.getSecondBestPocketPosition());
		assertEquals(CardRank.JACK, result.getFifthCardRank());
	}

	@Test
	public void testWhenBestPokerisAlsoPublicThenItShouldBeNull() {
		Pocket pocket = new Pocket(Card.build("Jc"), Card.build("Kd"));
		Flop flop = new Flop(Card.build("Kh"), Card.build("Ks"), Card.build("Jd"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Kc")));
		CompletedFourOfAKind result = CompletedFourOfAKind.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.FOUR_OF_A_KIND, result.getGenericHandRank());
		assertEquals(CardRank.KING, result.getRank());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertNull(result.getSecondBestPocketPosition());
		assertEquals(CardRank.JACK, result.getFifthCardRank());
	}
	
	@Test
	public void testCompareWithStraightFlush() {
		Pocket pocket = new Pocket(Card.build("Jc"), Card.build("Kd"));
		Flop flop = new Flop(Card.build("Kh"), Card.build("Ks"), Card.build("Jd"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Kc")));
		CompletedFourOfAKind fourOfAKind = CompletedFourOfAKind.build(pocket, handCards.getCards());
		

		Pocket pocket2 = new Pocket(Card.build("As"), Card.build("4s"));
		Flop flop2 = new Flop(Card.build("3s"), Card.build("2s"), Card.build("5s"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2,  Card.build("Qs")));
		CompletedStraightFlush completedStraightFlush = CompletedStraightFlush.build(pocket2, handCards2.getCards());
		
		assertTrue(fourOfAKind.compareTo(completedStraightFlush) < 0);
	}
	
	@Test
	public void testCompareWithSuperiorFifthCard() {
		Pocket pocket1 = new Pocket(Card.build("Jc"), Card.build("Kd"));
		Flop flop1 = new Flop(Card.build("Kh"), Card.build("Ks"), Card.build("Jd"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("Kc")));
		CompletedFourOfAKind fourOfAKind1 = CompletedFourOfAKind.build(pocket1, handCards1.getCards());
		

		Pocket pocket2 = new Pocket(Card.build("Jc"), Card.build("Kd"));
		Flop flop2 = new Flop(Card.build("Kh"), Card.build("Ks"), Card.build("Qd"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("Kc")));
		CompletedFourOfAKind fourOfAKind2 = CompletedFourOfAKind.build(pocket2, handCards2.getCards());
		
		assertTrue(fourOfAKind1.compareTo(fourOfAKind2) < 0);
	}
	
	@Test
	public void testCompareWithInferiorFifthCard() {
		Pocket pocket1 = new Pocket(Card.build("Jc"), Card.build("Kd"));
		Flop flop1 = new Flop(Card.build("Kh"), Card.build("Ks"), Card.build("Qd"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("Kc")));
		CompletedFourOfAKind fourOfAKind1 = CompletedFourOfAKind.build(pocket1, handCards1.getCards());
		

		Pocket pocket2 = new Pocket(Card.build("Jc"), Card.build("Kd"));
		Flop flop2 = new Flop(Card.build("Kh"), Card.build("Ks"), Card.build("Jd"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("Kc")));
		CompletedFourOfAKind fourOfAKind2 = CompletedFourOfAKind.build(pocket2, handCards2.getCards());
		
		assertTrue(fourOfAKind1.compareTo(fourOfAKind2) > 0);
	}
	
	@Test
	public void testCompareEqualOnes() {
		Pocket pocket1 = new Pocket(Card.build("Jc"), Card.build("Kd"));
		Flop flop1 = new Flop(Card.build("Kh"), Card.build("Ks"), Card.build("Jd"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("Kc")));
		CompletedFourOfAKind fourOfAKind1 = CompletedFourOfAKind.build(pocket1, handCards1.getCards());
		

		Pocket pocket2 = new Pocket(Card.build("Jc"), Card.build("Kd"));
		Flop flop2 = new Flop(Card.build("Kh"), Card.build("Ks"), Card.build("Jd"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("Kc")));
		CompletedFourOfAKind fourOfAKind2 = CompletedFourOfAKind.build(pocket2, handCards2.getCards());
		
		assertTrue(fourOfAKind1.compareTo(fourOfAKind2) == 0);
	}
	
	@Test
	public void testCompareSuperiorRank() {
		Pocket pocket1 = new Pocket(Card.build("Jc"), Card.build("Kd"));
		Flop flop1 = new Flop(Card.build("Kh"), Card.build("Ks"), Card.build("Jd"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("Kc")));
		CompletedFourOfAKind fourOfAKind1 = CompletedFourOfAKind.build(pocket1, handCards1.getCards());
		

		Pocket pocket2 = new Pocket(Card.build("Jc"), Card.build("Ad"));
		Flop flop2 = new Flop(Card.build("Ah"), Card.build("As"), Card.build("Jd"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("Ac")));
		CompletedFourOfAKind fourOfAKind2 = CompletedFourOfAKind.build(pocket2, handCards2.getCards());
		
		assertTrue(fourOfAKind1.compareTo(fourOfAKind2) < 0);
	}
	
	@Test
	public void testCompareInferiorRank() {
		Pocket pocket1 = new Pocket(Card.build("Jc"), Card.build("Kd"));
		Flop flop1 = new Flop(Card.build("Kh"), Card.build("Ks"), Card.build("Jd"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("Kc")));
		CompletedFourOfAKind fourOfAKind1 = CompletedFourOfAKind.build(pocket1, handCards1.getCards());
		

		Pocket pocket2 = new Pocket(Card.build("Jc"), Card.build("Ad"));
		Flop flop2 = new Flop(Card.build("Ah"), Card.build("As"), Card.build("Jd"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("Ac")));
		CompletedFourOfAKind fourOfAKind2 = CompletedFourOfAKind.build(pocket2, handCards2.getCards());
		
		assertTrue(fourOfAKind2.compareTo(fourOfAKind1) > 0);
	}
	
	@Test
	public void testCompareSuperiorFifthCard() {
		Pocket pocket1 = new Pocket(Card.build("Jc"), Card.build("Kd"));
		Flop flop1 = new Flop(Card.build("Kh"), Card.build("Ks"), Card.build("Jd"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("Kc")));
		CompletedFourOfAKind fourOfAKind1 = CompletedFourOfAKind.build(pocket1, handCards1.getCards());
		

		Pocket pocket2 = new Pocket(Card.build("Jc"), Card.build("Kd"));
		Flop flop2 = new Flop(Card.build("Kh"), Card.build("Ks"), Card.build("Kc"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("Ac")));
		CompletedFourOfAKind fourOfAKind2 = CompletedFourOfAKind.build(pocket2, handCards2.getCards());
		
		assertTrue(fourOfAKind1.compareTo(fourOfAKind2) < 0);
	}
	
	@Test
	public void testCompareInferiorFifthCard() {
		Pocket pocket1 = new Pocket(Card.build("Jc"), Card.build("Kd"));
		Flop flop1 = new Flop(Card.build("Kh"), Card.build("Ks"), Card.build("Jd"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("Kc")));
		CompletedFourOfAKind fourOfAKind1 = CompletedFourOfAKind.build(pocket1, handCards1.getCards());
		

		Pocket pocket2 = new Pocket(Card.build("7c"), Card.build("Kd"));
		Flop flop2 = new Flop(Card.build("Kh"), Card.build("Ks"), Card.build("Kc"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("3c")));
		CompletedFourOfAKind fourOfAKind2 = CompletedFourOfAKind.build(pocket2, handCards2.getCards());
		
		assertTrue(fourOfAKind1.compareTo(fourOfAKind2) > 0);
	}
	
	@Test
	public void testCompareSameFifthCard() {
		Pocket pocket1 = new Pocket(Card.build("Jc"), Card.build("Kd"));
		Flop flop1 = new Flop(Card.build("Kh"), Card.build("Ks"), Card.build("Jd"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("Kc")));
		CompletedFourOfAKind fourOfAKind1 = CompletedFourOfAKind.build(pocket1, handCards1.getCards());
		

		Pocket pocket2 = new Pocket(Card.build("7c"), Card.build("Kd"));
		Flop flop2 = new Flop(Card.build("Kh"), Card.build("Ks"), Card.build("Kc"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("Jc")));
		CompletedFourOfAKind fourOfAKind2 = CompletedFourOfAKind.build(pocket2, handCards2.getCards());
		
		assertEquals(0, fourOfAKind1.compareTo(fourOfAKind2));
	}
}
