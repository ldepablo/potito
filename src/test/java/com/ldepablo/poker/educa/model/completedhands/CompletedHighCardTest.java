package com.ldepablo.poker.educa.model.completedhands;

import static org.junit.Assert.*;

import org.junit.Test;

import com.ldepablo.poker.educa.model.Board;
import com.ldepablo.poker.educa.model.Card;
import com.ldepablo.poker.educa.model.CardRank;
import com.ldepablo.poker.educa.model.Flop;
import com.ldepablo.poker.educa.model.GenericHandRank;
import com.ldepablo.poker.educa.model.HandCards;
import com.ldepablo.poker.educa.model.Pocket;

public class CompletedHighCardTest {

	@Test
	public void testGetFirstAndSecondPocket() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Qd"));
		Flop flop = new Flop(Card.build("Tc"), Card.build("Jh"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("4h")));
		CompletedHighCard result = CompletedHighCard.build(pocket, handCards.getCards());
		assertEquals(GenericHandRank.HIGH_CARD, result.getGenericHandRank());
		assertEquals(CardRank.ACE, result.getRank1());
		assertEquals(CardRank.QUEEN, result.getRank2());
		assertEquals(CardRank.JACK, result.getRank3());
		assertEquals(CardRank.TEN, result.getRank4());
		assertEquals(CardRank.SEVEN, result.getRank5());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertEquals(1, result.getSecondBestPocketPosition().byteValue());
	}

	@Test
	public void testFirstAndFourth() {
		Pocket pocket = new Pocket(Card.build("9s"), Card.build("Qd"));
		Flop flop = new Flop(Card.build("Tc"), Card.build("Jh"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("4h")));
		CompletedHighCard result = CompletedHighCard.build(pocket, handCards.getCards());
		assertEquals(GenericHandRank.HIGH_CARD, result.getGenericHandRank());
		assertEquals(CardRank.QUEEN, result.getRank1());
		assertEquals(CardRank.JACK, result.getRank2());
		assertEquals(CardRank.TEN, result.getRank3());
		assertEquals(CardRank.NINE, result.getRank4());
		assertEquals(CardRank.SEVEN, result.getRank5());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertEquals(3, result.getSecondBestPocketPosition().byteValue());
	}

	@Test
	public void testFirstAndUseless() {
		Pocket pocket = new Pocket(Card.build("3s"), Card.build("Qd"));
		Flop flop = new Flop(Card.build("Tc"), Card.build("Jh"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("4h")));
		CompletedHighCard result = CompletedHighCard.build(pocket, handCards.getCards());
		assertEquals(GenericHandRank.HIGH_CARD, result.getGenericHandRank());
		assertEquals(CardRank.QUEEN, result.getRank1());
		assertEquals(CardRank.JACK, result.getRank2());
		assertEquals(CardRank.TEN, result.getRank3());
		assertEquals(CardRank.SEVEN, result.getRank4());
		assertEquals(CardRank.FOUR, result.getRank5());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertNull(result.getSecondBestPocketPosition());
	}

	@Test
	public void testSecondtAndUseless() {
		Pocket pocket = new Pocket(Card.build("3s"), Card.build("Jd"));
		Flop flop = new Flop(Card.build("Tc"), Card.build("Qh"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("4h")));
		CompletedHighCard result = CompletedHighCard.build(pocket, handCards.getCards());
		assertEquals(GenericHandRank.HIGH_CARD, result.getGenericHandRank());
		assertEquals(CardRank.QUEEN, result.getRank1());
		assertEquals(CardRank.JACK, result.getRank2());
		assertEquals(CardRank.TEN, result.getRank3());
		assertEquals(CardRank.SEVEN, result.getRank4());
		assertEquals(CardRank.FOUR, result.getRank5());
		assertEquals(1, result.getBestPocketPosition().byteValue());
		assertNull(result.getSecondBestPocketPosition());
	}

	@Test
	public void testUseless() {
		Pocket pocket = new Pocket(Card.build("3s"), Card.build("4d"));
		Flop flop = new Flop(Card.build("Tc"), Card.build("Qh"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Jh"), Card.build("8h")));
		CompletedHighCard result = CompletedHighCard.build(pocket, handCards.getCards());
		assertEquals(GenericHandRank.HIGH_CARD, result.getGenericHandRank());
		assertEquals(CardRank.QUEEN, result.getRank1());
		assertEquals(CardRank.JACK, result.getRank2());
		assertEquals(CardRank.TEN, result.getRank3());
		assertEquals(CardRank.EIGHT, result.getRank4());
		assertEquals(CardRank.SEVEN, result.getRank5());
		assertNull(result.getBestPocketPosition());
		assertNull(result.getSecondBestPocketPosition());
	}

	@Test
	public void testSecondtAndFifth() {
		Pocket pocket = new Pocket(Card.build("4s"), Card.build("Jd"));
		Flop flop = new Flop(Card.build("Tc"), Card.build("Qh"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("3h")));
		CompletedHighCard result = CompletedHighCard.build(pocket, handCards.getCards());
		assertEquals(GenericHandRank.HIGH_CARD, result.getGenericHandRank());
		assertEquals(CardRank.QUEEN, result.getRank1());
		assertEquals(CardRank.JACK, result.getRank2());
		assertEquals(CardRank.TEN, result.getRank3());
		assertEquals(CardRank.SEVEN, result.getRank4());
		assertEquals(CardRank.FOUR, result.getRank5());
		assertEquals(1, result.getBestPocketPosition().byteValue());
		assertEquals(4, result.getSecondBestPocketPosition().byteValue());
	}

	@Test
	public void testCompareWithSuperiorHand() {
		Pocket pocket1 = new Pocket(Card.build("As"), Card.build("Ad"));
		Flop flop1 = new Flop(Card.build("Kc"), Card.build("Qh"), Card.build("7c"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1));
		CompletedPair pair = CompletedPair.build(pocket1, handCards1.getCards());

		Pocket pocket = new Pocket(Card.build("As"), Card.build("Qd"));
		Flop flop = new Flop(Card.build("Tc"), Card.build("Jh"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("4h")));
		CompletedHighCard highCard = CompletedHighCard.build(pocket, handCards.getCards());

		assertTrue(highCard.compareTo(pair) < 0);
	}

	@Test
	public void testCompareByRank1() {

		Pocket pocket1 = new Pocket(Card.build("As"), Card.build("Qd"));
		Flop flop1 = new Flop(Card.build("Tc"), Card.build("8h"), Card.build("6c"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("4h")));
		CompletedHighCard highCard1 = CompletedHighCard.build(pocket1, handCards1.getCards());

		Pocket pocket2 = new Pocket(Card.build("Ks"), Card.build("Qd"));
		Flop flop2 = new Flop(Card.build("Tc"), Card.build("8h"), Card.build("6c"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("4h")));
		CompletedHighCard highCard2 = CompletedHighCard.build(pocket2, handCards2.getCards());

		assertTrue(highCard1.compareTo(highCard2) > 0);
		assertTrue(highCard2.compareTo(highCard1) < 0);
	}

	@Test
	public void testCompareByRank2() {

		Pocket pocket1 = new Pocket(Card.build("As"), Card.build("Qd"));
		Flop flop1 = new Flop(Card.build("Tc"), Card.build("8h"), Card.build("6c"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("4h")));
		CompletedHighCard highCard1 = CompletedHighCard.build(pocket1, handCards1.getCards());

		Pocket pocket2 = new Pocket(Card.build("As"), Card.build("Jd"));
		Flop flop2 = new Flop(Card.build("Tc"), Card.build("8h"), Card.build("6c"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("4h")));
		CompletedHighCard highCard2 = CompletedHighCard.build(pocket2, handCards2.getCards());

		assertTrue(highCard1.compareTo(highCard2) > 0);
		assertTrue(highCard2.compareTo(highCard1) < 0);
	}

	@Test
	public void testCompareByRank3() {

		Pocket pocket1 = new Pocket(Card.build("As"), Card.build("Qd"));
		Flop flop1 = new Flop(Card.build("Tc"), Card.build("8h"), Card.build("6c"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("4h")));
		CompletedHighCard highCard1 = CompletedHighCard.build(pocket1, handCards1.getCards());

		Pocket pocket2 = new Pocket(Card.build("As"), Card.build("Qd"));
		Flop flop2 = new Flop(Card.build("9c"), Card.build("8h"), Card.build("6c"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("4h")));
		CompletedHighCard highCard2 = CompletedHighCard.build(pocket2, handCards2.getCards());

		assertTrue(highCard1.compareTo(highCard2) > 0);
		assertTrue(highCard2.compareTo(highCard1) < 0);
	}

	@Test
	public void testCompareByRank4() {

		Pocket pocket1 = new Pocket(Card.build("As"), Card.build("Qd"));
		Flop flop1 = new Flop(Card.build("Tc"), Card.build("8h"), Card.build("6c"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("4h")));
		CompletedHighCard highCard1 = CompletedHighCard.build(pocket1, handCards1.getCards());

		Pocket pocket2 = new Pocket(Card.build("As"), Card.build("Qd"));
		Flop flop2 = new Flop(Card.build("Tc"), Card.build("7h"), Card.build("6c"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("4h")));
		CompletedHighCard highCard2 = CompletedHighCard.build(pocket2, handCards2.getCards());

		assertTrue(highCard1.compareTo(highCard2) > 0);
		assertTrue(highCard2.compareTo(highCard1) < 0);
	}

	@Test
	public void testCompareByRank5() {

		Pocket pocket1 = new Pocket(Card.build("As"), Card.build("Qd"));
		Flop flop1 = new Flop(Card.build("Tc"), Card.build("8h"), Card.build("6c"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("4h")));
		CompletedHighCard highCard1 = CompletedHighCard.build(pocket1, handCards1.getCards());

		Pocket pocket2 = new Pocket(Card.build("As"), Card.build("Qd"));
		Flop flop2 = new Flop(Card.build("Tc"), Card.build("8h"), Card.build("5c"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("4h")));
		CompletedHighCard highCard2 = CompletedHighCard.build(pocket2, handCards2.getCards());

		assertTrue(highCard1.compareTo(highCard2) > 0);
		assertTrue(highCard2.compareTo(highCard1) < 0);
	}

	@Test
	public void testCompareSameHand() {

		Pocket pocket1 = new Pocket(Card.build("As"), Card.build("Qd"));
		Flop flop1 = new Flop(Card.build("Tc"), Card.build("8h"), Card.build("6c"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("4h")));
		CompletedHighCard highCard1 = CompletedHighCard.build(pocket1, handCards1.getCards());

		Pocket pocket2 = new Pocket(Card.build("As"), Card.build("Qd"));
		Flop flop2 = new Flop(Card.build("Tc"), Card.build("8h"), Card.build("6c"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("3h")));
		CompletedHighCard highCard2 = CompletedHighCard.build(pocket2, handCards2.getCards());

		assertEquals(0, highCard1.compareTo(highCard2));
		assertEquals(0, highCard2.compareTo(highCard1));
	}
}
