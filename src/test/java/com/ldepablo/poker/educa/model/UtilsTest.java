package com.ldepablo.poker.educa.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.junit.Ignore;
import org.junit.Test;

public class UtilsTest {

	Card card1 = new Card(CardRank.SEVEN, Suit.DIAMONDS);
	Card card2 = new Card(CardRank.ACE, Suit.HEARTS);
	Card card3 = new Card(CardRank.TEN, Suit.CLUBS);
	
	@Test
	public void testSuitedCards() {
		Collection<Card> cards = new ArrayList<Card>();
	cards.add(card1);
	cards.add(card2);
	cards.add(card3);
		assertEquals(1, Utils.suitedCards(cards));
		
		Card card4 = new Card(CardRank.TEN, Suit.HEARTS);
		cards = new ArrayList<Card>();
		cards.add(card1);
		cards.add(card2);
		cards.add(card4);
		assertEquals(2, Utils.suitedCards(cards));
		
		Card card5 = new Card(CardRank.TEN, Suit.SPADES);
		Card card6 = new Card(CardRank.TEN, Suit.HEARTS);
		Card card7 = new Card(CardRank.TEN, Suit.DIAMONDS);
		Card card8 = new Card(CardRank.TEN, Suit.CLUBS);
		
		cards = new ArrayList<Card>();
		cards.add(card1);
		cards.add(card2);
		cards.add(card3);
		cards.add(card4);
		cards.add(card5);
		cards.add(card6);
		cards.add(card7);
		cards.add(card8);
		assertEquals(3, Utils.suitedCards(cards));
	}
	
	@Test
	public void testGetPairs() {
		Collection<Card> cards = new HashSet<Card>(3);
		cards.add(card1);
		cards.add(card2);
		cards.add(card3);
		
		Collection<CardRank> pairs = Utils.getPairs(cards);
		assertEquals(0,  pairs.size());
		
		cards.clear();
		cards.add(card1);
		cards.add(card2);
		cards.add(Card.build("7h"));
		cards.add(Card.build("As"));
		
		pairs = Utils.getPairs(cards);
		assertEquals(2,  pairs.size());
		assertTrue(pairs.contains(CardRank.SEVEN));
		assertTrue(pairs.contains(CardRank.ACE));
	}
	
	@Test
	@Ignore
	public void testNStraightDrawF11110() {

		Collection<CardRank> pairs = new ArrayList<CardRank>();
		pairs.add(CardRank.ACE);
		pairs.add(CardRank.TWO);
		pairs.add(CardRank.THREE);
		pairs.add(CardRank.FOUR);
		pairs.add(CardRank.SEVEN);
		
		assertEquals(1, Utils.nStraightDraw(pairs));
	}
	
	@Test
	public void testNStraightDraw0111101() {
		Collection<CardRank> pairs = new ArrayList<CardRank>();
		pairs.add(CardRank.TWO);
		pairs.add(CardRank.THREE);
		pairs.add(CardRank.FOUR);
		pairs.add(CardRank.FIVE);
		pairs.add(CardRank.SEVEN);
		
		assertEquals(1, Utils.nStraightDraw(pairs));
	}
	
	@Test
	public void testNStraightDraw011101() {
		Collection<CardRank> pairs = new ArrayList<CardRank>();
		pairs.add(CardRank.TWO);
		pairs.add(CardRank.THREE);
		pairs.add(CardRank.FOUR);
		pairs.add(CardRank.SIX);
		pairs.add(CardRank.TEN);
		
		assertEquals(1, Utils.nStraightDraw(pairs));
	}
	
	@Test
	public void testNStraightDraw01111F() {
		Collection<CardRank> pairs = new ArrayList<CardRank>();
		pairs.add(CardRank.JACK);
		pairs.add(CardRank.QUEEN);
		pairs.add(CardRank.KING);
		pairs.add(CardRank.ACE);
		pairs.add(CardRank.FOUR);
		
		assertEquals(1, Utils.nStraightDraw(pairs));
	}
	
	@Test
	public void testNStraightDraw1011101() {
		Collection<CardRank> pairs = new ArrayList<CardRank>();
		pairs.add(CardRank.SEVEN);
		pairs.add(CardRank.NINE);
		pairs.add(CardRank.TEN);
		pairs.add(CardRank.JACK);
		pairs.add(CardRank.KING);
		
		assertEquals(1, Utils.nStraightDraw(pairs));
	}
	
	@Test
	public void testNStraightDraw101101() {
		Collection<CardRank> pairs = new ArrayList<CardRank>();
		pairs.add(CardRank.EIGHT);
		pairs.add(CardRank.TEN);
		pairs.add(CardRank.JACK);
		pairs.add(CardRank.KING);
		
		assertEquals(2, Utils.nStraightDraw(pairs));
	}
	
	@Test
	public void testNStraightDraw111() {
		Collection<CardRank> pairs = new ArrayList<CardRank>();
		pairs.add(CardRank.SIX);
		pairs.add(CardRank.TEN);
		pairs.add(CardRank.JACK);
		pairs.add(CardRank.QUEEN);
		
		assertEquals(2, Utils.nStraightDraw(pairs));
	}
	
	@Test
	public void testNStraightDraw111F() {

		Collection<CardRank> pairs = new ArrayList<CardRank>();
		pairs.add(CardRank.FIVE);
		pairs.add(CardRank.ACE);
		pairs.add(CardRank.KING);
		pairs.add(CardRank.QUEEN);
		
		assertEquals(2, Utils.nStraightDraw(pairs));
	}
	
	@Test
	public void testGetRanksFromCards() {
		Collection<Card> cards = new ArrayList<Card>();
		cards.add(Card.build("As"));
		cards.add(Card.build("Kd"));
		cards.add(Card.build("5s"));
		cards.add(Card.build("3h"));
		cards.add(Card.build("Ad"));
		cards.add(Card.build("7s"));
		
		Set<CardRank> result = Utils.getRanksSet(cards);
		assertEquals(5, result.size());
		assertTrue(result.contains(CardRank.ACE));
		assertTrue(result.contains(CardRank.KING));
		assertTrue(result.contains(CardRank.SEVEN));
		assertTrue(result.contains(CardRank.FIVE));
		assertTrue(result.contains(CardRank.THREE));
	}
		
	@Test
	public void testGetRanksQuantitiesInArray(){

		Collection<Card> cards = new ArrayList<Card>();
		cards.add(Card.build("As"));
		cards.add(Card.build("Kd"));
		cards.add(Card.build("5s"));
		cards.add(Card.build("3h"));
		cards.add(Card.build("Ad"));
		cards.add(Card.build("7s"));
		
		int[] result = Utils.getRanksQuantitiesInArray(cards);
		assertEquals(13, result.length);
		assertEquals(0, result[0]);
		assertEquals(1, result[1]);
		assertEquals(0, result[2]);
		assertEquals(1, result[3]);
		assertEquals(0, result[4]);
		assertEquals(1, result[5]);
		assertEquals(0, result[6]);
		assertEquals(0, result[7]);
		assertEquals(0, result[8]);
		assertEquals(0, result[9]);
		assertEquals(0, result[10]);
		assertEquals(1, result[11]);
		assertEquals(2, result[12]);
		
		cards = new ArrayList<Card>();
		result = Utils.getRanksQuantitiesInArray(cards);
		assertEquals(13, result.length);
		for (int i=0; i<13; i++){
			assertEquals(0, result[i]);
		}
		
	}

}
