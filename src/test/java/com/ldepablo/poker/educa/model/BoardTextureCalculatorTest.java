package com.ldepablo.poker.educa.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BoardTextureCalculatorTest {


	
	@Test
	public void test01() {
		assertEquals(BoardTexture.DRY, new BoardTextureCalculator(Card.build("8c"), Card.build("Ah"), Card.build("5d")).calculate());
		assertEquals(BoardTexture.SEMICOORDINATED, new BoardTextureCalculator(Card.build("8c"), Card.build("Ah"), Card.build("5d"), Card.build("Qs")).calculate());
		assertEquals(BoardTexture.COORDINATED, new BoardTextureCalculator(Card.build("8c"), Card.build("Ah"), Card.build("5d"), Card.build("Qs"), Card.build("4s")).calculate());
	}
	
	@Test
	public void test02() {
		
		assertEquals(BoardTexture.COORDINATED, new BoardTextureCalculator(Card.build("5h"), Card.build("7h"), Card.build("8h")).calculate());
		Card turn = Card.build("5d");
		assertEquals(BoardTexture.COORDINATED, new BoardTextureCalculator(Card.build("5h"), Card.build("7h"), Card.build("8h"), turn).calculate());
		Card river = Card.build("3h");
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, new BoardTextureCalculator(Card.build("5h"), Card.build("7h"), Card.build("8h"), turn, river).calculate());
	}
	
	@Test
	public void test03() {
		assertEquals(BoardTexture.DRY, new BoardTextureCalculator(Card.build("3c"), Card.build("8d"), Card.build("8c")).calculate());
		Card turn = Card.build("Ks");
		assertEquals(BoardTexture.DRY, new BoardTextureCalculator(Card.build("3c"), Card.build("8d"), Card.build("8c"), turn).calculate());
		Card river = Card.build("4d");
		assertEquals(BoardTexture.DRY, new BoardTextureCalculator(Card.build("3c"), Card.build("8d"), Card.build("8c"), turn, river).calculate());
	}
	
	@Test
	public void test04() {
		assertEquals(BoardTexture.DRY, new BoardTextureCalculator(Card.build("7d"), Card.build("2d"), Card.build("Ks")).calculate());
		assertEquals(BoardTexture.SEMICOORDINATED, new BoardTextureCalculator(Card.build("7d"), Card.build("2d"), Card.build("Ks"), Card.build("8c")).calculate());
		assertEquals(BoardTexture.DRY, new BoardTextureCalculator(Card.build("7d"), Card.build("2d"), Card.build("Ks"), Card.build("8c"), Card.build("7c")).calculate());
	}
	
	@Test
	public void test05() {
		assertEquals(BoardTexture.DRY, new BoardTextureCalculator(Card.build("2s"), Card.build("3h"), Card.build("Qc")).calculate());
		Card turn = Card.build("Kh");
		assertEquals(BoardTexture.SEMICOORDINATED, new BoardTextureCalculator(Card.build("2s"), Card.build("3h"), Card.build("Qc"), turn).calculate());
		Card river = Card.build("3d");
		assertEquals(BoardTexture.DRY, new BoardTextureCalculator(Card.build("2s"), Card.build("3h"), Card.build("Qc"), turn, river).calculate());
	}
	
	@Test
	public void test06() {
		assertEquals(BoardTexture.COORDINATED, new BoardTextureCalculator(Card.build("9s"), Card.build("5d"), Card.build("7c")).calculate());
		Card turn = Card.build("Td");
		assertEquals(BoardTexture.COORDINATED, new BoardTextureCalculator(Card.build("9s"), Card.build("5d"), Card.build("7c"), turn).calculate());
		Card river = Card.build("6s");
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, new BoardTextureCalculator(Card.build("9s"), Card.build("5d"), Card.build("7c"), turn, river).calculate());
	}
	
	@Test
	public void test07() {
		assertEquals(BoardTexture.SEMICOORDINATED, new BoardTextureCalculator(Card.build("9c"), Card.build("Td"), Card.build("3d")).calculate());
		Card turn = Card.build("Ks");
		assertEquals(BoardTexture.COORDINATED, new BoardTextureCalculator(Card.build("9c"), Card.build("Td"), Card.build("3d"), turn).calculate());
		Card river = Card.build("2c");
		assertEquals(BoardTexture.COORDINATED, new BoardTextureCalculator(Card.build("9c"), Card.build("Td"), Card.build("3d"), turn, river).calculate());
	}
	
	@Test
	public void test08() {
		assertEquals(BoardTexture.COORDINATED, new BoardTextureCalculator(Card.build("7h"), Card.build("9h"), Card.build("Kh")).calculate());
		Card turn = Card.build("2s");
		assertEquals(BoardTexture.COORDINATED, new BoardTextureCalculator(Card.build("7h"), Card.build("9h"), Card.build("Kh"), turn).calculate());
		Card river = Card.build("8s");
		assertEquals(BoardTexture.COORDINATED, new BoardTextureCalculator(Card.build("7h"), Card.build("9h"), Card.build("Kh"), turn, river).calculate());
	}
	
	@Test
	public void test09() {
		
		assertEquals(BoardTexture.SEMICOORDINATED, new BoardTextureCalculator(Card.build("Qh"), Card.build("Td"), Card.build("Qs")).calculate());
		Card turn = Card.build("Ah");
		assertEquals(BoardTexture.COORDINATED, new BoardTextureCalculator(Card.build("Qh"), Card.build("Td"), Card.build("Qs"), turn).calculate());
		Card river = Card.build("As");
		assertEquals(BoardTexture.COORDINATED, new BoardTextureCalculator(Card.build("Qh"), Card.build("Td"), Card.build("Qs"), turn, river).calculate());
	}
	
	@Test
	public void test10() {
		
		assertEquals(BoardTexture.DRY, new BoardTextureCalculator(Card.build("6s"), Card.build("Ac"), Card.build("3s")).calculate());
		Card turn = Card.build("Jh");
		assertEquals(BoardTexture.SEMICOORDINATED, new BoardTextureCalculator(Card.build("6s"), Card.build("Ac"), Card.build("3s"), turn).calculate());
		Card river = Card.build("8d");
		assertEquals(BoardTexture.DRY, new BoardTextureCalculator(Card.build("6s"), Card.build("Ac"), Card.build("3s"), turn, river).calculate());
	}
	
	@Test
	public void test11() {
		assertEquals(BoardTexture.DRY, new BoardTextureCalculator(Card.build("2c"), Card.build("5h"), Card.build("Tc")).calculate());
		assertEquals(BoardTexture.COORDINATED, new BoardTextureCalculator(Card.build("2c"), Card.build("5h"), Card.build("Tc"), Card.build("4h")).calculate());
		assertEquals(BoardTexture.COORDINATED, new BoardTextureCalculator(Card.build("2c"), Card.build("5h"), Card.build("Tc"), Card.build("4h"), Card.build("As")).calculate());
	}
	
	@Test
	public void test12() {
		assertEquals(BoardTexture.SEMICOORDINATED, new BoardTextureCalculator(Card.build("Tc"), Card.build("Kh"), Card.build("2h")).calculate());
		assertEquals(BoardTexture.COORDINATED, new BoardTextureCalculator(Card.build("Tc"), Card.build("Kh"), Card.build("2h"), Card.build("Qh")).calculate());
		assertEquals(BoardTexture.COORDINATED, new BoardTextureCalculator(Card.build("Tc"), Card.build("Kh"), Card.build("2h"), Card.build("Qh"), Card.build("2d")).calculate());
	}
	
	@Test
	public void test13() {
		assertEquals(BoardTexture.DRY, new BoardTextureCalculator(Card.build("3s"), Card.build("3c"), Card.build("3d")).calculate());
		assertEquals(BoardTexture.DRY, new BoardTextureCalculator(Card.build("3s"), Card.build("3c"), Card.build("3d"), Card.build("9h")).calculate());
		assertEquals(BoardTexture.DRY, new BoardTextureCalculator(Card.build("3s"), Card.build("3c"), Card.build("3d"), Card.build("9h"), Card.build("Js")).calculate());
	}
	
	@Test
	public void test14() {
		assertEquals(BoardTexture.DRY, new BoardTextureCalculator(Card.build("2d"), Card.build("4s"), Card.build("Kd")).calculate());
		assertEquals(BoardTexture.COORDINATED, new BoardTextureCalculator(Card.build("2d"), Card.build("4s"), Card.build("Kd"), Card.build("5d")).calculate());
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, new BoardTextureCalculator(Card.build("2d"), Card.build("4s"), Card.build("Kd"), Card.build("5d"), Card.build("Td")).calculate());
	}
	
	@Test
	public void test15() {
		assertEquals(BoardTexture.COORDINATED, new BoardTextureCalculator(Card.build("Ts"), Card.build("Qc"), Card.build("Ks")).calculate());
		assertEquals(BoardTexture.COORDINATED, new BoardTextureCalculator(Card.build("Ts"), Card.build("Qc"), Card.build("Ks"), Card.build("4h")).calculate());
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, new BoardTextureCalculator(Card.build("Ts"), Card.build("Qc"), Card.build("Ks"), Card.build("4h"), Card.build("Jc")).calculate());
	}
	
	@Test
	public void test16() {
		assertEquals(BoardTexture.DRY, new BoardTextureCalculator(Card.build("Ts"), Card.build("5d"), Card.build("2d")).calculate());
		assertEquals(BoardTexture.SEMICOORDINATED, new BoardTextureCalculator(Card.build("Ts"), Card.build("5d"), Card.build("2d"), Card.build("7c")).calculate());
		assertEquals(BoardTexture.DRY, new BoardTextureCalculator(Card.build("Ts"), Card.build("5d"), Card.build("2d"), Card.build("7c"), Card.build("7s")).calculate());
	}
	
//	@Test
//	public void test() {
//		assertEquals(BoardTexture., new BoardTextureCalculator(Card.build(""), Card.build(""), Card.build("")).calculate());
//		assertEquals(BoardTexture., new BoardTextureCalculator(, Card.build("")).calculate());
//		assertEquals(BoardTexture., new BoardTextureCalculator(, Card.build("")).calculate());
//	}
//	
//	@Test
//	public void test() {
//		assertEquals(BoardTexture., new BoardTextureCalculator(Card.build(""), Card.build(""), Card.build("")).calculate());
//		assertEquals(BoardTexture., new BoardTextureCalculator(, Card.build("")).calculate());
//		assertEquals(BoardTexture., new BoardTextureCalculator(, Card.build("")).calculate());
//	}
//	
//	@Test
//	public void test() {
//		assertEquals(BoardTexture., new BoardTextureCalculator(Card.build(""), Card.build(""), Card.build("")).calculate());
//		assertEquals(BoardTexture., new BoardTextureCalculator(, Card.build("")).calculate());
//		assertEquals(BoardTexture., new BoardTextureCalculator(, Card.build("")).calculate());
//	}
//	
//	@Test
//	public void test() {
//		assertEquals(BoardTexture., new BoardTextureCalculator(Card.build(""), Card.build(""), Card.build("")).calculate());
//		assertEquals(BoardTexture., new BoardTextureCalculator(, Card.build("")).calculate());
//		assertEquals(BoardTexture., new BoardTextureCalculator(, Card.build("")).calculate());
//	}
//	
//	@Test
//	public void test() {
//		assertEquals(BoardTexture., new BoardTextureCalculator(Card.build(""), Card.build(""), Card.build("")).calculate());
//		assertEquals(BoardTexture., new BoardTextureCalculator(, Card.build("")).calculate());
//		assertEquals(BoardTexture., new BoardTextureCalculator(, Card.build("")).calculate());
//	}
//	
//	@Test
//	public void test() {
//		assertEquals(BoardTexture., new BoardTextureCalculator(Card.build(""), Card.build(""), Card.build("")).calculate());
//		assertEquals(BoardTexture., new BoardTextureCalculator(, Card.build("")).calculate());
//		assertEquals(BoardTexture., new BoardTextureCalculator(, Card.build("")).calculate());
//	}
//	
//	@Test
//	public void test() {
//		assertEquals(BoardTexture., new BoardTextureCalculator(Card.build(""), Card.build(""), Card.build("")).calculate());
//		assertEquals(BoardTexture., new BoardTextureCalculator(, Card.build("")).calculate());
//		assertEquals(BoardTexture., new BoardTextureCalculator(, Card.build("")).calculate());
//	}
//	
}
