package com.ldepablo.poker.educa.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CardTest {

	Card card1 = new Card(CardRank.SEVEN, Suit.DIAMONDS);
	Card card2 = new Card(CardRank.ACE, Suit.HEARTS);
	Card card3 = new Card(CardRank.TEN, Suit.CLUBS);
	
	@Test
	public void testGetNumber() {
		Card card = new Card(CardRank.SEVEN, Suit.DIAMONDS);
		assertEquals(CardRank.SEVEN, card.getRank());
	}

	@Test
	public void testGetSuit() {
		Card card = new Card(CardRank.SEVEN, Suit.DIAMONDS);
		assertEquals(Suit.DIAMONDS, card.getSuit());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testNullNotAllowedAsNumber() {
		new Card(null, Suit.DIAMONDS);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testNullNotAllowedAsSuit() {
		new Card(CardRank.SEVEN, null);
	}
	
	@Test
	public void testDistance() {
		Card card1 = new Card(CardRank.EIGHT, Suit.CLUBS);
		Card card2 = new Card(CardRank.EIGHT, Suit.HEARTS);
		assertEquals(0, card1.distanceTo(card2));

		Card card3 = new Card(CardRank.SEVEN, Suit.HEARTS);
		assertEquals(1, card1.distanceTo(card3));

		Card card4 = new Card(CardRank.TEN, Suit.HEARTS);
		assertEquals(2, card1.distanceTo(card4));
	}
	
	@Test
	public void testIsSameNumber() {
		Card card1 = new Card(CardRank.EIGHT, Suit.CLUBS);
		Card card2 = new Card(CardRank.EIGHT, Suit.HEARTS);
		assertTrue(card1.isPairWith(card2));

		Card card3 = new Card(CardRank.FOUR, Suit.HEARTS);
		assertFalse(card1.isPairWith(card3));
	}
	
	@Test
	public void testIsPair() {
		
		assertFalse(card1.isPairWith(card2));
		
		Card card3 = new Card(card1.getRank(), Suit.HEARTS);
		assertTrue(card1.isPairWith(card3));
	}

	@Test
	public void testIsSuited() {
		assertFalse(card1.isSuitedWith(card2));
		
		Card card3 = new Card(CardRank.TEN, card1.getSuit());
		assertTrue(card1.isSuitedWith(card3));
	}

	@Test
	public void testCompareWithCard() {
		assertEquals(-1,  card1.compareTo(card2));
		assertEquals(1,  card2.compareTo(card1));
		
		assertEquals(1,  card2.compareTo(card3));
		assertEquals(-1,  card3.compareTo(card2));
		
		assertEquals(-1,  card1.compareTo(card2));
		assertEquals(1,  card2.compareTo(card1));
		
		assertEquals(-1,  card1.compareTo(card3));
		assertEquals(1,  card3.compareTo(card1));

		assertEquals(0,  card1.compareTo(Card.build("7s")));
	}

	@Test
	public void testCompareWithCardRank() {
		assertEquals(-1,  card1.compareTo(CardRank.ACE));
		assertEquals(1,  card2.compareTo(CardRank.SEVEN));
		
		assertEquals(1,  card2.compareTo(CardRank.TEN));
		assertEquals(-1,  card3.compareTo(CardRank.ACE));
		
		assertEquals(-1,  card1.compareTo(CardRank.ACE));
		assertEquals(1,  card2.compareTo(CardRank.SEVEN));
		
		assertEquals(-1,  card1.compareTo(CardRank.TEN));
		assertEquals(1,  card3.compareTo(CardRank.SEVEN));

		assertEquals(0,  card1.compareTo(CardRank.SEVEN));
	}
	
}
