package com.ldepablo.poker.educa.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class PreFlopHandTest {
	
	@Test
	public void testIsWithSuited() {
		Pocket pocket = new Pocket(Card.build("Ac"), Card.build("Kc"));
		PreFlopHand preFlopHand = new PreFlopHand(CardRank.ACE, CardRank.KING, true);
		assertEquals(preFlopHand, pocket.getPreFlopHand());
	}
	
	@Test
	public void testIsWithNonSuited() {
		Pocket pocket = new Pocket(Card.build("Ac"), Card.build("Kh"));
		PreFlopHand preFlopHand = new PreFlopHand(CardRank.ACE, CardRank.KING, false);
		assertEquals(preFlopHand, pocket.getPreFlopHand());
	}
	
	@Test
	public void testIsFalse() {
		Pocket pocket = new Pocket(Card.build("Ac"), Card.build("Kc"));
		PreFlopHand preFlopHand = new PreFlopHand(CardRank.ACE, CardRank.KING, false);
		assertNotSame(preFlopHand, pocket.getPreFlopHand());
	}
	
	@Test
	public void testIsWithPair() {
		Pocket pocket = new Pocket(Card.build("Ac"), Card.build("Ah"));
		PreFlopHand preFlopHand = new PreFlopHand(CardRank.ACE, CardRank.ACE, false);
		assertEquals(preFlopHand, pocket.getPreFlopHand());
	}
	
	@Test
	public void testIsWithPreFlopUnSorted() {
		Pocket pocket = new Pocket(Card.build("Ac"), Card.build("Kc"));
		PreFlopHand preFlopHand = new PreFlopHand(CardRank.KING, CardRank.ACE, false);
		assertNotSame(preFlopHand, pocket.getPreFlopHand());
	}
	
	@Test
	public void testIsWithPocketUnSorted() {
		Pocket pocket = new Pocket(Card.build("Kc"), Card.build("Ac"));
		PreFlopHand preFlopHand = new PreFlopHand(CardRank.ACE, CardRank.KING, false);
		assertNotSame(preFlopHand, pocket.getPreFlopHand());
	}
	
	@Test
	public void testPocketBuilder() {
		Pocket pocket = new Pocket(Card.build("Ac"), Card.build("Kc"));
		PreFlopHand preFlopHand = new PreFlopHand(pocket);
		assertNotSame(preFlopHand, pocket.getPreFlopHand());
	}
	
	
}
