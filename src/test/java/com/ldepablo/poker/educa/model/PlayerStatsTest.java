package com.ldepablo.poker.educa.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class PlayerStatsTest {
	private PlayerStats playerStats = new PlayerStats();
	private static final double DELTA = 1e-15;
	
	@Test
	public void testGetOpenRaise() {
		
		playerStats.setOpenRaise(33);
		assertEquals(33, playerStats.getOpenRaise(), DELTA);
		
	}
	
	@Test
	public void testGetHands() {
		int hands = 23452;
		playerStats.setHands(hands);
		assertEquals(hands, playerStats.getHands());
	}
}
