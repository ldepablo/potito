package com.ldepablo.poker.educa.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Map;

import org.junit.Test;

public class BoardTest {
	
	
	Flop flop = new Flop(Card.build("3h"), Card.build("4s"), Card.build("5c"));
	
	@Test
	public void testConstructorWithFlop() {
		Board board = new Board(flop);
		assertEquals(flop, board.getFlop());
		assertEquals(3, board.getMaxConnectedCards());
	}
	
	@Test
	public void testConstructorWithFlopAndTurn() {
		
		Board board = new Board(flop, Card.build("6d"));
		
		assertEquals(flop, board.getFlop());
		assertEquals(Card.build("6d"), board.getTurn());
		assertEquals(4, board.getMaxConnectedCards());
	}
	
	@Test
	public void testConstructorWithFlopTurnAndRiver() {
		
		Board board = new Board(flop, Card.build("6d"), Card.build("7h"));
		
		assertEquals(flop, board.getFlop());
		assertEquals(Card.build("6d"), board.getTurn());
		assertEquals(Card.build("7h"), board.getRiver());
		assertEquals(5, board.getMaxConnectedCards());
	}
	
	@Test
	public void testGetCardsInFlop() {
		Board board = new Board(flop);
		assertTrue(flop.getCards().containsAll(board.getCards()));
	}
	
	@Test
	public void testIsExtremelyCoordinatedBecauseOfFlushDraw() {
		
		Board board = new Board(new Flop(Card.build("Ac"), Card.build("2d"), Card.build("3h")), Card.build("5c"));
		assertEquals(3, board.getMaxConnectedCards());

		board = new Board(Card.build("Ac"), Card.build("5c"), Card.build("Kc"), Card.build("Qc"));
		assertEquals(3, board.getMaxConnectedCards());
	}
	
	@Test
	public void testIsExtremelyCoordinatedBecauseOfStraightDrawNoExceptions() {
		Board board = new Board(Card.build("Ac"), Card.build("2d"), Card.build("3h"), Card.build("4s"));
		assertEquals(4, board.getMaxConnectedCards());
		
		board = new Board(Card.build("Ac"), Card.build("2d"), Card.build("5c"), Card.build("4s"));
		assertEquals(2, board.getMaxConnectedCards());

		board = new Board(Card.build("2d"), Card.build("3h"), Card.build("4s"));
		assertEquals(3, board.getMaxConnectedCards());
		
		board = new Board(Card.build("2d"), Card.build("3h"), Card.build("4s"), Card.build("5c"));
		assertEquals(4, board.getMaxConnectedCards());
	}
	
	@Test
	public void testIsExtremelyCoordinatedBecauseOfStraightDrawWithExtreExceptions() {
		Card dA = new Card(CardRank.ACE, Suit.DIAMONDS);
		Card c2 = new Card(CardRank.TWO, Suit.CLUBS);
		Card h3 = new Card(CardRank.THREE, Suit.HEARTS);
		Card sK = new Card(CardRank.KING, Suit.SPADES);
		Card s5 = new Card(CardRank.FIVE, Suit.SPADES);
		Board board = new Board(dA, c2, h3, s5);
		assertEquals(3, board.getMaxConnectedCards());
		
		Card s4 = new Card(CardRank.FOUR, Suit.SPADES);
		board = new Board(dA, c2, h3, sK, s4);
		assertEquals(4, board.getMaxConnectedCards());
		
		board = new Board(c2, h3, s4, s5);
		assertEquals(4, board.getMaxConnectedCards());
		
		Card d6 = new Card(CardRank.SIX, Suit.DIAMONDS);
		Card h7 = new Card(CardRank.SEVEN, Suit.HEARTS);
		Card c8 = new Card(CardRank.EIGHT, Suit.CLUBS);
		Card c9 = new Card(CardRank.NINE, Suit.CLUBS);
		board = new Board(d6, h7, c8, c9);
		assertEquals(4, board.getMaxConnectedCards());
		
		Card sT = new Card(CardRank.TEN, Suit.SPADES);
		board = new Board(d6, h7, c8, sT);
		assertEquals(3, board.getMaxConnectedCards());
		
	}
	
	@Test
	public void testIsCoordinatedBecauseOfStraightDrawWithCoorExceptions() {
		Card card1 = Card.build("Ad");
		Card card2 = Card.build("2c");
		Card card3 = Card.build("3h");
		Card card13 = Card.build("Ks");
		Card card5 = Card.build("5s");
		Board board = new Board(card1, card3, card13, card5);
		assertEquals(2, board.getMaxConnectedCards());
		
		/**
		 * Finally we test situations in which there might be two straight
		 * draws, one being valid and one not. I.e. 2,3,5 can be completed with:
		 * 1. A4 --> LEGAL
		 * 2. 64 --> ILLEGAL
		 */
		board = new Board(card2, card3, card5);
		assertEquals(2, board.getMaxConnectedCards());
	}
	
	
	@Test
	public void testGetTexture() {
		Board board = new Board(Card.build("3h"), Card.build("5c"), Card.build("Ks"));
		assertEquals(BoardTexture.DRY, board.getTexture());

		board = new Board(Card.build("2d"), Card.build("3h"), Card.build("Ks"));
		assertEquals(BoardTexture.DRY, board.getTexture());
		
		board = new Board(Card.build("2d"), Card.build("6d"), Card.build("7h"));
		assertEquals(BoardTexture.SEMICOORDINATED, board.getTexture());
		
		board = new Board(Card.build("5c"), Card.build("6d"), Card.build("7h"));
		assertEquals(BoardTexture.COORDINATED, board.getTexture());

		board = new Board(Card.build("5c"), Card.build("6d"), Card.build("7h"), Card.build("4s"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
	}
	
	@Test
	public void testGetCardsIsImmutable() {
		Board board = new Board(Card.build("3h"), Card.build("5c"), Card.build("Ks"));
		Collection<Card> cards = board.getCards();
		cards.add(Card.build("7h"));
		assertEquals(3, board.getCards().size());
	}
	
	@Test
	public void testIsPaired() {
		Board board = new Board(Card.build("Ac"), Card.build("2d"), Card.build("3h"), Card.build("4s"), Card.build("5c"));
		assertFalse(board.isPaired());
		
		board = new Board(Card.build("Ac"), Card.build("5d"), Card.build("3h"), Card.build("4s"), Card.build("5c"));
		assertTrue(board.isPaired());
	}
	
	@Test
	public void testGetHighestRank() {
		Board board = new Board(Card.build("Ac"), Card.build("2d"), Card.build("3h"), Card.build("4s"), Card.build("5c"));
		assertEquals(CardRank.ACE, board.getHighestRank());
		
		board = new Board(Card.build("2d"), Card.build("3h"), Card.build("4s"), Card.build("5c"));
		assertEquals(CardRank.FIVE, board.getHighestRank());
		
		board = new Board(Card.build("3h"), Card.build("4s"), Card.build("Ks"), Card.build("5c"));
		assertEquals(CardRank.KING, board.getHighestRank());
	}
	
	@Test
	public void testIsDoublePaired() {
		Board board = new Board(Card.build("Ac"), Card.build("2d"), Card.build("3h"), Card.build("4s"), Card.build("5c"));
		assertFalse(board.isDoublePaired());

		board = new Board(Card.build("Ac"), Card.build("2d"), Card.build("3h"), Card.build("4s"), Card.build("3s"));
		assertFalse(board.isDoublePaired());
	
		board = new Board(Card.build("Ac"), Card.build("4h"), Card.build("3h"), Card.build("4s"), Card.build("3s"));
		assertTrue(board.isDoublePaired());
	}
	
	@Test
	public void testGetSuitsMap() {
		Board board = new Board(Card.build("Ac"), Card.build("2d"), Card.build("3h"), Card.build("5c"));
		Map<Suit, Collection<CardRank>> suitsMap = board.getSuitsMap();
		Collection<CardRank> ranks = suitsMap.get(Suit.CLUBS);
		assertEquals(2, ranks.size());
		assertTrue(ranks.contains(CardRank.ACE));
		assertTrue(ranks.contains(CardRank.FIVE));
		
		ranks = suitsMap.get(Suit.DIAMONDS);
		assertEquals(1, ranks.size());
		assertTrue(ranks.contains(CardRank.TWO));
		
		ranks = suitsMap.get(Suit.HEARTS);
		assertEquals(1, ranks.size());
		assertTrue(ranks.contains(CardRank.THREE));

		assertNull(suitsMap.get(Suit.SPADES));
	}
	
	@Test
	public void testGetSuitedCards() {
		Board board = new Board(Card.build("Ac"), Card.build("2d"), Card.build("3h"), Card.build("5s"));
		assertEquals(1, board.getSuitedCards());
		
		board = new Board(Card.build("Ac"), Card.build("2d"), Card.build("3h"), Card.build("5d"));
		assertEquals(2, board.getSuitedCards());
		
		board = new Board(Card.build("Ac"), Card.build("2d"), Card.build("Jd"), Card.build("5d"));
		assertEquals(3, board.getSuitedCards());
		
		board = new Board(Card.build("Ad"), Card.build("2d"), Card.build("Jd"), Card.build("5d"));
		assertEquals(4, board.getSuitedCards());
		
		board = new Board(Card.build("Ad"), Card.build("2d"), Card.build("Jd"), Card.build("5d"), Card.build("8d"));
		assertEquals(5, board.getSuitedCards());
	}
}
