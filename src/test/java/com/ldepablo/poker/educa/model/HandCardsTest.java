package com.ldepablo.poker.educa.model;

import static org.junit.Assert.assertEquals;

import java.util.Collection;

import org.junit.Test;

import com.ldepablo.poker.educa.model.completedhands.CompletedHand;

public class HandCardsTest {
	
	Card cA = Card.build("Ac");
	Card d2 = Card.build("2d");
	Card h3 = Card.build("3h");
	Card s4 = Card.build("4s");
	Card c5 = Card.build("5d");
	Card d6 = Card.build("6d");
	Card h7 = Card.build("7h");
	Card sK = Card.build("Ks");

	Pocket pocket = new Pocket(cA, d2);

	@Test
	public void testPocketConstructorAndGetAll() {
		HandCards handCards = new HandCards(pocket, null);
		assertEquals(handCards.getCards(), pocket.getCards());
	}
	
	@Test
	public void testBoardConstructorAndGetAll() {
		Flop flop = new Flop(h3, s4, c5);
		Board board = new Board(flop);
		HandCards handCards = new HandCards(pocket, board);
		
		Collection<Card> cards = pocket.getCards();
		cards.addAll(board.getCards());
		
		assertEquals(handCards.getCards(), cards);
	}
	
	@Test
	public void testGetCardsIsImmutable() {
		HandCards handCards = new HandCards(pocket, null);
		Collection<Card> cards = handCards.getCards();
		cards.add(Card.build("5h"));
		assertEquals(handCards.getCards(), pocket.getCards());
	}
	
	@Test
	public void testGetPocket() {
		HandCards handCards = new HandCards(pocket, null);
		assertEquals(pocket.getCards(), handCards.getPocketCards());
	}
	
	@Test
	public void testHaveFourOfAKind() {
		Pocket pocket = new Pocket(Card.build("Js"), Card.build("Qs"));
		Flop flop = new Flop(Card.build("Jh"), Card.build("Jc"), Card.build("8h"));
		Board board = new Board(flop, Card.build("Jd"));
		HandCards handCards = new HandCards(pocket, board);
		CompletedHand completedHand = handCards.getCompletedHand();
		assertEquals(GenericHandRank.FOUR_OF_A_KIND, completedHand.getGenericHandRank());
	}
	
	@Test
	public void testFullHouse() {
		Pocket pocket = new Pocket(Card.build("Js"), Card.build("Qs"));
		Flop flop = new Flop(Card.build("Jh"), Card.build("Jc"), Card.build("8h"));
		Board board = new Board(flop, Card.build("Qd"));
		HandCards handCards = new HandCards(pocket, board);
		CompletedHand completedHand = handCards.getCompletedHand();
		assertEquals(GenericHandRank.FULL_HOUSE, completedHand.getGenericHandRank());
	}
	
	@Test
	public void testFullHouseWithTwoThreeOfAindReturnsRightOrder1() {
		Pocket pocket = new Pocket(Card.build("Js"), Card.build("Qs"));
		Flop flop = new Flop(Card.build("Jh"), Card.build("Jc"), Card.build("Qh"));
		Board board = new Board(flop, Card.build("Qd"));
		HandCards handCards = new HandCards(pocket, board);
		CompletedHand completedHand = handCards.getCompletedHand();
		assertEquals(GenericHandRank.FULL_HOUSE, completedHand.getGenericHandRank());
	}
	
	@Test
	public void testFullHouseWithTwoThreeOfAindReturnsRightOrder2() {
		Pocket pocket = new Pocket(Card.build("Qs"), Card.build("Js"));
		Flop flop = new Flop(Card.build("Qh"), Card.build("Qc"), Card.build("Jh"));
		Board board = new Board(flop, Card.build("Jd"));
		HandCards handCards = new HandCards(pocket, board);
		CompletedHand completedHand = handCards.getCompletedHand();
		assertEquals(GenericHandRank.FULL_HOUSE, completedHand.getGenericHandRank());
	}
	
	@Test
	public void testHaveStraightAndFlushButNoStraightFlush() {
		Pocket pocket = new Pocket(Card.build("Js"), Card.build("Qs"));
		Flop flop = new Flop(Card.build("As"), Card.build("Ks"), Card.build("8s"));
		Board board = new Board(flop, Card.build("Th"));
		HandCards handCards = new HandCards(pocket, board);
		CompletedHand completedHand = handCards.getCompletedHand();
		assertEquals(GenericHandRank.FLUSH, completedHand.getGenericHandRank());
	}
	
	@Test
	public void testHaveFlush() {
		Pocket pocket = new Pocket(Card.build("Js"), Card.build("Qs"));
		Flop flop = new Flop(Card.build("Jh"), Card.build("2s"), Card.build("8s"));
		Board board = new Board(flop, Card.build("3s"));
		HandCards handCards = new HandCards(pocket, board);
		CompletedHand completedHand = handCards.getCompletedHand();
		assertEquals(GenericHandRank.FLUSH, completedHand.getGenericHandRank());
	}
	
	@Test
	public void testHaveStraight() {
		Pocket pocket = new Pocket(Card.build("Js"), Card.build("Qc"));
		Flop flop = new Flop(Card.build("Ad"), Card.build("Kh"), Card.build("8s"));
		Board board = new Board(flop, Card.build("Tc"));
		HandCards handCards = new HandCards(pocket, board);
		CompletedHand completedHand = handCards.getCompletedHand();
		assertEquals(GenericHandRank.STRAIGHT, completedHand.getGenericHandRank());
	}
	
	@Test
	public void testHaveThreeOfAKind() {
		Pocket pocket = new Pocket(Card.build("Js"), Card.build("Qs"));
		Flop flop = new Flop(Card.build("Jh"), Card.build("Jc"), Card.build("8h"));
		Board board = new Board(flop, Card.build("3s"));
		HandCards handCards = new HandCards(pocket, board);
		CompletedHand completedHand = handCards.getCompletedHand();
		assertEquals(GenericHandRank.THREE_OF_A_KIND, completedHand.getGenericHandRank());
	}
	
	@Test
	public void testHaveTwoPair() {
		Pocket pocket = new Pocket(Card.build("Js"), Card.build("Qs"));
		Flop flop = new Flop(Card.build("Jh"), Card.build("Kc"), Card.build("8h"));
		Board board = new Board(flop, Card.build("Qc"));
		HandCards handCards = new HandCards(pocket, board);
		CompletedHand completedHand = handCards.getCompletedHand();
		assertEquals(GenericHandRank.TWO_PAIR, completedHand.getGenericHandRank());
	}
	
	@Test
	public void testHavePair() {
		Pocket pocket = new Pocket(Card.build("Js"), Card.build("Qs"));
		Flop flop = new Flop(Card.build("5h"), Card.build("Kc"), Card.build("8h"));
		Board board = new Board(flop, Card.build("Qc"));
		HandCards handCards = new HandCards(pocket, board);
		CompletedHand completedHand = handCards.getCompletedHand();
		assertEquals(GenericHandRank.PAIR, completedHand.getGenericHandRank());
	}
	
	@Test
	public void testHaveHighPair() {
		Pocket pocket = new Pocket(Card.build("Js"), Card.build("Qs"));
		Flop flop = new Flop(Card.build("5h"), Card.build("Kc"), Card.build("8h"));
		Board board = new Board(flop);
		HandCards handCards = new HandCards(pocket, board);
		CompletedHand completedHand = handCards.getCompletedHand();
		assertEquals(GenericHandRank.HIGH_CARD, completedHand.getGenericHandRank());
	}
	
//	@Test
//	public void testGetHandStrengthVeryStrong() {
//		Pocket pocket = new Pocket(Card.build("As"), Card.build("Ah"));
//		Flop flop = new Flop(Card.build("Ad"), Card.build("Kc"), Card.build("Ac"));
//		Board board = new Board(flop);
//		HandCards handCards = new HandCards(pocket, board);
//		assertEquals(HandStrenght.VERY_STRONG, handCards.getStrength());
//	}
}
