package com.ldepablo.poker.educa.model.completedhands;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.ldepablo.poker.educa.model.Board;
import com.ldepablo.poker.educa.model.Card;
import com.ldepablo.poker.educa.model.CardRank;
import com.ldepablo.poker.educa.model.Flop;
import com.ldepablo.poker.educa.model.GenericHandRank;
import com.ldepablo.poker.educa.model.HandCards;
import com.ldepablo.poker.educa.model.Pocket;
import com.ldepablo.poker.educa.model.Suit;

public class CompletedFlushTest {

	@Test
	public void testAceHighFlushWithAceinPocket() {
		Pocket pocket = new Pocket(Card.build("Ac"), Card.build("Kc"));
		Flop flop = new Flop(Card.build("Qc"), Card.build("Jc"), Card.build("Ts"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("9c"), Card.build("8s")));
		CompletedFlush result = CompletedFlush.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(Suit.CLUBS, result.getSuit());
		assertEquals(GenericHandRank.FLUSH, result.getGenericHandRank());
		assertEquals(CardRank.ACE, result.getRank1());
		assertNotNull(result.getRanks());
		assertEquals(CardRank.ACE, result.getRanks()[0]);
		assertEquals(CardRank.KING, result.getRanks()[1]);
		assertEquals(CardRank.QUEEN, result.getRanks()[2]);
		assertEquals(CardRank.JACK, result.getRanks()[3]);
		assertEquals(CardRank.NINE, result.getRanks()[4]);
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertEquals(1, result.getSecondBestPocketPosition().byteValue());
//		assertEquals(0, result.getCardsThatMakeABetterFlush().byteValue());
		assertEquals(0, result.getCardsThatMakeABetterFlush());
	}
	
	@Test
	public void testReturnNullWhenNotPossible() {
		Pocket pocket = new Pocket(Card.build("Ad"), Card.build("3c"));
		Flop flop = new Flop(Card.build("Qc"), Card.build("Jc"), Card.build("Ts"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		CompletedFlush result = CompletedFlush.build(pocket, handCards.getCards());
		assertNull(result);
	}

	@Test
	public void testKingHighFlushWithKingInPocket() {
		Pocket pocket = new Pocket(Card.build("Kh"), Card.build("3h"));
		Flop flop = new Flop(Card.build("Qh"), Card.build("Jh"), Card.build("Ts"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("9h"), Card.build("8s")));
		CompletedFlush result = CompletedFlush.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.FLUSH, result.getGenericHandRank());
		assertEquals(Suit.HEARTS, result.getSuit());
		assertEquals(CardRank.KING, result.getRank1());
		assertNotNull(result.getRanks());
		assertEquals(CardRank.KING, result.getRanks()[0]);
		assertEquals(CardRank.QUEEN, result.getRanks()[1]);
		assertEquals(CardRank.JACK, result.getRanks()[2]);
		assertEquals(CardRank.NINE, result.getRanks()[3]);
		assertEquals(CardRank.THREE, result.getRanks()[4]);	
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertEquals(4, result.getSecondBestPocketPosition().byteValue());
		assertEquals(1, result.getCardsThatMakeABetterFlush());
	}

	@Test
	public void testFlushWithUselessPocket() {
		Pocket pocket = new Pocket(Card.build("Ks"), Card.build("3d"));
		Flop flop = new Flop(Card.build("Qh"), Card.build("4h"), Card.build("Th"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("9h"), Card.build("5h")));
		CompletedFlush result = CompletedFlush.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.FLUSH, result.getGenericHandRank());
		assertEquals(Suit.HEARTS, result.getSuit());
		assertEquals(CardRank.QUEEN, result.getRank1());
		assertNotNull(result.getRanks());
		assertEquals(CardRank.QUEEN, result.getRanks()[0]);
		assertEquals(CardRank.TEN, result.getRanks()[1]);
		assertEquals(CardRank.NINE, result.getRanks()[2]);
		assertEquals(CardRank.FIVE, result.getRanks()[3]);
		assertEquals(CardRank.FOUR, result.getRanks()[4]);	
		assertNull(result.getBestPocketPosition());
		assertNull(result.getSecondBestPocketPosition());
		assertEquals(6, result.getCardsThatMakeABetterFlush());
	}

	@Test
	public void testFlushWithOneHighPocketCard() {
		Pocket pocket = new Pocket(Card.build("Kh"), Card.build("3d"));
		Flop flop = new Flop(Card.build("Qh"), Card.build("4h"), Card.build("Th"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("9s"), Card.build("5h")));
		CompletedFlush result = CompletedFlush.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.FLUSH, result.getGenericHandRank());
		assertEquals(Suit.HEARTS, result.getSuit());
		assertEquals(CardRank.KING, result.getRank1());	
		assertNotNull(result.getRanks());
		assertEquals(CardRank.KING, result.getRanks()[0]);
		assertEquals(CardRank.QUEEN, result.getRanks()[1]);
		assertEquals(CardRank.TEN, result.getRanks()[2]);
		assertEquals(CardRank.FIVE, result.getRanks()[3]);
		assertEquals(CardRank.FOUR, result.getRanks()[4]);
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertNull(result.getSecondBestPocketPosition());
		assertEquals(1, result.getCardsThatMakeABetterFlush());
	}

	@Test
	public void testFlushWithOneMiddlePocketCard() {
		Pocket pocket = new Pocket(Card.build("8h"), Card.build("3d"));
		Flop flop = new Flop(Card.build("Qh"), Card.build("4h"), Card.build("Th"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("9s"), Card.build("5h")));
		CompletedFlush result = CompletedFlush.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.FLUSH, result.getGenericHandRank());
		assertEquals(Suit.HEARTS, result.getSuit());
		assertEquals(CardRank.QUEEN, result.getRank1());	
		assertNotNull(result.getRanks());
		assertEquals(CardRank.QUEEN, result.getRanks()[0]);
		assertEquals(CardRank.TEN, result.getRanks()[1]);
		assertEquals(CardRank.EIGHT, result.getRanks()[2]);
		assertEquals(CardRank.FIVE, result.getRanks()[3]);
		assertEquals(CardRank.FOUR, result.getRanks()[4]);
		assertEquals(2, result.getBestPocketPosition().byteValue());
		assertNull(result.getSecondBestPocketPosition());
		assertEquals(4, result.getCardsThatMakeABetterFlush());
	}

	@Test
	public void testFlushWithOneLowPocketCard() {
		Pocket pocket = new Pocket(Card.build("3h"), Card.build("3d"));
		Flop flop = new Flop(Card.build("Qh"), Card.build("4h"), Card.build("Th"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("9s"), Card.build("5h")));
		CompletedFlush result = CompletedFlush.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.FLUSH, result.getGenericHandRank());
		assertEquals(Suit.HEARTS, result.getSuit());
		assertEquals(CardRank.QUEEN, result.getRank1());
		assertNotNull(result.getRanks());
		assertEquals(CardRank.QUEEN, result.getRanks()[0]);
		assertEquals(CardRank.TEN, result.getRanks()[1]);
		assertEquals(CardRank.FIVE, result.getRanks()[2]);
		assertEquals(CardRank.FOUR, result.getRanks()[3]);
		assertEquals(CardRank.THREE, result.getRanks()[4]);	
		assertEquals(4, result.getBestPocketPosition().byteValue());
		assertNull(result.getSecondBestPocketPosition());
		assertEquals(7, result.getCardsThatMakeABetterFlush());
	}

	@Test
	public void testFlushWithOneLowSuitedButUselessPocketCard() {
		Pocket pocket = new Pocket(Card.build("3h"), Card.build("3d"));
		Flop flop = new Flop(Card.build("Qh"), Card.build("4h"), Card.build("Th"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("9h"), Card.build("5h")));
		CompletedFlush result = CompletedFlush.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.FLUSH, result.getGenericHandRank());
		assertEquals(Suit.HEARTS, result.getSuit());
		assertEquals(CardRank.QUEEN, result.getRank1());	
		assertNull(result.getBestPocketPosition());
		assertNull(result.getSecondBestPocketPosition());
		assertEquals(6, result.getCardsThatMakeABetterFlush());
	}

	@Test
	public void testCompareToSuperiorHand() {
		Pocket pocket = new Pocket(Card.build("3h"), Card.build("3d"));
		Flop flop = new Flop(Card.build("Qh"), Card.build("4h"), Card.build("Th"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("9h"), Card.build("5h")));
		CompletedFlush result = CompletedFlush.build(pocket, handCards.getCards()); 
		
		Pocket pocket2 = new Pocket(Card.build("8c"), Card.build("8d"));
		Flop flop2 = new Flop(Card.build("Jh"), Card.build("Js"), Card.build("7c"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("Jd")));
		CompletedFullHouse superior = CompletedFullHouse.build(pocket2, handCards2.getCards());
		
		assertTrue(result.compareTo(superior) <0);
	}

	@Test
	public void testCompareToInferiorHand() {
		
		Pocket pocket = new Pocket(Card.build("3h"), Card.build("3d"));
		Flop flop = new Flop(Card.build("Qh"), Card.build("4h"), Card.build("Th"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("9h"), Card.build("5h")));
		CompletedFlush result = CompletedFlush.build(pocket, handCards.getCards());
		
		Pocket pocket2 = new Pocket(Card.build("As"), Card.build("Qd"));
		Flop flop2 = new Flop(Card.build("3c"), Card.build("2h"), Card.build("5c"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("4d"), Card.build("6h")));
		CompletedStraight inferior = CompletedStraight.build(pocket2, handCards2.getCards());
		
		assertTrue(result.compareTo(inferior) > 0);
	}

	@Test
	public void testCompareToSuperiorFirstRank() {
		
		Pocket pocket = new Pocket(Card.build("3h"), Card.build("3d"));
		Flop flop = new Flop(Card.build("Qh"), Card.build("4h"), Card.build("Th"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("9h"), Card.build("5h")));
		CompletedFlush result = CompletedFlush.build(pocket, handCards.getCards());
		
		Pocket pocket2 = new Pocket(Card.build("3h"), Card.build("3d"));
		Flop flop2 = new Flop(Card.build("Ah"), Card.build("4h"), Card.build("Th"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("9h"), Card.build("5h")));
		CompletedFlush result2 = CompletedFlush.build(pocket2, handCards2.getCards());
		
		assertTrue(result.compareTo(result2) < 0);
	}

	@Test
	public void testCompareToInferiorFirstRank() {
		
		Pocket pocket = new Pocket(Card.build("3h"), Card.build("3d"));
		Flop flop = new Flop(Card.build("Qh"), Card.build("4h"), Card.build("Th"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("9h"), Card.build("5h")));
		CompletedFlush result = CompletedFlush.build(pocket, handCards.getCards());
		
		Pocket pocket2 = new Pocket(Card.build("3h"), Card.build("3d"));
		Flop flop2 = new Flop(Card.build("Jh"), Card.build("4h"), Card.build("Th"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("9h"), Card.build("5h")));
		CompletedFlush result2 = CompletedFlush.build(pocket2, handCards2.getCards());
		
		assertTrue(result.compareTo(result2) > 0);
	}

	@Test
	public void testCompareToSuperiorSecondRank() {
		
		Pocket pocket = new Pocket(Card.build("3h"), Card.build("3d"));
		Flop flop = new Flop(Card.build("Qh"), Card.build("4h"), Card.build("Th"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("9h"), Card.build("5h")));
		CompletedFlush result = CompletedFlush.build(pocket, handCards.getCards());
		
		Pocket pocket2 = new Pocket(Card.build("3h"), Card.build("3d"));
		Flop flop2 = new Flop(Card.build("Qh"), Card.build("4h"), Card.build("Jh"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("9h"), Card.build("5h")));
		CompletedFlush result2 = CompletedFlush.build(pocket2, handCards2.getCards());
		
		assertTrue(result.compareTo(result2) < 0);
	}

	@Test
	public void testCompareToInferiorSecondRank() {
		
		Pocket pocket = new Pocket(Card.build("3h"), Card.build("3d"));
		Flop flop = new Flop(Card.build("Qh"), Card.build("4h"), Card.build("Jh"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("9h"), Card.build("5h")));
		CompletedFlush result = CompletedFlush.build(pocket, handCards.getCards());
		
		Pocket pocket2 = new Pocket(Card.build("3h"), Card.build("3d"));
		Flop flop2 = new Flop(Card.build("Qh"), Card.build("4h"), Card.build("Th"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("9h"), Card.build("5h")));
		CompletedFlush result2 = CompletedFlush.build(pocket2, handCards2.getCards());
		
		assertTrue(result.compareTo(result2) > 0);
	}

	@Test
	public void testCompareToSuperiorThirdRank() {
		
		Pocket pocket = new Pocket(Card.build("3h"), Card.build("3d"));
		Flop flop = new Flop(Card.build("Qh"), Card.build("4h"), Card.build("Jh"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("9h"), Card.build("5h")));
		CompletedFlush result = CompletedFlush.build(pocket, handCards.getCards());
		
		Pocket pocket2 = new Pocket(Card.build("3h"), Card.build("3d"));
		Flop flop2 = new Flop(Card.build("Qh"), Card.build("4h"), Card.build("Jh"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("Th"), Card.build("5h")));
		CompletedFlush result2 = CompletedFlush.build(pocket2, handCards2.getCards());
		
		assertTrue(result.compareTo(result2) < 0);
	}

	@Test
	public void testCompareToInferiorThirdRank() {
		
		Pocket pocket = new Pocket(Card.build("3h"), Card.build("3d"));
		Flop flop = new Flop(Card.build("Qh"), Card.build("4h"), Card.build("Jh"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Th"), Card.build("5h")));
		CompletedFlush result = CompletedFlush.build(pocket, handCards.getCards());
		
		Pocket pocket2 = new Pocket(Card.build("3h"), Card.build("3d"));
		Flop flop2 = new Flop(Card.build("Qh"), Card.build("4h"), Card.build("Jh"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("9h"), Card.build("5h")));
		CompletedFlush result2 = CompletedFlush.build(pocket2, handCards2.getCards());
		
		assertTrue(result.compareTo(result2) > 0);
	}

	@Test
	public void testCompareToSuperiorFourthRank() {
		
		Pocket pocket = new Pocket(Card.build("3h"), Card.build("3d"));
		Flop flop = new Flop(Card.build("Qh"), Card.build("4h"), Card.build("Jh"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Th"), Card.build("5h")));
		CompletedFlush result = CompletedFlush.build(pocket, handCards.getCards());
		
		Pocket pocket2 = new Pocket(Card.build("3h"), Card.build("3d"));
		Flop flop2 = new Flop(Card.build("Qh"), Card.build("4h"), Card.build("Jh"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("Th"), Card.build("6h")));
		CompletedFlush result2 = CompletedFlush.build(pocket2, handCards2.getCards());
		
		assertTrue(result.compareTo(result2) < 0);
	}

	@Test
	public void testCompareToInferiorFourthRank() {
		
		Pocket pocket = new Pocket(Card.build("3h"), Card.build("3d"));
		Flop flop = new Flop(Card.build("Qh"), Card.build("4h"), Card.build("Jh"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Th"), Card.build("6h")));
		CompletedFlush result = CompletedFlush.build(pocket, handCards.getCards());
		
		Pocket pocket2 = new Pocket(Card.build("3h"), Card.build("3d"));
		Flop flop2 = new Flop(Card.build("Qh"), Card.build("4h"), Card.build("Jh"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("Th"), Card.build("5h")));
		CompletedFlush result2 = CompletedFlush.build(pocket2, handCards2.getCards());
		
		assertTrue(result.compareTo(result2) > 0);
	}

	@Test
	public void testCompareToSuperiorFifthRank() {
		
		Pocket pocket = new Pocket(Card.build("3h"), Card.build("3d"));
		Flop flop = new Flop(Card.build("Qh"), Card.build("4h"), Card.build("Jh"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Th"), Card.build("6h")));
		CompletedFlush result = CompletedFlush.build(pocket, handCards.getCards());
		
		Pocket pocket2 = new Pocket(Card.build("3h"), Card.build("3d"));
		Flop flop2 = new Flop(Card.build("Qh"), Card.build("5h"), Card.build("Jh"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("Th"), Card.build("6h")));
		CompletedFlush result2 = CompletedFlush.build(pocket2, handCards2.getCards());
		
		assertTrue(result.compareTo(result2) < 0);
	}

	@Test
	public void testCompareToInferiorFifthRank() {
		
		Pocket pocket = new Pocket(Card.build("3h"), Card.build("3d"));
		Flop flop = new Flop(Card.build("Qh"), Card.build("5h"), Card.build("Jh"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Th"), Card.build("6h")));
		CompletedFlush result = CompletedFlush.build(pocket, handCards.getCards());
		
		Pocket pocket2 = new Pocket(Card.build("3h"), Card.build("3d"));
		Flop flop2 = new Flop(Card.build("Qh"), Card.build("4h"), Card.build("Jh"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("Th"), Card.build("6h")));
		CompletedFlush result2 = CompletedFlush.build(pocket2, handCards2.getCards());
		
		assertTrue(result.compareTo(result2) > 0);
	}

	@Test
	public void testCompareToSuperiorUselessSixthRank() {
		
		Pocket pocket = new Pocket(Card.build("3h"), Card.build("3d"));
		Flop flop = new Flop(Card.build("Qh"), Card.build("5h"), Card.build("Jh"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Th"), Card.build("6h")));
		CompletedFlush result = CompletedFlush.build(pocket, handCards.getCards());
		
		Pocket pocket2 = new Pocket(Card.build("4h"), Card.build("3d"));
		Flop flop2 = new Flop(Card.build("Qh"), Card.build("5h"), Card.build("Jh"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("Th"), Card.build("6h")));
		CompletedFlush result2 = CompletedFlush.build(pocket2, handCards2.getCards());
		
		assertEquals(0, result.compareTo(result2));
	}

	@Test
	public void testCompareToInferiorUselessSixthRank() {
		
		Pocket pocket = new Pocket(Card.build("4h"), Card.build("3d"));
		Flop flop = new Flop(Card.build("Qh"), Card.build("5h"), Card.build("Jh"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Th"), Card.build("6h")));
		CompletedFlush result = CompletedFlush.build(pocket, handCards.getCards());
		
		Pocket pocket2 = new Pocket(Card.build("3h"), Card.build("3d"));
		Flop flop2 = new Flop(Card.build("Qh"), Card.build("5h"), Card.build("Jh"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("Th"), Card.build("6h")));
		CompletedFlush result2 = CompletedFlush.build(pocket2, handCards2.getCards());
		
		assertEquals(0, result.compareTo(result2));
	}
}
