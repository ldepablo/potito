package com.ldepablo.poker.educa.model.completedhands;

import static org.junit.Assert.*;

import org.junit.Test;

import com.ldepablo.poker.educa.model.Board;
import com.ldepablo.poker.educa.model.Card;
import com.ldepablo.poker.educa.model.CardRank;
import com.ldepablo.poker.educa.model.Flop;
import com.ldepablo.poker.educa.model.GenericHandRank;
import com.ldepablo.poker.educa.model.HandCards;
import com.ldepablo.poker.educa.model.Pocket;

public class CompletedStraightTest {

	@Test
	public void testNoPossibleStraightReturnsNull() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Qd"));
		Flop flop = new Flop(Card.build("Tc"), Card.build("8h"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		assertNull(CompletedStraight.build(pocket, handCards.getCards()));
	}

	@Test
	public void testStraight_AceInPocket() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Qd"));
		Flop flop = new Flop(Card.build("Kc"), Card.build("Jh"), Card.build("Tc"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		CompletedStraight result = CompletedStraight.build(pocket, handCards.getCards());
		assertEquals(GenericHandRank.STRAIGHT, result.getGenericHandRank());
		assertEquals(CardRank.ACE , result.getHighestRank());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertEquals(2, result.getSecondBestPocketPosition().byteValue());
		assertEquals(1, result.getSuperiors());
	}

	@Test
	public void testAceHighStraightKingInPocket() {
		Pocket pocket = new Pocket(Card.build("Ks"), Card.build("Qd"));
		Flop flop = new Flop(Card.build("Ac"), Card.build("Jh"), Card.build("Tc"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		CompletedStraight result = CompletedStraight.build(pocket, handCards.getCards());
		assertEquals(GenericHandRank.STRAIGHT, result.getGenericHandRank());
		assertEquals(CardRank.ACE , result.getHighestRank());
		assertEquals(1, result.getBestPocketPosition().byteValue());
		assertEquals(2, result.getSecondBestPocketPosition().byteValue());
		assertEquals(0, result.getSuperiors());
	}

	@Test
	public void testAceHighStraightKingPlusUselessInPocket() {
		Pocket pocket = new Pocket(Card.build("Ks"), Card.build("3d"));
		Flop flop = new Flop(Card.build("Ac"), Card.build("Jh"), Card.build("Tc"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Qd")));
		CompletedStraight result = CompletedStraight.build(pocket, handCards.getCards());
		assertEquals(GenericHandRank.STRAIGHT, result.getGenericHandRank());
		assertEquals(CardRank.ACE , result.getHighestRank());
		assertEquals(1, result.getBestPocketPosition().byteValue());
		assertNull(result.getSecondBestPocketPosition());
		assertEquals(0, result.getSuperiors());
	}

	@Test
	public void testKingHighStraightKingPlusUselessInPocket() {
		Pocket pocket = new Pocket(Card.build("Ks"), Card.build("3d"));
		Flop flop = new Flop(Card.build("9c"), Card.build("Jh"), Card.build("Tc"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Qd")));
		CompletedStraight result = CompletedStraight.build(pocket, handCards.getCards());
		assertEquals(GenericHandRank.STRAIGHT, result.getGenericHandRank());
		assertEquals(CardRank.KING , result.getHighestRank());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertNull(result.getSecondBestPocketPosition());
		assertEquals(1, result.getSuperiors());
	}

	@Test
	public void testKingHighStraightWithUselessPocket() {
		Pocket pocket = new Pocket(Card.build("4s"), Card.build("3d"));
		Flop flop = new Flop(Card.build("9c"), Card.build("Jh"), Card.build("Tc"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Qd"), Card.build("Kh")));
		CompletedStraight result = CompletedStraight.build(pocket, handCards.getCards());
		assertEquals(GenericHandRank.STRAIGHT, result.getGenericHandRank());
		assertEquals(CardRank.KING , result.getHighestRank());
		assertNull(result.getBestPocketPosition());
		assertNull(result.getSecondBestPocketPosition());
		assertEquals(0, result.getSuperiors());
	}

	@Test
	public void testKingHighStraightWithAUselessPocket() {
		Pocket pocket = new Pocket(Card.build("9s"), Card.build("3d"));
		Flop flop = new Flop(Card.build("4c"), Card.build("Jh"), Card.build("Tc"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Qd"), Card.build("Kh")));
		CompletedStraight result = CompletedStraight.build(pocket, handCards.getCards());
		assertEquals(GenericHandRank.STRAIGHT, result.getGenericHandRank());
		assertEquals(CardRank.KING , result.getHighestRank());
		assertEquals(4, result.getBestPocketPosition().byteValue());
		assertNull(result.getSecondBestPocketPosition());
		assertEquals(0, result.getSuperiors());
	}

	@Test
	public void testStraightPocketUsefulCardExistsOnBoardWithAUselessPocket() {
		Pocket pocket = new Pocket(Card.build("9s"), Card.build("3d"));
		Flop flop = new Flop(Card.build("9c"), Card.build("Jh"), Card.build("Tc"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Qd"), Card.build("Kh")));
		CompletedStraight result = CompletedStraight.build(pocket, handCards.getCards());
		assertEquals(GenericHandRank.STRAIGHT, result.getGenericHandRank());
		assertEquals(CardRank.KING , result.getHighestRank());
		assertNull(result.getBestPocketPosition());
		assertNull(result.getSecondBestPocketPosition());
		assertEquals(0, result.getSuperiors());
	}

	@Test
	public void testStraightSecondPocketUsefulCardExistsOnBoardWithAUselessPocket() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Qd"));
		Flop flop = new Flop(Card.build("9c"), Card.build("Jh"), Card.build("Tc"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Qs"), Card.build("Kh")));
		CompletedStraight result = CompletedStraight.build(pocket, handCards.getCards());
		assertEquals(CardRank.ACE , result.getHighestRank());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertNull(result.getSecondBestPocketPosition());
		assertEquals(1, result.getSuperiors());
	}

	@Test
	public void test5HighStraightAceInPocket() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Qd"));
		Flop flop = new Flop(Card.build("3c"), Card.build("2h"), Card.build("5c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("4d"), Card.build("Kh")));
		CompletedStraight result = CompletedStraight.build(pocket, handCards.getCards());
		assertEquals(CardRank.FIVE , result.getHighestRank());
		assertEquals(4, result.getBestPocketPosition().byteValue());
		assertNull(result.getSecondBestPocketPosition());
		assertEquals(0, result.getSuperiors());
	}

	@Test
	public void test5HighStraightUselessPocket() {
		Pocket pocket = new Pocket(Card.build("Ks"), Card.build("Qd"));
		Flop flop = new Flop(Card.build("3c"), Card.build("2h"), Card.build("5c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("4d"), Card.build("Ah")));
		CompletedStraight result = CompletedStraight.build(pocket, handCards.getCards());
		assertEquals(CardRank.FIVE , result.getHighestRank());
		assertNull(result.getBestPocketPosition());
		assertNull(result.getSecondBestPocketPosition());
		assertEquals(0, result.getSuperiors());
	}

	@Test
	public void test6HighStraightAceInPocket() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Qd"));
		Flop flop = new Flop(Card.build("3c"), Card.build("2h"), Card.build("5c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("4d"), Card.build("6h")));
		CompletedStraight result = CompletedStraight.build(pocket, handCards.getCards());
		assertEquals(CardRank.SIX , result.getHighestRank());
		assertNull(result.getBestPocketPosition());
		assertNull(result.getSecondBestPocketPosition());
		assertEquals(0, result.getSuperiors());
	}

	@Test
	public void testCompareWithSuperiorHand() {
		Pocket pocket = new Pocket(Card.build("4s"), Card.build("3d"));
		Flop flop = new Flop(Card.build("9c"), Card.build("Jh"), Card.build("Tc"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Qd"), Card.build("Kh")));
		CompletedStraight straight = CompletedStraight.build(pocket, handCards.getCards());

		Pocket pocket2 = new Pocket(Card.build("4h"), Card.build("3d"));
		Flop flop2 = new Flop(Card.build("Qh"), Card.build("5h"), Card.build("Jh"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("Th"), Card.build("6h")));
		CompletedFlush flush = CompletedFlush.build(pocket2, handCards2.getCards());
		
		assertTrue(straight.compareTo(flush) < 0);
	}

	@Test
	public void testCompareWithInferiorHand() {
		Pocket pocket1 = new Pocket(Card.build("4s"), Card.build("3d"));
		Flop flop1 = new Flop(Card.build("9c"), Card.build("Jh"), Card.build("Tc"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("Qd"), Card.build("Kh")));
		CompletedStraight straight = CompletedStraight.build(pocket1, handCards1.getCards());

		Pocket pocket2 = new Pocket(Card.build("3s"), Card.build("Kd"));
		Flop flop2 = new Flop(Card.build("As"), Card.build("Ah"), Card.build("Ac"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("Qc")));
		CompletedThreeOfAKind inferior = CompletedThreeOfAKind.build(pocket2, handCards2.getCards());
		
		assertTrue(straight.compareTo(inferior) > 0);
	}

	@Test
	public void testSameRankStraightsAreEqual() {
		Pocket pocket1 = new Pocket(Card.build("4s"), Card.build("2d"));
		Flop flop1 = new Flop(Card.build("9c"), Card.build("Jh"), Card.build("Tc"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("Qd"), Card.build("Kh")));
		CompletedStraight straight1 = CompletedStraight.build(pocket1, handCards1.getCards());
		
		Pocket pocket2 = new Pocket(Card.build("8s"), Card.build("3d"));
		Flop flop2 = new Flop(Card.build("9c"), Card.build("Jh"), Card.build("Tc"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("Qd"), Card.build("Kh")));
		CompletedStraight straight2 = CompletedStraight.build(pocket2, handCards2.getCards());

		assertTrue(straight1.compareTo(straight2) == 0);
		assertTrue(straight2.compareTo(straight1) == 0);
	}

	@Test
	public void testDifferentStraightsAreOK() {
		Pocket pocket1 = new Pocket(Card.build("As"), Card.build("2d"));
		Flop flop1 = new Flop(Card.build("9c"), Card.build("Jh"), Card.build("Tc"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("Qd"), Card.build("Kh")));
		CompletedStraight straight1 = CompletedStraight.build(pocket1, handCards1.getCards());
		
		Pocket pocket = new Pocket(Card.build("Ks"), Card.build("Qd"));
		Flop flop = new Flop(Card.build("3c"), Card.build("2h"), Card.build("5c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("4d"), Card.build("Ah")));
		CompletedStraight straight2 = CompletedStraight.build(pocket, handCards.getCards());

		assertTrue(straight1.compareTo(straight2) > 0);
		assertTrue(straight2.compareTo(straight1) < 0);
	}
	
	@Test
	public void testSuperior_6high() {
		Pocket pocket = new Pocket(Card.build("6s"), Card.build("5d"));
		Flop flop = new Flop(Card.build("3c"), Card.build("2h"), Card.build("Qc"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("4d"), Card.build("Kh")));
		CompletedStraight result = CompletedStraight.build(pocket, handCards.getCards());
		assertEquals(2, result.getSuperiors());
	}
	
	@Test
	public void testSuperior_6high_HighestCommonRankEqual() {
		Pocket pocket = new Pocket(Card.build("6s"), Card.build("5d"));
		Flop flop = new Flop(Card.build("3c"), Card.build("2h"), Card.build("Qc"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("4d"), Card.build("6h")));
		CompletedStraight result = CompletedStraight.build(pocket, handCards.getCards());
		assertEquals(0, result.getSuperiors());
	}
	
	@Test
	public void testSuperior_6high_SecondHighestCommonRankEqual() {
		Pocket pocket = new Pocket(Card.build("6s"), Card.build("5d"));
		Flop flop = new Flop(Card.build("3c"), Card.build("2h"), Card.build("Qc"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("4d"), Card.build("5h")));
		CompletedStraight result = CompletedStraight.build(pocket, handCards.getCards());
		assertEquals(1, result.getSuperiors());
	}
	
	@Test
	public void testSuperior_KingHigh() {
		Pocket pocket = new Pocket(Card.build("Ks"), Card.build("Qd"));
		Flop flop = new Flop(Card.build("Jc"), Card.build("2h"), Card.build("9c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("4d"), Card.build("Th")));
		CompletedStraight result = CompletedStraight.build(pocket, handCards.getCards());
		assertEquals(2, result.getSuperiors());
		
	}
}
