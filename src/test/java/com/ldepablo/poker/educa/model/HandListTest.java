package com.ldepablo.poker.educa.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class HandListTest {

	@Test
	public void testIsIn() {
		HandList list = new HandList();
		Pocket pocket = new Pocket(Card.build("Ac"), Card.build("5c"));
		list.add(pocket.getPreFlopHand());
		assertTrue(list.contains(pocket.getPreFlopHand()));
		
		Pocket pocket2 = new Pocket(Card.build("Ac"), Card.build("4c"));
		assertFalse(list.contains(pocket2.getPreFlopHand()));
	}
}
