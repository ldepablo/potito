package com.ldepablo.poker.educa.model;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class GenericHandRankTest {
	
	@Test
	public void testHandsOrderIsCorrect() {
		assertTrue(GenericHandRank.STRAIGHT_FLUSH.compareTo(GenericHandRank.FOUR_OF_A_KIND) > 0);
		assertTrue(GenericHandRank.FOUR_OF_A_KIND.compareTo(GenericHandRank.FULL_HOUSE) > 0);
		assertTrue(GenericHandRank.FULL_HOUSE.compareTo(GenericHandRank.FLUSH) > 0);
		assertTrue(GenericHandRank.FLUSH.compareTo(GenericHandRank.STRAIGHT) > 0);
		assertTrue(GenericHandRank.STRAIGHT.compareTo(GenericHandRank.THREE_OF_A_KIND) > 0);
		assertTrue(GenericHandRank.THREE_OF_A_KIND.compareTo(GenericHandRank.TWO_PAIR) > 0);
		assertTrue(GenericHandRank.TWO_PAIR.compareTo(GenericHandRank.PAIR) > 0);
		assertTrue(GenericHandRank.PAIR.compareTo(GenericHandRank.HIGH_CARD) > 0);
	}
}
