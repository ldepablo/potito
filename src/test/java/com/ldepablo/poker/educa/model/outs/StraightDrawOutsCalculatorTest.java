package com.ldepablo.poker.educa.model.outs;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.ldepablo.poker.educa.model.Board;
import com.ldepablo.poker.educa.model.Card;
import com.ldepablo.poker.educa.model.Flop;
import com.ldepablo.poker.educa.model.Pocket;

public class StraightDrawOutsCalculatorTest {

	private static final double DELTA = 1e-15;

	@Test
	public void testGetOutsFor_22OpenEndedS8D_NoPair_NoFD(){
		Pocket pocket = new Pocket(Card.build("Js"), Card.build("Tc"));
		Flop flop = new Flop(Card.build("Qd"), Card.build("9h"), Card.build("2s"));
		Board board = new Board(flop);
		StraightDrawOutsCalculator calculator = new StraightDrawOutsCalculator(pocket, board);
		assertEquals(8, calculator.getOutsFor22S8Draw(), DELTA);
	}

	@Test
	public void testGetOutsFor_22OpenEndedS8D_NoPair_FDInBoard(){
		Pocket pocket = new Pocket(Card.build("Js"), Card.build("Tc"));
		Flop flop = new Flop(Card.build("Qd"), Card.build("9d"), Card.build("2s"));
		Board board = new Board(flop);
		StraightDrawOutsCalculator calculator = new StraightDrawOutsCalculator(pocket, board);
		assertEquals(6, calculator.getOutsFor22S8Draw(), DELTA);
	}

	@Test
	public void testGetOutsFor_22OpenEndedS8D_PairInBoard_NoFD(){
		Pocket pocket = new Pocket(Card.build("Js"), Card.build("Tc"));
		Flop flop = new Flop(Card.build("Qd"), Card.build("9h"), Card.build("Qs"));
		Board board = new Board(flop);
		StraightDrawOutsCalculator calculator = new StraightDrawOutsCalculator(pocket, board);
		assertEquals(6, calculator.getOutsFor22S8Draw(), DELTA);
	}

	@Test
	public void testGetOutsFor_22OpenEndedS8D_NotPossibleToDoS8WithPocketCards(){
		Pocket pocket = new Pocket(Card.build("Js"), Card.build("5c"));
		Flop flop = new Flop(Card.build("Ad"), Card.build("8d"), Card.build("2s"));
		Board board = new Board(flop);
		StraightDrawOutsCalculator calculator = new StraightDrawOutsCalculator(pocket, board);
		assertEquals(0, calculator.getOutsFor22S8Draw(), DELTA);
	}

	@Test
	public void testGetOutsFor_22OpenEndedS8D_NoS8D(){
		Pocket pocket = new Pocket(Card.build("Js"), Card.build("Qc"));
		Flop flop = new Flop(Card.build("6d"), Card.build("8d"), Card.build("2s"));
		Board board = new Board(flop);
		StraightDrawOutsCalculator calculator = new StraightDrawOutsCalculator(pocket, board);
		assertEquals(0, calculator.getOutsFor22S8Draw(), DELTA);
	}
	
	@Test
	public void testGetOutsFor_22S8_NoDraw() {
		Pocket pocket = new Pocket(Card.build("Js"), Card.build("Qc"));
		Flop flop = new Flop(Card.build("6d"), Card.build("8d"), Card.build("2s"));
		Board board = new Board(flop);
		assertEquals(0, new StraightDrawOutsCalculator(pocket, board).calculate(), DELTA);
	}
	
	@Test
	public void testGetOutsFor_22S8_OESD_011110() {
		Pocket pocket = new Pocket(Card.build("Js"), Card.build("Qc"));
		Flop flop = new Flop(Card.build("Td"), Card.build("9d"), Card.build("2s"));
		Board board = new Board(flop);
		assertEquals(8, new StraightDrawOutsCalculator(pocket, board).calculate(), DELTA);
		
	}
}
