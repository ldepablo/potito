package com.ldepablo.poker.educa.model;

import static org.junit.Assert.*;

import org.junit.Test;

import com.ldepablo.poker.educa.model.completedhands.CompletedFlush;
import com.ldepablo.poker.educa.model.completedhands.CompletedFullHouse;
import com.ldepablo.poker.educa.model.completedhands.CompletedHand;
import com.ldepablo.poker.educa.model.completedhands.CompletedStraight;

public class StrengthCalculatorTest {

	
	@Test
	public void testGetHandStrengthNothing_DryBoard_HighCard() {
		Pocket pocket = new Pocket(Card.build("Ks"), Card.build("2s"));
		Flop flop = new Flop(Card.build("3h"), Card.build("Qc"), Card.build("8d"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.DRY, board.getTexture());
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.HIGH_CARD, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.NOTHING, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrengthNothing_SemiBoard_HighCard() {
		Pocket pocket = new Pocket(Card.build("Ts"), Card.build("2s"));
		Flop flop = new Flop(Card.build("3h"), Card.build("Kc"), Card.build("Qh"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.SEMICOORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.HIGH_CARD, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.NOTHING, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrengthNothing_CoorBoard_HighCard() {
		Pocket pocket = new Pocket(Card.build("Ts"), Card.build("2s"));
		Flop flop = new Flop(Card.build("Jh"), Card.build("Kc"), Card.build("Qh"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.HIGH_CARD, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.NOTHING, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrengthNothing_ExtreBoard_HighCard() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("2s"));
		Flop flop = new Flop(Card.build("Jh"), Card.build("Kc"), Card.build("Qh"));
		Board board = new Board(flop, Card.build("9s"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.HIGH_CARD, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.NOTHING, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrengthWeak_DryBoard_AceHigh() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("2s"));
		Flop flop = new Flop(Card.build("3h"), Card.build("Kc"), Card.build("8h"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.DRY, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.HIGH_CARD, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.WEAK, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrengthWeak_SemiBoard_AceHigh() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("2s"));
		Flop flop = new Flop(Card.build("3h"), Card.build("Qc"), Card.build("8h"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.SEMICOORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.HIGH_CARD, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.WEAK, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrengthWeak_CoorBoard_AceHigh() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("2s"));
		Flop flop = new Flop(Card.build("9h"), Card.build("Qc"), Card.build("8h"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.HIGH_CARD, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.WEAK, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrengthNothing_ExtreBoard_AceHigh() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("2s"));
		Flop flop = new Flop(Card.build("9h"), Card.build("Qc"), Card.build("8h"));
		Board board = new Board(flop, Card.build("Js"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.HIGH_CARD, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.NOTHING, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrengthWeak_DryBoard_PairUnderMidPair() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("3s"));
		Flop flop = new Flop(Card.build("3h"), Card.build("Kc"), Card.build("8h"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.DRY, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.WEAK, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrengthWeak_SemiBoard_PairUnderMidPair() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("3s"));
		Flop flop = new Flop(Card.build("3h"), Card.build("Kc"), Card.build("Th"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.SEMICOORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.WEAK, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrengthWeak_CoorBoard_PairUnderMidPair() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Js"));
		Flop flop = new Flop(Card.build("Jh"), Card.build("Kc"), Card.build("Th"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.WEAK, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrengthWeak_ExtreBoard_PairUnderMidPair() {
		Pocket pocket = new Pocket(Card.build("5s"), Card.build("3s"));
		Flop flop = new Flop(Card.build("3h"), Card.build("Kc"), Card.build("Th"));
		Board board = new Board(flop, Card.build("Qd"), Card.build("Jd"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.NOTHING, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrengthWeak_DryBoard_PairMidPair_WorseThanSecondKicker() {
		Pocket pocket = new Pocket(Card.build("Js"), Card.build("8s"));
		Flop flop = new Flop(Card.build("3h"), Card.build("Kc"), Card.build("8h"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.DRY, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.WEAK, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrengthWeak_SemiBoard_PairMidPair_WorseThanSecondKicker() {
		Pocket pocket = new Pocket(Card.build("Js"), Card.build("Ts"));
		Flop flop = new Flop(Card.build("3h"), Card.build("Kc"), Card.build("Th"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.SEMICOORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.WEAK, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrengthWeak_DryBoard_PairMidPair_SecondBestKicker() {
		Pocket pocket = new Pocket(Card.build("Qs"), Card.build("8s"));
		Flop flop = new Flop(Card.build("3h"), Card.build("Kc"), Card.build("8h"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.DRY, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.MEDIUM, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrengthWeak_SemiBoard_PairMidPair_SecondBestKicker() {
		Pocket pocket = new Pocket(Card.build("Qs"), Card.build("Ts"));
		Flop flop = new Flop(Card.build("3h"), Card.build("Kc"), Card.build("Th"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.SEMICOORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.MEDIUM, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrengthWeak_CoorBoard_PairMidPair_SecondBestKicker() {
		Pocket pocket = new Pocket(Card.build("Qs"), Card.build("Ts"));
		Flop flop = new Flop(Card.build("9h"), Card.build("Kc"), Card.build("Th"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.WEAK, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrengthWeak_ExtreBoard_PairMidPair_SecondBestKicker() {
		Pocket pocket = new Pocket(Card.build("Qs"), Card.build("Ts"));
		Flop flop = new Flop(Card.build("9c"), Card.build("Kc"), Card.build("Tc"));
		Board board = new Board(flop, Card.build("3c"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.NOTHING, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrengthWeak_NonPairedDryBoard_PairTopPair_BadKicker() {
		Pocket pocket = new Pocket(Card.build("2s"), Card.build("Ks"));
		Flop flop = new Flop(Card.build("3h"), Card.build("Kc"), Card.build("8h"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.DRY, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.MEDIUM, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_NonPairedSemiBoard_TopPair_BadKicker() {
		Pocket pocket = new Pocket(Card.build("2s"), Card.build("Ks"));
		Flop flop = new Flop(Card.build("3h"), Card.build("Kc"), Card.build("Th"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.SEMICOORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.MEDIUM, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_NonPairedCoorBoard_TopPair_BadKicker() {
		Pocket pocket = new Pocket(Card.build("2s"), Card.build("Kd"));
		Flop flop = new Flop(Card.build("Jh"), Card.build("Kc"), Card.build("Ts"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.MEDIUM, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_NonPairedExtreBoard_TopPair_BadKicker() {
		Pocket pocket = new Pocket(Card.build("2s"), Card.build("Kd"));
		Flop flop = new Flop(Card.build("Jh"), Card.build("Kc"), Card.build("Ts"));
		Board board = new Board(flop, Card.build("Qs"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.NOTHING, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_NonPairedDryBoard_PairTopPair_SecondKicker() {
		Pocket pocket = new Pocket(Card.build("Qs"), Card.build("Ks"));
		Flop flop = new Flop(Card.build("3h"), Card.build("Kc"), Card.build("8h"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.DRY, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_NonPairedSemiBoard_PairTopPair_SecondKicker() {
		Pocket pocket = new Pocket(Card.build("Qs"), Card.build("Ks"));
		Flop flop = new Flop(Card.build("3h"), Card.build("Kc"), Card.build("Th"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.SEMICOORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_NonPairedCoorBoard_PairTopPair_SecondKicker() {
		Pocket pocket = new Pocket(Card.build("Qs"), Card.build("Ks"));
		Flop flop = new Flop(Card.build("Jh"), Card.build("Kc"), Card.build("Th"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.MEDIUM, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_NonPairedExtreBoard_PairTopPair_SecondKicker() {
		Pocket pocket = new Pocket(Card.build("Qs"), Card.build("Ks"));
		Flop flop = new Flop(Card.build("Jd"), Card.build("Kd"), Card.build("Td"));
		Board board = new Board(flop, Card.build("3d"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.NOTHING, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_PairedDryBoard_PairTopPair_SecondKicker() {
		Pocket pocket = new Pocket(Card.build("Qs"), Card.build("4s"));
		Flop flop = new Flop(Card.build("8s"), Card.build("Kc"), Card.build("Kh"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.DRY, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.MEDIUM, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_PairedSemiBoard_PairTopPair_SecondKicker() {
		Pocket pocket = new Pocket(Card.build("Qs"), Card.build("Ks"));
		Flop flop = new Flop(Card.build("Ts"), Card.build("9c"), Card.build("Th"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.SEMICOORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.MEDIUM, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_PairedCoorBoard_PairTopPair_SecondKicker() {
		Pocket pocket = new Pocket(Card.build("Qs"), Card.build("Ks"));
		Flop flop = new Flop(Card.build("Ts"), Card.build("9c"), Card.build("Th"));
		Board board = new Board(flop, Card.build("8c"));
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.MEDIUM, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_PairedExtreBoard_PairTopPair_SecondKicker() {
		Pocket pocket = new Pocket(Card.build("Qs"), Card.build("Ks"));
		Flop flop = new Flop(Card.build("Ts"), Card.build("9c"), Card.build("Th"));
		Board board = new Board(flop, Card.build("8c"), Card.build("7d"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.NOTHING, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_NonPairedDryBoard_Overpair() {
		Pocket pocket = new Pocket(Card.build("Kh"), Card.build("Ks"));
		Flop flop = new Flop(Card.build("8s"), Card.build("Qc"), Card.build("3h"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.DRY, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_NonPairedSemiBoard_Overpair() {
		Pocket pocket = new Pocket(Card.build("Kh"), Card.build("Ks"));
		Flop flop = new Flop(Card.build("9s"), Card.build("Qc"), Card.build("3h"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.SEMICOORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_NonPairedCoorBoard_Overpair() {
		Pocket pocket = new Pocket(Card.build("Kh"), Card.build("Kc"));
		Flop flop = new Flop(Card.build("9s"), Card.build("Qs"), Card.build("3s"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.MEDIUM, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_NonPairedExtreBoard_Overpair() {
		Pocket pocket = new Pocket(Card.build("Kh"), Card.build("Kc"));
		Flop flop = new Flop(Card.build("9s"), Card.build("Qs"), Card.build("3s"));
		Board board = new Board(flop, Card.build("5s"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.NOTHING, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_NonPairedDryBoard_TwoPair() {
		Pocket pocket = new Pocket(Card.build("Kh"), Card.build("3s"));
		Flop flop = new Flop(Card.build("Ks"), Card.build("5c"), Card.build("3h"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.DRY, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.TWO_PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_PairedDryBoard_TwoPair() {
		Pocket pocket = new Pocket(Card.build("Kh"), Card.build("5s"));
		Flop flop = new Flop(Card.build("Ks"), Card.build("3c"), Card.build("3h"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.DRY, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.TWO_PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.MEDIUM, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_NonPairedDryBoard_BestTwoPair() {
		Pocket pocket = new Pocket(Card.build("Kh"), Card.build("5s"));
		Flop flop = new Flop(Card.build("Ks"), Card.build("5c"), Card.build("3h"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.DRY, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.TWO_PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.VERY_STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_PairedDryBoard_BestTwoPair() {
		Pocket pocket = new Pocket(Card.build("Kh"), Card.build("3s"));
		Flop flop = new Flop(Card.build("Ks"), Card.build("5c"), Card.build("5h"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.DRY, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.TWO_PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.MEDIUM, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_NonPairedSemiBoard_TwoPair() {
		Pocket pocket = new Pocket(Card.build("Kh"), Card.build("3s"));
		Flop flop = new Flop(Card.build("Ks"), Card.build("Jc"), Card.build("3h"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.SEMICOORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.TWO_PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_PairedSemiBoard_TwoPair() {
		Pocket pocket = new Pocket(Card.build("Kh"), Card.build("Qs"));
		Flop flop = new Flop(Card.build("Kc"), Card.build("Jc"), Card.build("Jh"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.SEMICOORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.TWO_PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.MEDIUM, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_NonPairedSemiBoard_BestTwoPair() {
		Pocket pocket = new Pocket(Card.build("Kh"), Card.build("Js"));
		Flop flop = new Flop(Card.build("Kc"), Card.build("Jc"), Card.build("3h"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.SEMICOORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.TWO_PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.VERY_STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_PairedSemiBoard_BestTwoPair() {
		Pocket pocket = new Pocket(Card.build("Kh"), Card.build("3s"));
		Flop flop = new Flop(Card.build("Kc"), Card.build("Jc"), Card.build("Jh"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.SEMICOORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.TWO_PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.MEDIUM, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_PairedCoordBoard_TwoPair() {
		Pocket pocket = new Pocket(Card.build("Kh"), Card.build("Qs"));
		Flop flop = new Flop(Card.build("Kc"), Card.build("Jc"), Card.build("Jh"));
		Board board = new Board(flop, Card.build("2c"));
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		assertTrue(board.isPaired());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.TWO_PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.MEDIUM, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_DoublePairedCoordBoard_TwoPair() {
		Pocket pocket = new Pocket(Card.build("Kh"), Card.build("Qs"));
		Flop flop = new Flop(Card.build("Kc"), Card.build("Jc"), Card.build("Jh"));
		Board board = new Board(flop, Card.build("2c"), Card.build("2d"));
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		assertTrue(board.isDoublePaired());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.TWO_PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.MEDIUM, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_NonPairedCoordBoard_TwoPair() {
		Pocket pocket = new Pocket(Card.build("Kh"), Card.build("Js"));
		Flop flop = new Flop(Card.build("Kc"), Card.build("Jc"), Card.build("3c"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		assertFalse(board.isPaired());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.TWO_PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_NonPairedExtreBoard_TwoPair() {
		Pocket pocket = new Pocket(Card.build("Kh"), Card.build("Js"));
		Flop flop = new Flop(Card.build("Kc"), Card.build("Jc"), Card.build("3c"));
		Board board = new Board(flop, Card.build("2c"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		assertFalse(board.isPaired());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.TWO_PAIR, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.NOTHING, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	
	
	
	
	
	
	// THREE OF A KIND
	
	@Test
	public void testGetHandStrength_NonPairedDryBoard_TOAK() {
		Pocket pocket = new Pocket(Card.build("Kh"), Card.build("Ks"));
		Flop flop = new Flop(Card.build("Kc"), Card.build("3s"), Card.build("8d"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.DRY, board.getTexture());
		assertFalse(board.isPaired());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.THREE_OF_A_KIND, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.VERY_STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_PairedDryBoard_TOAK() {
		Pocket pocket = new Pocket(Card.build("Kh"), Card.build("3s"));
		Flop flop = new Flop(Card.build("Kc"), Card.build("Ks"), Card.build("8d"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.DRY, board.getTexture());
		assertTrue(board.isPaired());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.THREE_OF_A_KIND, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_NonPairedSemiBoard_TOAK() {
		Pocket pocket = new Pocket(Card.build("Kh"), Card.build("Ks"));
		Flop flop = new Flop(Card.build("Kc"), Card.build("3s"), Card.build("Qd"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.SEMICOORDINATED, board.getTexture());
		assertFalse(board.isPaired());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.THREE_OF_A_KIND, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.VERY_STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_PairedSemiBoard_TOAK() {
		Pocket pocket = new Pocket(Card.build("Kh"), Card.build("3s"));
		Flop flop = new Flop(Card.build("Kc"), Card.build("Ks"), Card.build("Qd"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.SEMICOORDINATED, board.getTexture());
		assertTrue(board.isPaired());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.THREE_OF_A_KIND, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_CoordBoard_TOAK() {
		Pocket pocket = new Pocket(Card.build("Kh"), Card.build("Ks"));
		Flop flop = new Flop(Card.build("Kc"), Card.build("Js"), Card.build("Qd"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		assertFalse(board.isPaired());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.THREE_OF_A_KIND, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_ExtreBoard_TOAK() {
		Pocket pocket = new Pocket(Card.build("Kh"), Card.build("Ks"));
		Flop flop = new Flop(Card.build("Kc"), Card.build("Js"), Card.build("Qd"));
		Board board = new Board(flop, Card.build("Ad"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		assertFalse(board.isPaired());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.THREE_OF_A_KIND, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.WEAK, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	
	
	
	
	
	
	// STRAIGHT
	
	@Test
	public void testGetHandStrength_NonPairedDryBoard_Straight() {
		Pocket pocket = new Pocket(Card.build("3h"), Card.build("2s"));
		Flop flop = new Flop(Card.build("Ac"), Card.build("4s"), Card.build("5d"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.DRY, board.getTexture());
		assertFalse(board.isPaired());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.STRAIGHT, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.VERY_STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_PairedDryBoard_Straight() {
		Pocket pocket = new Pocket(Card.build("3h"), Card.build("2s"));
		Flop flop = new Flop(Card.build("Ac"), Card.build("4s"), Card.build("5d"));
		Board board = new Board(flop, Card.build("As"));
		assertEquals(BoardTexture.DRY, board.getTexture());
		assertTrue(board.isPaired());
		assertFalse(board.isDoublePaired());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.STRAIGHT, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_DoublePairedDryBoard_Straight() {
		Pocket pocket = new Pocket(Card.build("3h"), Card.build("2s"));
		Flop flop = new Flop(Card.build("Ac"), Card.build("4s"), Card.build("5d"));
		Board board = new Board(flop, Card.build("As"), Card.build("4h"));
		assertEquals(BoardTexture.DRY, board.getTexture());
		assertTrue(board.isDoublePaired());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.STRAIGHT, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.MEDIUM, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_DoublePairedCoorBoard_Straight() {
		Pocket pocket = new Pocket(Card.build("Qh"), Card.build("Js"));
		Flop flop = new Flop(Card.build("Ac"), Card.build("Ks"), Card.build("Td"));
		Board board = new Board(flop, Card.build("As"), Card.build("Th"));
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		assertTrue(board.isDoublePaired());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.STRAIGHT, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.MEDIUM, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_PairedCoorBoard_Straight() {
		Pocket pocket = new Pocket(Card.build("Qh"), Card.build("Js"));
		Flop flop = new Flop(Card.build("Ac"), Card.build("Ks"), Card.build("Td"));
		Board board = new Board(flop, Card.build("As"), Card.build("4h"));
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		assertTrue(board.isPaired());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.STRAIGHT, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_3SuitedCoorBoard_Straight() {
		Pocket pocket = new Pocket(Card.build("Qh"), Card.build("Js"));
		Flop flop = new Flop(Card.build("Ac"), Card.build("Kc"), Card.build("Tc"));
		Board board = new Board(flop, Card.build("As"), Card.build("4h"));
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		assertTrue(board.isPaired());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.STRAIGHT, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_3ConnectedCoorBoard_Straight() {
		Pocket pocket = new Pocket(Card.build("Th"), Card.build("Js"));
		Flop flop = new Flop(Card.build("Ac"), Card.build("Kc"), Card.build("Qc"));
		Board board = new Board(flop, Card.build("5s"), Card.build("4h"));
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		assertFalse(board.isPaired());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.STRAIGHT, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_NonPairedCoorBoard_Straight() {
		Pocket pocket = new Pocket(Card.build("Qh"), Card.build("Js"));
		Flop flop = new Flop(Card.build("Ac"), Card.build("Kc"), Card.build("Tc"));
		Board board = new Board(flop, Card.build("5s"), Card.build("4h"));
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		assertFalse(board.isPaired());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.STRAIGHT, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.VERY_STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_4Suited_NonPaired_ExtreBoard_Straight() {
		Pocket pocket = new Pocket(Card.build("Qh"), Card.build("Js"));
		Flop flop = new Flop(Card.build("Ac"), Card.build("Kc"), Card.build("Tc"));
		Board board = new Board(flop, Card.build("5c"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		assertFalse(board.isPaired());
		assertEquals(4, board.getSuitedCards());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.STRAIGHT, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.WEAK, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_4Suited_Paired_ExtreBoard_Straight() {
		Pocket pocket = new Pocket(Card.build("Qh"), Card.build("Js"));
		Flop flop = new Flop(Card.build("Ac"), Card.build("Kc"), Card.build("Tc"));
		Board board = new Board(flop, Card.build("5c"), Card.build("5h"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		assertTrue(board.isPaired());
		assertEquals(4, board.getSuitedCards());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.STRAIGHT, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.WEAK, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_4Connected_NonPaired_ExtreBoard_Straight() {
		Pocket pocket = new Pocket(Card.build("Qh"), Card.build("Ts"));
		Flop flop = new Flop(Card.build("Ac"), Card.build("Kd"), Card.build("Jh"));
		Board board = new Board(flop, Card.build("Qc"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		assertFalse(board.isPaired());
		assertEquals(4, board.getMaxConnectedCards());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.STRAIGHT, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.MEDIUM, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_4Connected_Paired_ExtreBoard_NonSuperior_Straight() {
		Pocket pocket = new Pocket(Card.build("4h"), Card.build("Ts"));
		Flop flop = new Flop(Card.build("Ac"), Card.build("Kd"), Card.build("Jh"));
		Board board = new Board(flop, Card.build("Qc"), Card.build("Qh"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		assertTrue(board.isPaired());
		assertEquals(4, board.getMaxConnectedCards());
		
		HandCards handCards = new HandCards(pocket, board);
		CompletedHand straight = handCards.getCompletedHand();
		assertEquals(GenericHandRank.STRAIGHT, straight.getGenericHandRank());
		assertEquals(0, ((CompletedStraight) straight).getSuperiors());
		assertEquals(HandStrength.MEDIUM, new StrengthCalculator().calculate(board, straight));
	}
	
	@Test
	public void testGetHandStrength_4Connected_NonPaired_ExtreBoard_NonSuperior_Straight() {
		Pocket pocket = new Pocket(Card.build("4h"), Card.build("Ts"));
		Flop flop = new Flop(Card.build("Ac"), Card.build("Kd"), Card.build("Jh"));
		Board board = new Board(flop, Card.build("Qc"), Card.build("7h"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		assertFalse(board.isPaired());
		assertEquals(4, board.getMaxConnectedCards());
		
		HandCards handCards = new HandCards(pocket, board);
		CompletedHand straight = handCards.getCompletedHand();
		assertEquals(GenericHandRank.STRAIGHT, straight.getGenericHandRank());
		assertEquals(0, ((CompletedStraight) straight).getSuperiors());
		assertEquals(HandStrength.MEDIUM, new StrengthCalculator().calculate(board, straight));
	}
	
	@Test
	public void testGetHandStrength_4Connected_Paired_ExtreBoard_SuperiorBy1_Straight() {
		Pocket pocket = new Pocket(Card.build("4h"), Card.build("As"));
		Flop flop = new Flop(Card.build("Tc"), Card.build("Kd"), Card.build("Jh"));
		Board board = new Board(flop, Card.build("Qc"), Card.build("Qh"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		assertTrue(board.isPaired());
		assertEquals(4, board.getMaxConnectedCards());
		
		HandCards handCards = new HandCards(pocket, board);
		CompletedHand straight = handCards.getCompletedHand();
		assertEquals(GenericHandRank.STRAIGHT, straight.getGenericHandRank());
		assertEquals(HandStrength.MEDIUM, new StrengthCalculator().calculate(board, straight));
	}
	
	@Test
	public void testGetHandStrength_4Connected_Paired_ExtreBoard_SuperiorBy2_Straight() {
		Pocket pocket = new Pocket(Card.build("Kh"), Card.build("As"));
		Flop flop = new Flop(Card.build("Tc"), Card.build("9d"), Card.build("Jh"));
		Board board = new Board(flop, Card.build("Qc"), Card.build("Qh"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		assertTrue(board.isPaired());
		assertEquals(4, board.getMaxConnectedCards());
		
		HandCards handCards = new HandCards(pocket, board);
		CompletedHand straight = handCards.getCompletedHand();
		assertEquals(GenericHandRank.STRAIGHT, straight.getGenericHandRank());
		assertEquals(HandStrength.STRONG, new StrengthCalculator().calculate(board, straight));
	}
	
	@Test
	public void testGetHandStrength_4Connected_NonPaired_ExtreBoard_SuperiorBy1_Straight() {
		Pocket pocket = new Pocket(Card.build("4h"), Card.build("As"));
		Flop flop = new Flop(Card.build("Tc"), Card.build("Kd"), Card.build("Jh"));
		Board board = new Board(flop, Card.build("Qc"), Card.build("2h"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		assertFalse(board.isPaired());
		assertEquals(4, board.getMaxConnectedCards());
		
		HandCards handCards = new HandCards(pocket, board);
		CompletedHand straight = handCards.getCompletedHand();
		assertEquals(GenericHandRank.STRAIGHT, straight.getGenericHandRank());
		assertEquals(HandStrength.STRONG, new StrengthCalculator().calculate(board, straight));
	}
	
	@Test
	public void testGetHandStrength_4Connected_NonPaired_ExtreBoard_SuperiorBy2_Straight() {
		Pocket pocket = new Pocket(Card.build("Kh"), Card.build("As"));
		Flop flop = new Flop(Card.build("Tc"), Card.build("9d"), Card.build("Jh"));
		Board board = new Board(flop, Card.build("Qc"), Card.build("2h"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		assertFalse(board.isPaired());
		assertEquals(4, board.getMaxConnectedCards());
		
		HandCards handCards = new HandCards(pocket, board);
		CompletedHand straight = handCards.getCompletedHand();
		assertEquals(GenericHandRank.STRAIGHT, straight.getGenericHandRank());
		assertEquals(HandStrength.VERY_STRONG, new StrengthCalculator().calculate(board, straight));
	}
	
	
	
	
	
	
	
	// FLUSH
	
	@Test
	public void testGetHandStrength_NonPairedCoorBoard_Flush() {
		Pocket pocket = new Pocket(Card.build("3h"), Card.build("4h"));
		Flop flop = new Flop(Card.build("Ah"), Card.build("2h"), Card.build("7h"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		assertFalse(board.isPaired());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.FLUSH, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.VERY_STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_PairedCoorBoard_Flush() {
		Pocket pocket = new Pocket(Card.build("3h"), Card.build("4h"));
		Flop flop = new Flop(Card.build("Ah"), Card.build("2h"), Card.build("7h"));
		Board board = new Board(flop, Card.build("7s"));
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		assertTrue(board.isPaired());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.FLUSH, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_DoublePairedCoorBoard_Flush() {
		Pocket pocket = new Pocket(Card.build("3h"), Card.build("4h"));
		Flop flop = new Flop(Card.build("Ah"), Card.build("2h"), Card.build("7h"));
		Board board = new Board(flop, Card.build("7s"), Card.build("2s"));
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		assertTrue(board.isPaired());
		assertTrue(board.isDoublePaired());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.FLUSH, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.MEDIUM, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_4SameSuit_PairedExtreBoard_WorseThan5thBest_Flush() {
		Pocket pocket = new Pocket(Card.build("6h"), Card.build("4c"));
		Flop flop = new Flop(Card.build("Ah"), Card.build("2h"), Card.build("7h"));
		Board board = new Board(flop, Card.build("Kh"), Card.build("2s"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		assertTrue(board.isPaired());
		assertEquals(4, board.getSuitedCards());
		
		
		HandCards handCards = new HandCards(pocket, board);
		CompletedFlush flush = (CompletedFlush) handCards.getCompletedHand();
		assertEquals(GenericHandRank.FLUSH, flush.getGenericHandRank());
		assertEquals(5, flush.getCardsThatMakeABetterFlush());
		assertEquals(HandStrength.WEAK, new StrengthCalculator().calculate(board, flush));
	}
	
	@Test
	public void testGetHandStrength_4SameSuit_PairedExtreBoard_5thBest_Flush() {
		Pocket pocket = new Pocket(Card.build("8h"), Card.build("4c"));
		Flop flop = new Flop(Card.build("Ah"), Card.build("2h"), Card.build("7h"));
		Board board = new Board(flop, Card.build("Kh"), Card.build("2s"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		assertTrue(board.isPaired());
		assertEquals(4, board.getSuitedCards());
		
		
		HandCards handCards = new HandCards(pocket, board);
		CompletedFlush flush = (CompletedFlush) handCards.getCompletedHand();
		assertEquals(GenericHandRank.FLUSH, flush.getGenericHandRank());
		assertEquals(4, flush.getCardsThatMakeABetterFlush());
		assertEquals(HandStrength.MEDIUM, new StrengthCalculator().calculate(board, flush));
	}
	
	@Test
	public void testGetHandStrength_4SameSuit_PairedExtreBoard_3rdBest_Flush() {
		Pocket pocket = new Pocket(Card.build("Th"), Card.build("4c"));
		Flop flop = new Flop(Card.build("Ah"), Card.build("2h"), Card.build("7h"));
		Board board = new Board(flop, Card.build("Kh"), Card.build("2s"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		assertTrue(board.isPaired());
		assertEquals(4, board.getSuitedCards());
		
		
		HandCards handCards = new HandCards(pocket, board);
		CompletedFlush flush = (CompletedFlush) handCards.getCompletedHand();
		assertEquals(GenericHandRank.FLUSH, flush.getGenericHandRank());
		assertEquals(2, flush.getCardsThatMakeABetterFlush());
		assertEquals(HandStrength.MEDIUM, new StrengthCalculator().calculate(board, flush));
	}
	
	@Test
	public void testGetHandStrength_4SameSuit_PairedExtreBoard_2ndBest_Flush() {
		Pocket pocket = new Pocket(Card.build("Jh"), Card.build("4c"));
		Flop flop = new Flop(Card.build("Ah"), Card.build("2h"), Card.build("7h"));
		Board board = new Board(flop, Card.build("Kh"), Card.build("2s"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		assertTrue(board.isPaired());
		assertEquals(4, board.getSuitedCards());
		
		
		HandCards handCards = new HandCards(pocket, board);
		CompletedFlush flush = (CompletedFlush) handCards.getCompletedHand();
		assertEquals(GenericHandRank.FLUSH, flush.getGenericHandRank());
		assertEquals(1, flush.getCardsThatMakeABetterFlush());
		assertEquals(HandStrength.MEDIUM, new StrengthCalculator().calculate(board, flush));
	}
	
	@Test
	public void testGetHandStrength_4SameSuit_PairedExtreBoard_Nuts_Flush() {
		Pocket pocket = new Pocket(Card.build("Qh"), Card.build("4c"));
		Flop flop = new Flop(Card.build("Ah"), Card.build("2h"), Card.build("7h"));
		Board board = new Board(flop, Card.build("Kh"), Card.build("2s"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		assertTrue(board.isPaired());
		assertEquals(4, board.getSuitedCards());
		
		
		HandCards handCards = new HandCards(pocket, board);
		CompletedFlush flush = (CompletedFlush) handCards.getCompletedHand();
		assertEquals(GenericHandRank.FLUSH, flush.getGenericHandRank());
		assertEquals(0, flush.getCardsThatMakeABetterFlush());
		assertEquals(HandStrength.STRONG, new StrengthCalculator().calculate(board, flush));
	}
	
	@Test
	public void testGetHandStrength_4SameSuit_NonPairedExtreBoard_WorseThan5thBest_Flush() {
		Pocket pocket = new Pocket(Card.build("6h"), Card.build("4c"));
		Flop flop = new Flop(Card.build("Ah"), Card.build("2h"), Card.build("7h"));
		Board board = new Board(flop, Card.build("Kh"), Card.build("8s"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		assertFalse(board.isPaired());
		assertEquals(4, board.getSuitedCards());
		
		
		HandCards handCards = new HandCards(pocket, board);
		CompletedFlush flush = (CompletedFlush) handCards.getCompletedHand();
		assertEquals(GenericHandRank.FLUSH, flush.getGenericHandRank());
		assertEquals(5, flush.getCardsThatMakeABetterFlush());
		assertEquals(HandStrength.WEAK, new StrengthCalculator().calculate(board, flush));
	}
	
	@Test
	public void testGetHandStrength_4SameSuit_NonPairedExtreBoard_5thBest_Flush() {
		Pocket pocket = new Pocket(Card.build("8h"), Card.build("4c"));
		Flop flop = new Flop(Card.build("Ah"), Card.build("2h"), Card.build("7h"));
		Board board = new Board(flop, Card.build("Kh"), Card.build("8s"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		assertFalse(board.isPaired());
		assertEquals(4, board.getSuitedCards());
		
		
		HandCards handCards = new HandCards(pocket, board);
		CompletedFlush flush = (CompletedFlush) handCards.getCompletedHand();
		assertEquals(GenericHandRank.FLUSH, flush.getGenericHandRank());
		assertEquals(4, flush.getCardsThatMakeABetterFlush());
		assertEquals(HandStrength.MEDIUM, new StrengthCalculator().calculate(board, flush));
	}
	
	@Test
	public void testGetHandStrength_4SameSuit_NonPairedExtreBoard_3rdBest_Flush() {
		Pocket pocket = new Pocket(Card.build("Th"), Card.build("4c"));
		Flop flop = new Flop(Card.build("Ah"), Card.build("2h"), Card.build("7h"));
		Board board = new Board(flop, Card.build("Kh"), Card.build("8s"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		assertFalse(board.isPaired());
		assertEquals(4, board.getSuitedCards());
		
		
		HandCards handCards = new HandCards(pocket, board);
		CompletedFlush flush = (CompletedFlush) handCards.getCompletedHand();
		assertEquals(GenericHandRank.FLUSH, flush.getGenericHandRank());
		assertEquals(2, flush.getCardsThatMakeABetterFlush());
		assertEquals(HandStrength.MEDIUM, new StrengthCalculator().calculate(board, flush));
	}
	
	@Test
	public void testGetHandStrength_4SameSuit_NonPairedExtreBoard_2ndBest_Flush() {
		Pocket pocket = new Pocket(Card.build("Jh"), Card.build("4c"));
		Flop flop = new Flop(Card.build("Ah"), Card.build("2h"), Card.build("7h"));
		Board board = new Board(flop, Card.build("Kh"), Card.build("8s"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		assertFalse(board.isPaired());
		assertEquals(4, board.getSuitedCards());
		
		
		HandCards handCards = new HandCards(pocket, board);
		CompletedFlush flush = (CompletedFlush) handCards.getCompletedHand();
		assertEquals(GenericHandRank.FLUSH, flush.getGenericHandRank());
		assertEquals(1, flush.getCardsThatMakeABetterFlush());
		assertEquals(HandStrength.STRONG, new StrengthCalculator().calculate(board, flush));
	}
	
	@Test
	public void testGetHandStrength_4SameSuit_NonPairedExtreBoard_Nuts_Flush() {
		Pocket pocket = new Pocket(Card.build("Qh"), Card.build("4c"));
		Flop flop = new Flop(Card.build("Ah"), Card.build("2h"), Card.build("7h"));
		Board board = new Board(flop, Card.build("Kh"), Card.build("8s"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		assertFalse(board.isPaired());
		assertEquals(4, board.getSuitedCards());
		
		
		HandCards handCards = new HandCards(pocket, board);
		CompletedFlush flush = (CompletedFlush) handCards.getCompletedHand();
		assertEquals(GenericHandRank.FLUSH, flush.getGenericHandRank());
		assertEquals(0, flush.getCardsThatMakeABetterFlush());
		assertEquals(HandStrength.VERY_STRONG, new StrengthCalculator().calculate(board, flush));
	}
	
	@Test
	public void testGetHandStrength_4Connected_PairedExtreBoard_Flush() {
		Pocket pocket = new Pocket(Card.build("2h"), Card.build("4h"));
		Flop flop = new Flop(Card.build("6h"), Card.build("6c"), Card.build("7h"));
		Board board = new Board(flop, Card.build("9h"), Card.build("8s"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		assertTrue(board.isPaired());
		assertTrue(board.getSuitedCards() < 4);
		assertEquals(4, board.getMaxConnectedCards());
		
		
		HandCards handCards = new HandCards(pocket, board);
		CompletedFlush flush = (CompletedFlush) handCards.getCompletedHand();
		assertEquals(GenericHandRank.FLUSH, flush.getGenericHandRank());
		assertEquals(HandStrength.STRONG, new StrengthCalculator().calculate(board, flush));
	}
	
	@Test
	public void testGetHandStrength_4Connected_NonPairedExtreBoard_Flush() {
		Pocket pocket = new Pocket(Card.build("2h"), Card.build("4h"));
		Flop flop = new Flop(Card.build("6h"), Card.build("Kc"), Card.build("7h"));
		Board board = new Board(flop, Card.build("9h"), Card.build("8s"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		assertFalse(board.isPaired());
		assertTrue(board.getSuitedCards() < 4);
		assertEquals(4, board.getMaxConnectedCards());
		
		
		HandCards handCards = new HandCards(pocket, board);
		CompletedFlush flush = (CompletedFlush) handCards.getCompletedHand();
		assertEquals(GenericHandRank.FLUSH, flush.getGenericHandRank());
		assertEquals(HandStrength.VERY_STRONG, new StrengthCalculator().calculate(board, flush));
	}
	
	@Test
	public void testGetHandStrength_4Connected_4SameSuit_Flush() {
		Pocket pocket = new Pocket(Card.build("2h"), Card.build("4h"));
		Flop flop = new Flop(Card.build("6h"), Card.build("Kh"), Card.build("7h"));
		Board board = new Board(flop, Card.build("9h"), Card.build("8s"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		assertFalse(board.isPaired());
		assertEquals(4, board.getSuitedCards());
		assertEquals(4, board.getMaxConnectedCards());
		
		
		HandCards handCards = new HandCards(pocket, board);
		CompletedFlush flush = (CompletedFlush) handCards.getCompletedHand();
		assertEquals(GenericHandRank.FLUSH, flush.getGenericHandRank());
		assertEquals(HandStrength.WEAK, new StrengthCalculator().calculate(board, flush));
	}
	
	
	
	
	
	
	
	// FULL_HOUSE
	
	@Test
	public void testGetHandStrength_PairedDryBoard_FullHouse() {
		Pocket pocket = new Pocket(Card.build("3h"), Card.build("7h"));
		Flop flop = new Flop(Card.build("3d"), Card.build("3s"), Card.build("7s"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.DRY, board.getTexture());
		assertTrue(board.isPaired());
		assertFalse(board.isDoublePaired());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.FULL_HOUSE, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.VERY_STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_DoblePairedDryBoard_FullHouse() {
		Pocket pocket = new Pocket(Card.build("3h"), Card.build("7h"));
		Flop flop = new Flop(Card.build("3d"), Card.build("3s"), Card.build("7s"));
		Board board = new Board(flop, Card.build("7d"));
		assertEquals(BoardTexture.DRY, board.getTexture());
		assertTrue(board.isDoublePaired());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.FULL_HOUSE, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_PairedSemiBoard_FullHouse() {
		Pocket pocket = new Pocket(Card.build("6h"), Card.build("7h"));
		Flop flop = new Flop(Card.build("6d"), Card.build("6s"), Card.build("7s"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.SEMICOORDINATED, board.getTexture());
		assertTrue(board.isPaired());
		assertFalse(board.isDoublePaired());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.FULL_HOUSE, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.VERY_STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_PairedCoorBoard_NonSuperior_FullHouse() {
		Pocket pocket = new Pocket(Card.build("6s"), Card.build("6h"));
		Flop flop = new Flop(Card.build("6d"), Card.build("3s"), Card.build("7s"));
		Board board = new Board(flop, Card.build("7d"), Card.build("8d"));
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		assertTrue(board.isPaired());
		assertFalse(board.isDoublePaired());
		
		HandCards handCards = new HandCards(pocket, board);
		CompletedFullHouse fullHouse = (CompletedFullHouse) handCards.getCompletedHand();
		assertEquals(GenericHandRank.FULL_HOUSE, fullHouse.getGenericHandRank());
		assertFalse(fullHouse.isAtLeastBestFullHouse());
		assertEquals(HandStrength.VERY_STRONG, new StrengthCalculator().calculate(board, fullHouse));
	}
	
	@Test
	public void testGetHandStrength_PairedCoorBoard_Superior_FullHouse() {
		Pocket pocket = new Pocket(Card.build("8h"), Card.build("8d"));
		Flop flop = new Flop(Card.build("4d"), Card.build("6s"), Card.build("7s"));
		Board board = new Board(flop, Card.build("7h"), Card.build("8s"));
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		assertTrue(board.isPaired());
		assertFalse(board.isDoublePaired());
		
		HandCards handCards = new HandCards(pocket, board);
		CompletedFullHouse fullHouse = (CompletedFullHouse) handCards.getCompletedHand();
		assertEquals(GenericHandRank.FULL_HOUSE, fullHouse.getGenericHandRank());
		assertTrue(fullHouse.isAtLeastBestFullHouse());
		assertEquals(HandStrength.VERY_STRONG, new StrengthCalculator().calculate(board, fullHouse));
	}
	
	@Test
	public void testGetHandStrength_DoublePairedCoorBoard_NonSuperior_FullHouse() {
		Pocket pocket = new Pocket(Card.build("3h"), Card.build("6h"));
		Flop flop = new Flop(Card.build("6d"), Card.build("6s"), Card.build("7s"));
		Board board = new Board(flop, Card.build("7d"), Card.build("8d"));
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		assertTrue(board.isDoublePaired());
		
		HandCards handCards = new HandCards(pocket, board);
		CompletedFullHouse fullHouse = (CompletedFullHouse) handCards.getCompletedHand();
		assertEquals(GenericHandRank.FULL_HOUSE, fullHouse.getGenericHandRank());
		assertFalse(fullHouse.isAtLeastBestFullHouse());
		assertEquals(HandStrength.STRONG, new StrengthCalculator().calculate(board, fullHouse));
	}
	
	@Test
	public void testGetHandStrength_DoublePairedCoorBoard_Superior_FullHouse() {
		Pocket pocket = new Pocket(Card.build("8h"), Card.build("8s"));
		Flop flop = new Flop(Card.build("6d"), Card.build("6s"), Card.build("7s"));
		Board board = new Board(flop, Card.build("7d"), Card.build("8d"));
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		assertTrue(board.isDoublePaired());
		
		HandCards handCards = new HandCards(pocket, board);
		CompletedFullHouse fullHouse = (CompletedFullHouse) handCards.getCompletedHand();
		assertEquals(GenericHandRank.FULL_HOUSE, fullHouse.getGenericHandRank());
		assertTrue(fullHouse.isAtLeastBestFullHouse());
		assertEquals(HandStrength.VERY_STRONG, new StrengthCalculator().calculate(board, fullHouse));
	}
	
	
	
	
	
	
	
	// POKER
	
	@Test
	public void testGetHandStrength_dryBoard_FOAK() {
		Pocket pocket = new Pocket(Card.build("3h"), Card.build("3c"));
		Flop flop = new Flop(Card.build("3d"), Card.build("3s"), Card.build("7s"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.DRY, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.FOUR_OF_A_KIND, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.VERY_STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_semiBoard_FOAK() {
		Pocket pocket = new Pocket(Card.build("6h"), Card.build("6c"));
		Flop flop = new Flop(Card.build("6d"), Card.build("6s"), Card.build("7s"));
		Board board = new Board(flop);
		assertEquals(BoardTexture.SEMICOORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.FOUR_OF_A_KIND, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.VERY_STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_coorBoard_FOAK() {
		Pocket pocket = new Pocket(Card.build("6h"), Card.build("6c"));
		Flop flop = new Flop(Card.build("6d"), Card.build("6s"), Card.build("7s"));
		Board board = new Board(flop, Card.build("8h"));
		assertEquals(BoardTexture.COORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.FOUR_OF_A_KIND, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.VERY_STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_extreBoard_FOAK() {
		Pocket pocket = new Pocket(Card.build("6h"), Card.build("6c"));
		Flop flop = new Flop(Card.build("6d"), Card.build("6s"), Card.build("7s"));
		Board board = new Board(flop, Card.build("8h"), Card.build("9c"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.FOUR_OF_A_KIND, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.VERY_STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	
	
	
	
	
	
	// STRAIGHT FLUSH
	
	@Test
	public void testGetHandStrength_coorBoard_straightFlush() {
		Pocket pocket = new Pocket(Card.build("6c"), Card.build("7c"));
		Flop flop = new Flop(Card.build("6d"), Card.build("8c"), Card.build("Tc"));
		Board board = new Board(flop, Card.build("9c"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.STRAIGHT_FLUSH, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.VERY_STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
	
	@Test
	public void testGetHandStrength_extreBoard_straightFlush() {
		Pocket pocket = new Pocket(Card.build("6h"), Card.build("7c"));
		Flop flop = new Flop(Card.build("6c"), Card.build("8c"), Card.build("Tc"));
		Board board = new Board(flop, Card.build("9c"));
		assertEquals(BoardTexture.EXTREMELY_COORDINATED, board.getTexture());
		
		HandCards handCards = new HandCards(pocket, board);
		assertEquals(GenericHandRank.STRAIGHT_FLUSH, handCards.getCompletedHand().getGenericHandRank());
		assertEquals(HandStrength.VERY_STRONG, new StrengthCalculator().calculate(board, handCards.getCompletedHand()));
	}
}
