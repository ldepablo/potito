package com.ldepablo.poker.educa.model.completedhands;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.ldepablo.poker.educa.model.Board;
import com.ldepablo.poker.educa.model.Card;
import com.ldepablo.poker.educa.model.CardRank;
import com.ldepablo.poker.educa.model.Flop;
import com.ldepablo.poker.educa.model.GenericHandRank;
import com.ldepablo.poker.educa.model.HandCards;
import com.ldepablo.poker.educa.model.Pocket;

public class CompletedPairTest {

	@Test
	public void testNoPossiblePairReturnsNull() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Qd"));
		Flop flop = new Flop(Card.build("Tc"), Card.build("Jh"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		assertNull(CompletedPair.build(pocket, handCards.getCards()));
	}
	
	@Test
	public void testPair_PocketAces() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Ad"));
		Flop flop = new Flop(Card.build("Kc"), Card.build("Qh"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		CompletedPair result = CompletedPair.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.PAIR, result.getGenericHandRank());
		assertEquals(CardRank.ACE, result.getRank());
		assertEquals(CardRank.KING, result.getKicker1());
		assertEquals(CardRank.QUEEN, result.getKicker2());
		assertEquals(CardRank.SEVEN, result.getKicker3());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertEquals(0, result.getSecondBestPocketPosition().byteValue());
		assertTrue(result.isAtLeastMidPair());
		assertTrue(result.isAtLeastTopPair());
		assertFalse(result.isSecondKickerOrBetter());
		assertTrue(result.isOverpair());
	}
	
	@Test
	public void testPair_PocketKings_AceOnFlop() {
		Pocket pocket = new Pocket(Card.build("Ks"), Card.build("Kd"));
		Flop flop = new Flop(Card.build("Ac"), Card.build("Qh"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		CompletedPair result = CompletedPair.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.PAIR, result.getGenericHandRank());
		assertEquals(CardRank.KING, result.getRank());
		assertEquals(CardRank.ACE, result.getKicker1());
		assertEquals(CardRank.QUEEN, result.getKicker2());
		assertEquals(CardRank.SEVEN, result.getKicker3());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertEquals(0, result.getSecondBestPocketPosition().byteValue());
		assertTrue(result.isAtLeastMidPair());
		assertFalse(result.isAtLeastTopPair());
		assertFalse(result.isSecondKickerOrBetter());
		assertFalse(result.isOverpair());
	}
	
	@Test
	public void testPair_PocketAceKing_KingOnFlop() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Kd"));
		Flop flop = new Flop(Card.build("Kc"), Card.build("Qh"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		CompletedPair result = CompletedPair.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.PAIR, result.getGenericHandRank());
		assertEquals(CardRank.KING, result.getRank());
		assertEquals(CardRank.ACE, result.getKicker1());
		assertEquals(CardRank.QUEEN, result.getKicker2());
		assertEquals(CardRank.SEVEN, result.getKicker3());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertEquals(2, result.getSecondBestPocketPosition().byteValue());
		assertTrue(result.isAtLeastMidPair());
		assertTrue(result.isAtLeastTopPair());
		assertTrue(result.isSecondKickerOrBetter());
		assertFalse(result.isOverpair());
	}
	
	@Test
	public void testPair_PocketAceQueen_AceOnFlop() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Qd"));
		Flop flop = new Flop(Card.build("Ac"), Card.build("Kh"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		CompletedPair result = CompletedPair.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.PAIR, result.getGenericHandRank());
		assertEquals(CardRank.ACE, result.getRank());
		assertEquals(CardRank.KING, result.getKicker1());
		assertEquals(CardRank.QUEEN, result.getKicker2());
		assertEquals(CardRank.SEVEN, result.getKicker3());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertEquals(3, result.getSecondBestPocketPosition().byteValue());
		assertTrue(result.isAtLeastMidPair());
		assertTrue(result.isAtLeastTopPair());
		assertTrue(result.isSecondKickerOrBetter());
		assertFalse(result.isOverpair());
	}
	
	@Test
	public void testPair_PocketAceQueen_KingQueenOnFlop() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("Qd"));
		Flop flop = new Flop(Card.build("Kc"), Card.build("Qh"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		CompletedPair result = CompletedPair.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.PAIR, result.getGenericHandRank());
		assertEquals(CardRank.QUEEN, result.getRank());
		assertEquals(CardRank.ACE, result.getKicker1());
		assertEquals(CardRank.KING, result.getKicker2());
		assertEquals(CardRank.SEVEN, result.getKicker3());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertEquals(2, result.getSecondBestPocketPosition().byteValue());
		assertTrue(result.isAtLeastMidPair());
		assertFalse(result.isAtLeastTopPair());
		assertTrue(result.isSecondKickerOrBetter());
		assertFalse(result.isOverpair());
	}
	
	@Test
	public void testPair_PocketKingQueen_AceQueenOnFlop() {
		Pocket pocket = new Pocket(Card.build("Qs"), Card.build("Kd"));
		Flop flop = new Flop(Card.build("Ac"), Card.build("Qh"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		CompletedPair result = CompletedPair.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.PAIR, result.getGenericHandRank());
		assertEquals(CardRank.QUEEN, result.getRank());
		assertEquals(CardRank.ACE, result.getKicker1());
		assertEquals(CardRank.KING, result.getKicker2());
		assertEquals(CardRank.SEVEN, result.getKicker3());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertEquals(3, result.getSecondBestPocketPosition().byteValue());
		assertTrue(result.isAtLeastMidPair());
		assertFalse(result.isAtLeastTopPair());
		assertTrue(result.isSecondKickerOrBetter());
		assertFalse(result.isOverpair());
	}
	
	@Test
	public void testPair_UselessPocket() {
		Pocket pocket = new Pocket(Card.build("3s"), Card.build("2d"));
		Flop flop = new Flop(Card.build("Ac"), Card.build("Qh"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("Th"), Card.build("7h")));
		CompletedPair result = CompletedPair.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.PAIR, result.getGenericHandRank());
		assertEquals(CardRank.SEVEN, result.getRank());
		assertEquals(CardRank.ACE, result.getKicker1());
		assertEquals(CardRank.QUEEN, result.getKicker2());
		assertEquals(CardRank.TEN, result.getKicker3());
		assertNull(result.getBestPocketPosition());
		assertNull(result.getSecondBestPocketPosition());
		assertFalse(result.isAtLeastMidPair());
		assertFalse(result.isAtLeastTopPair());
		assertFalse(result.isSecondKickerOrBetter());
		assertFalse(result.isOverpair());
	}
	
	@Test
	public void testPair_OnlyThirdKickerInPocket() {
		Pocket pocket = new Pocket(Card.build("3s"), Card.build("Td"));
		Flop flop = new Flop(Card.build("Ac"), Card.build("Qh"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("2h"), Card.build("7h")));
		CompletedPair result = CompletedPair.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.PAIR, result.getGenericHandRank());
		assertEquals(CardRank.SEVEN, result.getRank());
		assertEquals(CardRank.ACE, result.getKicker1());
		assertEquals(CardRank.QUEEN, result.getKicker2());
		assertEquals(CardRank.TEN, result.getKicker3());
		assertEquals(4,  result.getBestPocketPosition().byteValue());
		assertNull(result.getSecondBestPocketPosition());
		assertFalse(result.isAtLeastMidPair());
		assertFalse(result.isAtLeastTopPair());
		assertFalse(result.isSecondKickerOrBetter());
		assertFalse(result.isOverpair());
	}
	
	@Test
	public void testPair_OnlyThirdKickerInPocket2() {
		Pocket pocket = new Pocket(Card.build("3s"), Card.build("Td"));
		Flop flop = new Flop(Card.build("Kc"), Card.build("Qh"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("2h"), Card.build("7h")));
		CompletedPair result = CompletedPair.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.PAIR, result.getGenericHandRank());
		assertEquals(CardRank.SEVEN, result.getRank());
		assertEquals(CardRank.KING, result.getKicker1());
		assertEquals(CardRank.QUEEN, result.getKicker2());
		assertEquals(CardRank.TEN, result.getKicker3());
		assertEquals(4,  result.getBestPocketPosition().byteValue());
		assertNull(result.getSecondBestPocketPosition());
		assertFalse(result.isAtLeastMidPair());
		assertFalse(result.isAtLeastTopPair());
		assertFalse(result.isSecondKickerOrBetter());
		assertFalse(result.isOverpair());
	}
	
	@Test
	public void testPair_OnlySecondKickerInPocket() {
		Pocket pocket = new Pocket(Card.build("3s"), Card.build("Qd"));
		Flop flop = new Flop(Card.build("Kc"), Card.build("Th"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("2h"), Card.build("7h")));
		CompletedPair result = CompletedPair.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.PAIR, result.getGenericHandRank());
		assertEquals(CardRank.SEVEN, result.getRank());
		assertEquals(CardRank.KING, result.getKicker1());
		assertEquals(CardRank.QUEEN, result.getKicker2());
		assertEquals(CardRank.TEN, result.getKicker3());
		assertEquals(3,  result.getBestPocketPosition().byteValue());
		assertNull(result.getSecondBestPocketPosition());
		assertFalse(result.isAtLeastMidPair());
		assertFalse(result.isAtLeastTopPair());
		assertTrue(result.isSecondKickerOrBetter());
		assertFalse(result.isOverpair());
	}
	
	@Test
	public void testPair_OnlySecondKickerInPocket2() {
		Pocket pocket = new Pocket(Card.build("3s"), Card.build("Kd"));
		Flop flop = new Flop(Card.build("Qc"), Card.build("Th"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("2h"), Card.build("7h")));
		CompletedPair result = CompletedPair.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.PAIR, result.getGenericHandRank());
		assertEquals(CardRank.SEVEN, result.getRank());
		assertEquals(CardRank.KING, result.getKicker1());
		assertEquals(CardRank.QUEEN, result.getKicker2());
		assertEquals(CardRank.TEN, result.getKicker3());
		assertEquals(2,  result.getBestPocketPosition().byteValue());
		assertNull(result.getSecondBestPocketPosition());
		assertFalse(result.isAtLeastMidPair());
		assertFalse(result.isAtLeastTopPair());
		assertFalse(result.isOverpair());
	}
	
	@Test
	public void testPair_PocketPairLowerThanThirdKicker() {
		Pocket pocket = new Pocket(Card.build("5s"), Card.build("5d"));
		Flop flop = new Flop(Card.build("Ac"), Card.build("Qh"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("3s")));
		CompletedPair result = CompletedPair.build(pocket, handCards.getCards());
		assertNotNull(result);
		assertEquals(GenericHandRank.PAIR, result.getGenericHandRank());
		assertEquals(CardRank.FIVE, result.getRank());
		assertEquals(CardRank.ACE, result.getKicker1());
		assertEquals(CardRank.QUEEN, result.getKicker2());
		assertEquals(CardRank.SEVEN, result.getKicker3());
		assertEquals(0, result.getBestPocketPosition().byteValue());
		assertEquals(0, result.getSecondBestPocketPosition().byteValue());
		assertFalse(result.isAtLeastMidPair());
		assertFalse(result.isAtLeastTopPair());
		assertFalse(result.isSecondKickerOrBetter());
		assertFalse(result.isOverpair());
	}

	@Test
	public void testCompareWithSuperiorHand() {
		Pocket pocket1 = new Pocket(Card.build("As"), Card.build("Ad"));
		Flop flop1 = new Flop(Card.build("Kc"), Card.build("Qh"), Card.build("7c"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1));
		CompletedPair pair = CompletedPair.build(pocket1, handCards1.getCards());
		assertTrue(pair.isOverpair());
		
		Pocket pocket = new Pocket(Card.build("2s"), Card.build("3d"));
		Flop flop = new Flop(Card.build("8c"), Card.build("8h"), Card.build("7d"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("7s"), Card.build("5s")));
		CompletedTwoPair twoPair = CompletedTwoPair.build(pocket, handCards.getCards());

		assertTrue(pair.compareTo(twoPair) < 0);
	}

	@Test
	public void testCompareWithInferiorHand() {
		Pocket pocket1 = new Pocket(Card.build("As"), Card.build("Ad"));
		Flop flop1 = new Flop(Card.build("Kc"), Card.build("Qh"), Card.build("7c"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1));
		CompletedPair pair = CompletedPair.build(pocket1, handCards1.getCards());

		Pocket pocket = new Pocket(Card.build("As"), Card.build("Qd"));
		Flop flop = new Flop(Card.build("Tc"), Card.build("Jh"), Card.build("7c"));
		HandCards handCards = new HandCards(pocket, new Board(flop, Card.build("4h")));
		CompletedHighCard highCard = CompletedHighCard.build(pocket, handCards.getCards());

		assertTrue(pair.compareTo(highCard) > 0);
	}

	@Test
	public void testCompareByRank() {
		Pocket pocket1 = new Pocket(Card.build("As"), Card.build("Ad"));
		Flop flop1 = new Flop(Card.build("Kc"), Card.build("Qh"), Card.build("7c"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1));
		CompletedPair pair1 = CompletedPair.build(pocket1, handCards1.getCards());

		Pocket pocket2 = new Pocket(Card.build("Js"), Card.build("Jd"));
		Flop flop2 = new Flop(Card.build("Kc"), Card.build("Qh"), Card.build("7c"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2));
		CompletedPair pair2 = CompletedPair.build(pocket2, handCards2.getCards());

		assertTrue(pair1.compareTo(pair2) > 0);
		assertTrue(pair2.compareTo(pair1) < 0);
	}

	@Test
	public void testCompareByKicker1() {
		Pocket pocket1 = new Pocket(Card.build("As"), Card.build("Ad"));
		Flop flop1 = new Flop(Card.build("Tc"), Card.build("Kh"), Card.build("7c"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1));
		CompletedPair pair1 = CompletedPair.build(pocket1, handCards1.getCards());

		Pocket pocket2 = new Pocket(Card.build("As"), Card.build("Ad"));
		Flop flop2 = new Flop(Card.build("Tc"), Card.build("Qh"), Card.build("7c"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2));
		CompletedPair pair2 = CompletedPair.build(pocket2, handCards2.getCards());

		assertTrue(pair1.compareTo(pair2) > 0);
		assertTrue(pair2.compareTo(pair1) < 0);
	}

	@Test
	public void testCompareByKicker2() {
		Pocket pocket1 = new Pocket(Card.build("As"), Card.build("Ad"));
		Flop flop1 = new Flop(Card.build("Jc"), Card.build("Qh"), Card.build("7c"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1));
		CompletedPair pair1 = CompletedPair.build(pocket1, handCards1.getCards());

		Pocket pocket2 = new Pocket(Card.build("As"), Card.build("Ad"));
		Flop flop2 = new Flop(Card.build("Tc"), Card.build("Qh"), Card.build("7c"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2));
		CompletedPair pair2 = CompletedPair.build(pocket2, handCards2.getCards());

		assertTrue(pair1.compareTo(pair2) > 0);
		assertTrue(pair2.compareTo(pair1) < 0);
	}

	@Test
	public void testCompareByKicker3() {
		Pocket pocket1 = new Pocket(Card.build("As"), Card.build("Ad"));
		Flop flop1 = new Flop(Card.build("Tc"), Card.build("Qh"), Card.build("8c"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1));
		CompletedPair pair1 = CompletedPair.build(pocket1, handCards1.getCards());

		Pocket pocket2 = new Pocket(Card.build("As"), Card.build("Ad"));
		Flop flop2 = new Flop(Card.build("Tc"), Card.build("Qh"), Card.build("7c"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2));
		CompletedPair pair2 = CompletedPair.build(pocket2, handCards2.getCards());

		assertTrue(pair1.compareTo(pair2) > 0);
		assertTrue(pair2.compareTo(pair1) < 0);
	}

	@Test
	public void testCompareSameHand() {
		Pocket pocket1 = new Pocket(Card.build("As"), Card.build("Ad"));
		Flop flop1 = new Flop(Card.build("Tc"), Card.build("Qh"), Card.build("7c"));
		HandCards handCards1 = new HandCards(pocket1, new Board(flop1, Card.build("4d")));
		CompletedPair pair1 = CompletedPair.build(pocket1, handCards1.getCards());

		Pocket pocket2 = new Pocket(Card.build("As"), Card.build("Ad"));
		Flop flop2 = new Flop(Card.build("Tc"), Card.build("Qh"), Card.build("7c"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2, Card.build("5d")));
		CompletedPair pair2 = CompletedPair.build(pocket2, handCards2.getCards());

		assertEquals(0, pair1.compareTo(pair2));
		assertEquals(0, pair2.compareTo(pair1));
	}
	
	@Test
	public void testPair_TwoKings_AceAndQueenOnFlop() {
		Pocket pocket = new Pocket(Card.build("Ks"), Card.build("3d"));
		Flop flop = new Flop(Card.build("Kc"), Card.build("Qh"), Card.build("Ac"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		CompletedPair result = CompletedPair.build(pocket, handCards.getCards());
		assertTrue(result.isAtLeastMidPair());
	}
	
	@Test
	public void testPair_TwoQueens_AceAndQueenOnFlop() {
		Pocket pocket = new Pocket(Card.build("Qs"), Card.build("Qd"));
		Flop flop = new Flop(Card.build("Kc"), Card.build("3h"), Card.build("Ac"));
		HandCards handCards = new HandCards(pocket, new Board(flop));
		CompletedPair result = CompletedPair.build(pocket, handCards.getCards());
		assertFalse(result.isAtLeastMidPair());
	}
	
	@Test
	public void testPair_DryBoard_PairMidPair_WorseThanSecondKicker() {
		Pocket pocket = new Pocket(Card.build("Js"), Card.build("8s"));
		Flop flop = new Flop(Card.build("3h"), Card.build("Kc"), Card.build("8h"));
		Board board = new Board(flop);
		HandCards handCards = new HandCards(pocket, board);
		CompletedPair pair = CompletedPair.build(pocket, handCards.getCards());
		assertFalse(pair.isSecondKickerOrBetter());
		
	}
	
	@Test
	public void testPair_NonPairedDryBoard_PairTopPair_BadKicker() {
		Pocket pocket = new Pocket(Card.build("2s"), Card.build("Ks"));
		Flop flop = new Flop(Card.build("3h"), Card.build("Kc"), Card.build("8h"));
		Board board = new Board(flop);
		HandCards handCards = new HandCards(pocket, board);
		CompletedPair pair = CompletedPair.build(pocket, handCards.getCards());
		assertNotNull(pair);
		assertFalse(pair.isOverpair());
	}
	
	@Test
	public void testPair_OverpairIsAtLeastTopPair() {
		Pocket pocket = new Pocket(Card.build("Kc"), Card.build("Ks"));
		Flop flop = new Flop(Card.build("3h"), Card.build("Qc"), Card.build("8h"));
		Board board = new Board(flop);
		HandCards handCards = new HandCards(pocket, board);
		CompletedPair pair = CompletedPair.build(pocket, handCards.getCards());
		assertTrue(pair.isAtLeastTopPair());
		assertTrue(pair.isOverpair());
	}
	
	@Test
	public void testPair_isOverPair() {
		Pocket pocket = new Pocket(Card.build("Kc"), Card.build("Ks"));
		Flop flop = new Flop(Card.build("3h"), Card.build("Qc"), Card.build("8h"));
		Board board = new Board(flop);
		HandCards handCards = new HandCards(pocket, board);
		CompletedPair pair = CompletedPair.build(pocket, handCards.getCards());
		assertTrue(pair.isOverpair());
	}
}
