package com.ldepablo.poker.educa.model.completedhands;

import static org.junit.Assert.*;

import org.junit.Test;

import com.ldepablo.poker.educa.model.Board;
import com.ldepablo.poker.educa.model.Card;
import com.ldepablo.poker.educa.model.CardRank;
import com.ldepablo.poker.educa.model.Flop;
import com.ldepablo.poker.educa.model.GenericHandRank;
import com.ldepablo.poker.educa.model.HandCards;
import com.ldepablo.poker.educa.model.Pocket;
import com.ldepablo.poker.educa.model.Suit;

public class CompletedStraightFlushTest {

	private final Pocket pocket = new Pocket(Card.build("Qs"), Card.build("Ts"));
	
	@Test
	public void testGetGenericHand() {
		Flop flop = new Flop(Card.build("Kh"), Card.build("Js"), Card.build("9s"));
		HandCards handCards = new HandCards(pocket, new Board(flop,  Card.build("Ks")));
		CompletedStraightFlush completedStraightFlush = CompletedStraightFlush.build(pocket, handCards.getCards());
		assertEquals(GenericHandRank.STRAIGHT_FLUSH, completedStraightFlush.getGenericHandRank());
	}
	
	@Test
	public void testNonPossibleHandReturnsNull() {
		Flop flop = new Flop(Card.build("Kh"), Card.build("Jh"), Card.build("9s"));
		HandCards handCards = new HandCards(pocket, new Board(flop,  Card.build("Ks")));
		assertNull(CompletedStraightFlush.build(pocket, handCards.getCards()));
	}
	
	@Test
	public void testGetBestPocket() {
		Flop flop = new Flop(Card.build("Kh"), Card.build("Js"), Card.build("9s"));
		HandCards handCards = new HandCards(pocket, new Board(flop,  Card.build("Ks")));
		CompletedStraightFlush completedStraightFlush = CompletedStraightFlush.build(pocket, handCards.getCards());
		assertEquals(1, completedStraightFlush.getBestPocketPosition().intValue());
	}
	
	@Test
	public void testGetBestPocketWith5HighSF() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("4s"));
		Flop flop = new Flop(Card.build("3s"), Card.build("2s"), Card.build("5s"));
		HandCards handCards = new HandCards(pocket, new Board(flop,  Card.build("Qs")));
		CompletedStraightFlush completedStraightFlush = CompletedStraightFlush.build(pocket, handCards.getCards());
		assertEquals(CardRank.FIVE, completedStraightFlush.getHighestRank());
		assertEquals(1, completedStraightFlush.getBestPocketPosition().intValue());
		assertEquals(4, completedStraightFlush.getSecondBestPocketPosition().intValue());
	}
	
	@Test
	public void testGetBestPocketWith5HighSF2() {
		Pocket pocket = new Pocket(Card.build("Qs"), Card.build("4s"));
		Flop flop = new Flop(Card.build("3s"), Card.build("2s"), Card.build("5s"));
		HandCards handCards = new HandCards(pocket, new Board(flop,  Card.build("As")));
		CompletedStraightFlush completedStraightFlush = CompletedStraightFlush.build(pocket, handCards.getCards());
		assertEquals(CardRank.FIVE, completedStraightFlush.getHighestRank());
		assertEquals(1, completedStraightFlush.getBestPocketPosition().intValue());
		assertNull(completedStraightFlush.getSecondBestPocketPosition());
	}
	
	@Test
	public void testGetBestPocketWith6HighSF() {
		Pocket pocket = new Pocket(Card.build("As"), Card.build("4s"));
		Flop flop = new Flop(Card.build("3s"), Card.build("2s"), Card.build("5s"));
		HandCards handCards = new HandCards(pocket, new Board(flop,  Card.build("6s")));
		CompletedStraightFlush completedStraightFlush = CompletedStraightFlush.build(pocket, handCards.getCards());
		assertEquals(CardRank.SIX, completedStraightFlush.getHighestRank());
		assertEquals(2, completedStraightFlush.getBestPocketPosition().intValue());
		assertNull(completedStraightFlush.getSecondBestPocketPosition());
	}
	
	@Test
	public void testGetSecondPocket() {
		Flop flop = new Flop(Card.build("Kh"), Card.build("Js"), Card.build("9s"));
		HandCards handCards = new HandCards(pocket, new Board(flop,  Card.build("Ks")));
		CompletedStraightFlush completedStraightFlush = CompletedStraightFlush.build(pocket, handCards.getCards());
		assertEquals(3, completedStraightFlush.getSecondBestPocketPosition().intValue());
	}
	
	@Test
	public void testHighestRank() {
		Flop flop = new Flop(Card.build("Kh"), Card.build("Js"), Card.build("9s"));
		HandCards handCards = new HandCards(pocket, new Board(flop,  Card.build("Ks")));
		CompletedStraightFlush completedStraightFlush = CompletedStraightFlush.build(pocket, handCards.getCards());
		assertEquals(CardRank.KING, completedStraightFlush.getHighestRank());
	}
	
	@Test
	public void testPocketsPositions1() {
		Pocket pocket = new Pocket(Card.build("Ks"), Card.build("Ts"));
		Flop flop = new Flop(Card.build("Kh"), Card.build("Js"), Card.build("9s"));
		HandCards handCards = new HandCards(pocket, new Board(flop,  Card.build("Qs")));
		CompletedStraightFlush completedStraightFlush = CompletedStraightFlush.build(pocket, handCards.getCards());
		assertEquals(0, completedStraightFlush.getBestPocketPosition().intValue());
		assertEquals(CardRank.KING, completedStraightFlush.getHighestRank());
		assertEquals(0, completedStraightFlush.getBestPocketPosition().byteValue());
		assertEquals(3, completedStraightFlush.getSecondBestPocketPosition().byteValue());
		assertEquals(Suit.SPADES, completedStraightFlush.getSuit());
	}
	
	@Test
	public void testPocketsPositions2() {
		Pocket pocket = new Pocket(Card.build("Ks"), Card.build("9s"));
		Flop flop = new Flop(Card.build("Kh"), Card.build("Js"), Card.build("Ts"));
		HandCards handCards = new HandCards(pocket, new Board(flop,  Card.build("Qs")));
		CompletedStraightFlush completedStraightFlush = CompletedStraightFlush.build(pocket, handCards.getCards());
		assertEquals(0, completedStraightFlush.getBestPocketPosition().intValue());
		assertEquals(CardRank.KING, completedStraightFlush.getHighestRank());
		assertEquals(0, completedStraightFlush.getBestPocketPosition().byteValue());
		assertEquals(4, completedStraightFlush.getSecondBestPocketPosition().byteValue());
	}
	
	@Test
	public void testPocketsPositions3() {
		Pocket pocket = new Pocket(Card.build("Ks"), Card.build("Kh"));
		Flop flop = new Flop(Card.build("Ts"), Card.build("Js"), Card.build("9s"));
		HandCards handCards = new HandCards(pocket, new Board(flop,  Card.build("Qs")));
		CompletedStraightFlush completedStraightFlush = CompletedStraightFlush.build(pocket, handCards.getCards());
		assertEquals(0, completedStraightFlush.getBestPocketPosition().intValue());
		assertEquals(CardRank.KING, completedStraightFlush.getHighestRank());
		assertEquals(0, completedStraightFlush.getBestPocketPosition().byteValue());
		assertNull(completedStraightFlush.getSecondBestPocketPosition());
	}
	
	@Test
	public void testPocketsPositions4() {
		Pocket pocket = new Pocket(Card.build("Ks"), Card.build("8s"));
		Flop flop = new Flop(Card.build("9s"), Card.build("Js"), Card.build("Ts"));
		HandCards handCards = new HandCards(pocket, new Board(flop,  Card.build("Qs")));
		CompletedStraightFlush completedStraightFlush = CompletedStraightFlush.build(pocket, handCards.getCards());
		assertEquals(0, completedStraightFlush.getBestPocketPosition().intValue());
		assertEquals(CardRank.KING, completedStraightFlush.getHighestRank());
		assertEquals(0, completedStraightFlush.getBestPocketPosition().byteValue());
		assertNull(completedStraightFlush.getSecondBestPocketPosition());
	}
	
	@Test
	public void testGetBestPocketWithPocketCardsBeingUseless() {
		Pocket pocket = new Pocket(Card.build("Ac"), Card.build("Th"));
		Flop flop = new Flop(Card.build("Ks"), Card.build("Js"), Card.build("9s"));
		HandCards handCards = new HandCards(pocket, new Board(flop,  Card.build("Qs"), Card.build("Ts")));
		CompletedStraightFlush completedStraightFlush = CompletedStraightFlush.build(pocket, handCards.getCards());
		assertNull(completedStraightFlush.getBestPocketPosition());
	}
	
	@Test
	public void testGetsBestPossibleStraightFlush() {
		Flop flop = new Flop(Card.build("As"), Card.build("Js"), Card.build("9s"));
		HandCards handCards = new HandCards(pocket, new Board(flop,  Card.build("Ks")));
		CompletedStraightFlush completedHand = CompletedStraightFlush.build(pocket, handCards.getCards());
		assertEquals(CardRank.ACE, completedHand.getHighestRank());
		assertEquals(2, completedHand.getBestPocketPosition().longValue());
	}
	
	@Test
	public void testCompareToInferiorStraightFlush() {
		Flop flop = new Flop(Card.build("As"), Card.build("Js"), Card.build("9s"));
		HandCards handCards = new HandCards(pocket, new Board(flop,  Card.build("Ks")));
		CompletedStraightFlush completedHand = CompletedStraightFlush.build(pocket, handCards.getCards());
		

		Flop flop1 = new Flop(Card.build("5h"), Card.build("Js"), Card.build("9s"));
		HandCards handCards1 = new HandCards(pocket, new Board(flop1,  Card.build("Ks")));
		CompletedStraightFlush completedHand1 = CompletedStraightFlush.build(pocket, handCards1.getCards());
		
		assertTrue(completedHand.compareTo(completedHand1) > 0);
	}
	
	@Test
	public void testCompareToHigherStraightFlush() {
		Flop flop = new Flop(Card.build("As"), Card.build("Js"), Card.build("9s"));
		HandCards handCards = new HandCards(pocket, new Board(flop,  Card.build("Ks")));
		CompletedStraightFlush completedHand = CompletedStraightFlush.build(pocket, handCards.getCards());
		

		Flop flop1 = new Flop(Card.build("5h"), Card.build("Js"), Card.build("9s"));
		HandCards handCards1 = new HandCards(pocket, new Board(flop1,  Card.build("Ks")));
		CompletedStraightFlush completedHand1 = CompletedStraightFlush.build(pocket, handCards1.getCards());
		
		assertTrue(completedHand1.compareTo(completedHand) < 0);
	}
	
	@Test
	public void testCompareToAnyOtherCompletedHand() {
		Flop flop = new Flop(Card.build("As"), Card.build("Js"), Card.build("9s"));
		HandCards handCards = new HandCards(pocket, new Board(flop,  Card.build("Ks")));
		CompletedStraightFlush completedHand = CompletedStraightFlush.build(pocket, handCards.getCards());
		

		Pocket pocket2 = new Pocket(Card.build("As"), Card.build("Qd"));
		Flop flop2 = new Flop(Card.build("Tc"), Card.build("Jh"), Card.build("7c"));
		HandCards handCards2 = new HandCards(pocket2, new Board(flop2));
		
		assertTrue(completedHand.compareTo(CompletedPair.build(pocket2, handCards2.getCards())) > 0);
	}
	
}
