package com.ldepablo.poker.educa.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class FlopTest {

	private Card card1 = new Card(CardRank.SEVEN, Suit.DIAMONDS);
	private Card card2 = new Card(CardRank.KING, Suit.HEARTS);
	private Card card3 = new Card(CardRank.ACE, Suit.CLUBS);
	
	private Flop flop = new Flop(card1, card2, card3);
	
	@Test
	public void testGetCard1() {
		assertEquals(card1, flop.getCard1());
	}
	
	@Test
	public void testGetCard2() {
		assertEquals(card2, flop.getCard2());
	}
	
	@Test
	public void testGetCard3() {
		assertEquals(card3, flop.getCard3());
	}
	
	@Test
	public void testIsTwoSuited() {
		
		Flop flop = new Flop(card1, card2, card3);
		assertFalse(flop.isTwoSuited());

		Card card4 = new Card(CardRank.QUEEN, card1.getSuit());
		flop = new Flop(card1, card2, card4);
		assertTrue(flop.isTwoSuited());
	}
	
	@Test
	public void testIsOneColored() {
		
		Flop flop = new Flop(card1, card2, card3);
		assertFalse(flop.isOneColor());

		Card card4 = new Card(CardRank.QUEEN, card1.getSuit());
		Card card5 = new Card(CardRank.QUEEN, card1.getSuit());
		flop = new Flop(card1, card4, card5);
		assertTrue(flop.isOneColor());
	}
}
