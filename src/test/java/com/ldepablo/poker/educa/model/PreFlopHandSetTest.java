package com.ldepablo.poker.educa.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class PreFlopHandSetTest {
	
	@Test
	public void testContains() {
		PreFlopHandSet handSet = new PreFlopHandSet();
		PreFlopHand preFlopHand1 = new PreFlopHand(CardRank.ACE, CardRank.KING, true);
		handSet.add(preFlopHand1);
		handSet.add(new PreFlopHand(CardRank.ACE, CardRank.KING, false));
		handSet.add(new PreFlopHand(CardRank.ACE, CardRank.ACE, false));
		handSet.add(new PreFlopHand(CardRank.KING, CardRank.KING, false));
		handSet.add(new PreFlopHand(CardRank.QUEEN, CardRank.QUEEN, false));
		handSet.add(new PreFlopHand(CardRank.JACK, CardRank.JACK, false));

		assertTrue(handSet.contains(preFlopHand1));
		assertTrue(handSet.contains(new PreFlopHand(CardRank.ACE, CardRank.KING, false)));
		assertFalse(handSet.contains(new PreFlopHand(CardRank.ACE, CardRank.TWO, false)));
	}
}
