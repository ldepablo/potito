package com.ldepablo.poker.educa.model;

import com.ldepablo.poker.educa.model.completedhands.CompletedHand;

public interface IStrengthCalculator {

	public HandStrength calculate(Board board, CompletedHand hand);
}
