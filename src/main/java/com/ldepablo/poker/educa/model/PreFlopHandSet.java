package com.ldepablo.poker.educa.model;

import java.util.HashSet;
import java.util.Set;

public class PreFlopHandSet {
	
	Set<PreFlopHand> set = new HashSet<PreFlopHand>();
	
	public boolean add(PreFlopHand preFlopHand) {
		return set.add(preFlopHand);
	}

	public boolean contains(PreFlopHand preFlopHand) {
		return set.contains(preFlopHand);
	}
	
}
