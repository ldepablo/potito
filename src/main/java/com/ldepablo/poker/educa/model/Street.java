package com.ldepablo.poker.educa.model;

public enum Street {
	PRE_FLOP,
	FLOP,
	TURN,
	RIVER
}
