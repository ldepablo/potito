package com.ldepablo.poker.educa.model.completedhands;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lombok.Getter;

import com.ldepablo.poker.educa.model.Card;
import com.ldepablo.poker.educa.model.CardRank;
import com.ldepablo.poker.educa.model.GenericHandRank;
import com.ldepablo.poker.educa.model.Pocket;
import com.ldepablo.poker.educa.model.Utils;

public class CompletedPair implements CompletedHand {

	@Getter private final CardRank rank;
	@Getter private final CardRank kicker1;
	@Getter private final CardRank kicker2;
	@Getter private final CardRank kicker3;
	@Getter private final Byte bestPocketPosition;
	@Getter private final Byte secondBestPocketPosition;
	
	/**
	 * is true if the pair is a pair for the second (or better) best rank in the table or better.
	 */
	@Getter private boolean atLeastMidPair;
	
	/**
	 * is true if the pair is a pair for the first best rank in the table or overpair.
	 */
	@Getter private boolean atLeastTopPair;
	
	/**
	 * true if any:
	 * 1. pair rank is A and first kicker is K or Q
	 * 2. pair rank is K and first kicker is A or Q
	 * 3. first kicker K
	 */
	@Getter private boolean secondKickerOrBetter;
	
	
	private Boolean overpair;

	public CompletedPair(CardRank rankPaired, CardRank kicker1, CardRank kicker2, CardRank kicker3, Byte bestPocketPos, Byte secondBestPocketPos, boolean atLeastMidPair, boolean atLeastTopPair, boolean secondKickerOrBetter) {
		this.rank = rankPaired;
		this.kicker1 = kicker1;
		this.kicker2 = kicker2;
		this.kicker3 = kicker3;
		this.bestPocketPosition = bestPocketPos;
		this.secondBestPocketPosition = secondBestPocketPos;
		this.atLeastMidPair = atLeastMidPair;
		this.atLeastTopPair = atLeastTopPair;
		this.secondKickerOrBetter = secondKickerOrBetter;
	}

	public static CompletedPair build(Pocket pocket, Collection<Card> allCards) {
		int[] ranks = Utils.getRanksQuantitiesInArray(allCards);
		Set<CardRank> pocketRanks = Utils.getRanksSet(pocket.getCards());
		Boolean secondKickerOrBetter = null;
		CardRank rankPaired = null;
		CardRank kicker1 = null;
		CardRank kicker2 = null;
		CardRank kicker3 = null;
		Byte bestPocketPos = null;
		Byte secondBestPocketPos = null;
		int i=12;
		int missingRanks=0;
		do {
			switch(ranks[i]) {
			case 2:
				if (rankPaired == null) {
					rankPaired = CardRank.fromValue(i+2);

					if (pocketRanks.contains(rankPaired)) {
						if (pocket.isPair())
							secondBestPocketPos = 0;
						else if (bestPocketPos != null)
							secondBestPocketPos = bestPocketPos;

						bestPocketPos = 0;
					}
				}
				break;
			case 1:
				CardRank currentRank = CardRank.fromValue(i+2);
				if (kicker1 == null) {
					kicker1 = currentRank;
					if (pocketRanks.contains(currentRank)) {
						if (secondKickerOrBetter == null && missingRanks<2)
							secondKickerOrBetter = true;
						if (bestPocketPos == null)
							bestPocketPos = 2;
						else if (secondBestPocketPos == null)
							secondBestPocketPos = 2;
					}
				} else if (kicker2 == null) {
					kicker2 = currentRank;
					if (pocketRanks.contains(currentRank)) {
						if (secondKickerOrBetter == null && missingRanks<2)
							secondKickerOrBetter = true;
						if (bestPocketPos == null)
							bestPocketPos = 3;
						else if (secondBestPocketPos == null)
							secondBestPocketPos = 3;
					}
				} else if (kicker3 == null) {
					kicker3 = currentRank;
					if (pocketRanks.contains(currentRank)) {
						if (secondKickerOrBetter == null && missingRanks<2)
							secondKickerOrBetter = true;
						if (bestPocketPos == null)
							bestPocketPos = 4;
						else if (secondBestPocketPos == null)
							secondBestPocketPos = 4;
					}
				}
				break;
			case 0:
				++missingRanks;
					
			default:
			}
		} while (--i>=0 && (kicker3 == null || rankPaired == null));
		if (rankPaired == null)
			return null;
		
		boolean atLeastMidPair = false;
		boolean atLeastTopPair = false;
		

		List<CardRank> commonCardsRanks = getSortedCommonCardsRanks(pocket, allCards);
		
		int commonRanksBetterThanPair = calculateAtLeastMidAndTopPair(pocket, commonCardsRanks, rankPaired);
		if (commonRanksBetterThanPair == 0) {
			atLeastTopPair = true;
			atLeastMidPair = true;
		} else if (commonRanksBetterThanPair == 1)
			atLeastMidPair = true;
		
		//TODO We probably don't need to check whether it's pocket pair
		if (secondKickerOrBetter == null || pocket.isPair())
			secondKickerOrBetter = false;
		
		return new CompletedPair(rankPaired, kicker1, kicker2, kicker3, bestPocketPos, secondBestPocketPos, atLeastMidPair, atLeastTopPair, secondKickerOrBetter);
	}

	private static List<CardRank> getSortedCommonCardsRanks(Pocket pocket,
			Collection<Card> allCards) {
		HashSet<Card> commonCards = new HashSet<Card>(allCards);
		commonCards.removeAll(pocket.getCards());
		List<CardRank> commonCardsRanks = new ArrayList<CardRank>(Utils.getRanksSet(commonCards));
		Collections.sort(commonCardsRanks);
		return commonCardsRanks;
	}

	private static int calculateAtLeastMidAndTopPair(Pocket pocket, List<CardRank> commonCardsRanks, CardRank rankPaired) {
		int i;
		
		int size = commonCardsRanks.size();
		i=0;
		boolean found = false;
		do {
			CardRank currentRank = commonCardsRanks.get(size-1-i);
			if (CardRank.compare(currentRank, rankPaired) <=0)
				found = true;
			i++;
		} while (i<2 && !found);
		if (found)
			return i-1;
		return 2;
	}

	@Override
	public GenericHandRank getGenericHandRank() {
		return GenericHandRank.PAIR;
	}

	@Override
	public int compareTo(CompletedHand o) {
		if (o instanceof CompletedPair) {
			CompletedPair other = (CompletedPair) o;
			int result = rank.compareTo(other.getRank());
			if (result == 0) {
				result = kicker1.compareTo(other.getKicker1());
				if (result == 0) {
					result = kicker2.compareTo(other.getKicker2());
					if (result == 0)
						result = kicker3.compareTo(other.getKicker3());
				}
			}
			return result;
		}
		return GenericHandRank.PAIR.compareTo(o.getGenericHandRank());
		
	}

	public boolean isOverpair() {
		if (overpair == null)
			overpair = bestPocketPosition != null && bestPocketPosition == 0 
			&& secondBestPocketPosition != null && secondBestPocketPosition == 0 
			&& CardRank.compare(rank, kicker1) > 0;
			
		return overpair;
	}

	

}
