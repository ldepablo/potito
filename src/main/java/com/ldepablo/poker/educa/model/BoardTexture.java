package com.ldepablo.poker.educa.model;

public enum BoardTexture {
	DRY,
	SEMICOORDINATED,
	COORDINATED,
	EXTREMELY_COORDINATED;

}
