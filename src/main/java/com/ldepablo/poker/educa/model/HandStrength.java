package com.ldepablo.poker.educa.model;

public enum HandStrength {
	NOTHING,
	WEAK,
	MEDIUM,
	STRONG,
	VERY_STRONG;
}
