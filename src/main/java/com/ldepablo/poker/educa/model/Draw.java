package com.ldepablo.poker.educa.model;


/**
 * It's a draw. It will have adjusted outs in itself.
 * 
 * HandCards will have a set of Draws and it will add them up to calculate outs
 * TODO It probably needs some tweaking because given two draws it could happen that
 * outs of Draw 1 + outs of Draw 2 < 3 outs
 *  
 * @author ldepablo
 *
 */
public interface Draw {

	public GenericHandRank getGenericHandRank();

}
