package com.ldepablo.poker.educa.model.outs;

import java.util.Collection;

import com.ldepablo.poker.educa.model.Board;
import com.ldepablo.poker.educa.model.BoardTexture;
import com.ldepablo.poker.educa.model.Card;
import com.ldepablo.poker.educa.model.CardRank;
import com.ldepablo.poker.educa.model.Pocket;
import com.ldepablo.poker.educa.model.Utils;

/**
 * Calculates outs for overcards and for semi overcards
 * 
 * @author ldepablo
 *
 */
public class OvercardsOutsCalculator {


	private final Pocket pocket;
	private final Board board;
	
	public OvercardsOutsCalculator(Pocket pocket, Board board){
		this.pocket = pocket;
		this.board = board;
	}

	public double calculate() {
		if (pocket.isPair())
			return 0;

		int overcards = getOvercardsTo(board.getHighestRank());

		int highPairs = getHighPairs();

		switch(board.getTexture()) {
		case EXTREMELY_COORDINATED:
			return 0;
		case COORDINATED:
			return Math.max(0, overcards-(highPairs*overcards));
		case SEMICOORDINATED:
			return Math.max(0, overcards*(2-highPairs));
		case DRY:
		 	return Math.max(0, overcards*(2-highPairs)) + getHalfOutForSemiOvercard();
		default:
			throw new RuntimeException("Got weird BoardTexture case: " + board.getTexture());
		}
		
	}

	private short getOvercardsTo(CardRank highestCardInBoard) {
		short overcards = 0;
		for (Card current : pocket.getCards()) {
			CardRank rank = current.getRank();
			int compareTo = new CardRank.RankComparator().compare(rank, highestCardInBoard);
			if (compareTo > 0)
				overcards ++;
		}

		return overcards;
	}

	private short getHighPairs() {
		if (!board.isPaired())
			return 0;

		Collection<CardRank> pairs = Utils.getPairs(board.getCards());
		short highPairs = 0;
		for (CardRank number : pairs)
			if (number.compareTo(CardRank.SEVEN)>0)
				highPairs++;

		return highPairs;
	}

	public double getHalfOutForSemiOvercard() {
		if (!BoardTexture.DRY.equals(board.getTexture()))
			return 0;

		CardRank highestRankInBoard = board.getHighestRank();
		CardRank secondHighestRankInBoard = board.getSecondHighestRank();
		int semiOvercards = 0;
		for(Card card : pocket.getCards()) {
			if (card.compareTo(highestRankInBoard)<0 && card.compareTo(secondHighestRankInBoard)>0)
				semiOvercards++;
		}	
		return semiOvercards * 0.5;
	}

}
