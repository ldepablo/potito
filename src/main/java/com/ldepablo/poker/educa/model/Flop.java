package com.ldepablo.poker.educa.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import lombok.Getter;

/**
 * The three cards of a flop. It should include:
 * 1. The 3 cards (and qualities)
 * 2. SPR
 *  
 * @author ldepablo
 *
 */
public class Flop {

	@Getter private final Card card1;
	@Getter private final Card card2;
	@Getter private final Card card3;
	@Getter private final Collection<Card> cards = new HashSet<Card>();
	
	public Flop(Card card1, Card card2, Card card3) {
		this.card1 = card1;
		this.card2 = card2;
		this.card3 = card3;

		cards.add(card1);
		cards.add(card2);
		cards.add(card3);
	}

	public boolean isTwoSuited() {
		Collection<Card> cards = new ArrayList<Card>();
		cards.add(card1);
		cards.add(card2);
		cards.add(card3);
		return Utils.suitedCards(cards) == 2;
	}
	
	public boolean isOneColor() {
		Collection<Card> cards = new ArrayList<Card>();
		cards.add(card1);
		cards.add(card2);
		cards.add(card3);
		return Utils.suitedCards(cards) == 3;
	}

	@Override
	public String toString() {
		return cards.toString();
	}
}
