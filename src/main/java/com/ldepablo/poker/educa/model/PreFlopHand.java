package com.ldepablo.poker.educa.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class PreFlopHand {
	
	private final List<CardRank> ranks;
	private boolean suited;
	
	public PreFlopHand(CardRank rank0, CardRank rank1, boolean suited) {
		ranks = new ArrayList<CardRank>();
		ranks.add(CardRank.compare(rank0, rank1) <= 0 ? rank0 : rank1);
		ranks.add(CardRank.compare(rank0, rank1) <= 0 ? rank1 : rank0);
		this.suited = suited;
	}
	
	public PreFlopHand(Pocket pocket) {
		ranks = new ArrayList<CardRank>();
		CardRank rank1 = pocket.getCard1().getRank();
		CardRank rank2 = pocket.getCard2().getRank();
		ranks.add(CardRank.compare(rank1, rank2) >= 0 ? rank1 : rank2);
		ranks.add(CardRank.compare(rank1, rank2) >= 0 ? rank2 : rank1);
		Collections.sort(ranks);
		suited = pocket.isSuited();
	}
	
	@Override
	public String toString() {
		return ranks.get(1).getMyChar() + ranks.get(0).getMyChar() + (suited ? "s" : "");
	}
	
	@Override
	public boolean equals(Object other) {
		return EqualsBuilder.reflectionEquals(this, other);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	

}
