package com.ldepablo.poker.educa.model;

import com.ldepablo.poker.educa.model.completedhands.CompletedFlush;
import com.ldepablo.poker.educa.model.completedhands.CompletedFullHouse;
import com.ldepablo.poker.educa.model.completedhands.CompletedHand;
import com.ldepablo.poker.educa.model.completedhands.CompletedHighCard;
import com.ldepablo.poker.educa.model.completedhands.CompletedPair;
import com.ldepablo.poker.educa.model.completedhands.CompletedStraight;
import com.ldepablo.poker.educa.model.completedhands.CompletedTwoPair;

public class StrengthCalculator implements IStrengthCalculator{

	@Override
	public HandStrength calculate(Board board, CompletedHand hand) {
		switch (board.getTexture()) {
		case DRY:
		case SEMICOORDINATED:
			switch(hand.getGenericHandRank()) {
			case HIGH_CARD:
				CompletedHighCard highCardHand = (CompletedHighCard) hand;
				if (highCardHand.getRank1().equals(CardRank.ACE))
					return HandStrength.WEAK;
				else
					return HandStrength.NOTHING;

			case PAIR:
				CompletedPair pair = (CompletedPair) hand;
				if (!pair.isAtLeastMidPair())
					return HandStrength.WEAK;
				else {
					if (!pair.isAtLeastTopPair()) {
						if (pair.isSecondKickerOrBetter())
							return HandStrength.MEDIUM;
						else
							return HandStrength.WEAK;
					} else {
						if (((!pair.isOverpair() && pair.isSecondKickerOrBetter()) || (pair.isOverpair())) && !board.isPaired())
							return HandStrength.STRONG;
						else
							return HandStrength.MEDIUM;
					}
				}
			case TWO_PAIR:
				CompletedTwoPair twoPair = (CompletedTwoPair) hand;
				if (board.isPaired())
					return HandStrength.MEDIUM;
				else {
					if (twoPair.isAtLeastBestTwoPair())
						return HandStrength.VERY_STRONG;
					else
						return HandStrength.STRONG;
				}
			case THREE_OF_A_KIND:
			case STRAIGHT:
				if (board.isDoublePaired())
					return HandStrength.MEDIUM;
				else if (board.isPaired())
					return HandStrength.STRONG;
				else
					return HandStrength.VERY_STRONG;

			case FLUSH:
				throw new RuntimeException("Found FLUSH in DRY/SEMI board");

			case FULL_HOUSE:
				if (board.isDoublePaired())
					return HandStrength.STRONG;
				else
					return HandStrength.VERY_STRONG;

			case FOUR_OF_A_KIND:
				return HandStrength.VERY_STRONG;

			case STRAIGHT_FLUSH:
				throw new RuntimeException("Found STRAIGHT_FLUSH in DRY/SEMI board");
			}




		case COORDINATED:
			switch(hand.getGenericHandRank()) {
			case HIGH_CARD:
				CompletedHighCard highCardHand = (CompletedHighCard) hand;
				if (!highCardHand.getRank1().equals(CardRank.ACE))
					return HandStrength.NOTHING;
				else { 
					Byte bestPocketPosition = hand.getBestPocketPosition();
					if (bestPocketPosition != null && bestPocketPosition == 0)
						return HandStrength.WEAK;
				}
				break;

			case PAIR:
				CompletedPair pair = (CompletedPair) hand;
				if (!pair.isAtLeastTopPair())
					return HandStrength.WEAK;
				else
					return HandStrength.MEDIUM;

			case TWO_PAIR:
				if (board.isPaired())
					return HandStrength.MEDIUM;
				else
					return HandStrength.STRONG;

			case THREE_OF_A_KIND:
				return HandStrength.STRONG;

			case STRAIGHT:
				if (board.isDoublePaired())
					return HandStrength.MEDIUM;
				else if (board.isPaired() || board.getMaxConnectedCards()>=3)
					return HandStrength.STRONG;
				else
					return HandStrength.VERY_STRONG;

			case FLUSH:
				if (board.isPaired()) {
					if (board.isDoublePaired())
						return HandStrength.MEDIUM;
					else
						return HandStrength.STRONG;
				} else
					return HandStrength.VERY_STRONG;
			case FULL_HOUSE:
				if (board.isDoublePaired()) {
					if (((CompletedFullHouse) hand).isAtLeastBestFullHouse())
						return HandStrength.VERY_STRONG;
					else
						return HandStrength.STRONG;
				} else
					return HandStrength.VERY_STRONG;

			case FOUR_OF_A_KIND:
				return HandStrength.VERY_STRONG;

			case STRAIGHT_FLUSH:
				return HandStrength.VERY_STRONG;
			}
			
			
		case EXTREMELY_COORDINATED:
			switch(hand.getGenericHandRank()) {
			case HIGH_CARD:
			case PAIR:
			case TWO_PAIR:
				return HandStrength.NOTHING;

			case THREE_OF_A_KIND:
				return HandStrength.WEAK;

			case STRAIGHT:
				if (board.getSuitedCards() >= 4)
					return HandStrength.WEAK;
				else {
					CompletedStraight straight = (CompletedStraight) hand;
					switch (straight.getSuperiors()) {
					case 2:
						if (board.isPaired())
							return HandStrength.STRONG;
						else
							return HandStrength.VERY_STRONG;
					case 1:
						if (board.isPaired())
							return HandStrength.MEDIUM;
						else
							return HandStrength.STRONG;
					case 0:
						return HandStrength.MEDIUM;

					}
					return HandStrength.MEDIUM;
				}
			case FLUSH:
				if (board.getSuitedCards() == 4) {

					CompletedFlush flush = (CompletedFlush) hand;
					switch (flush.getCardsThatMakeABetterFlush()) {
					case 0:
						if (board.isPaired())
							return HandStrength.STRONG;
						else
							return HandStrength.VERY_STRONG;

					case 1:
						if (board.isPaired())
							return HandStrength.MEDIUM;
						else
							return HandStrength.STRONG;
					case 2:
					case 3:
					case 4:
						return HandStrength.MEDIUM;

					default:
						return HandStrength.WEAK;
					}

				} else {
					if (board.isPaired())
						return HandStrength.STRONG;
					else
						return HandStrength.VERY_STRONG;
				}

			case FULL_HOUSE:
				throw new RuntimeException("Found FULL HOUSE in EXTRE board");

			case FOUR_OF_A_KIND:
				return HandStrength.VERY_STRONG;

			case STRAIGHT_FLUSH:
				return HandStrength.VERY_STRONG;
			}

		default:
			throw new RuntimeException("Found unrecognised enum " + board.getTexture());
		}

		//		throw new RuntimeException("Couldn't find suitable case with board " + board + " hand " + hand);
	}
}
