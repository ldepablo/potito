package com.ldepablo.poker.educa.model;

public enum Suit {

	CLUBS("c"),
	DIAMONDS("d"),
	HEARTS("h"),
	SPADES("s");
	
	private final String myChar;

	private Suit(String myChar) {
		this.myChar = myChar;
	}
	
	@Override
	public String toString() {
		return myChar;
	}
}
