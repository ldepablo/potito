package com.ldepablo.poker.educa.model.completedhands;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lombok.Getter;

import com.ldepablo.poker.educa.model.Card;
import com.ldepablo.poker.educa.model.CardRank;
import com.ldepablo.poker.educa.model.GenericHandRank;
import com.ldepablo.poker.educa.model.Pocket;
import com.ldepablo.poker.educa.model.Suit;

public class CompletedStraightFlush implements CompletedHand{

	@Getter private Byte bestPocketPosition;
	@Getter private Byte secondBestPocketPosition;
	@Getter private CardRank highestRank;
	@Getter private Suit suit;

	private CompletedStraightFlush(CardRank highestRank, Suit suit, Byte bestPocketPosition, Byte secondBestPocketPosition) {
		this.bestPocketPosition = bestPocketPosition;
		this.secondBestPocketPosition = secondBestPocketPosition;
		this.highestRank = highestRank;
		this.suit = suit;
	}
	
	@Override
	public GenericHandRank getGenericHandRank() {
		return GenericHandRank.STRAIGHT_FLUSH;
	}
	
	public static CompletedStraightFlush build(Pocket pocket, Collection<Card> allCards) {
		for (Suit suit : Suit.values()) {
			Set<Integer> ranksOfSameSuitAsSet = new HashSet<Integer>();
			for (Card card : allCards) {
				if (suit.equals(card.getSuit())){
					CardRank rank = card.getRank();
					ranksOfSameSuitAsSet.add(rank.getValue());
					if (CardRank.ACE.equals(rank))
						ranksOfSameSuitAsSet.add(1);
				}
			}
			if (ranksOfSameSuitAsSet.size() >= 5) {
				ArrayList<Integer> rankValues = new ArrayList<Integer>(ranksOfSameSuitAsSet);
				Collections.sort(rankValues);

				int inARow = 0;
				int highestRank = rankValues.get(rankValues.size()-1);
				
				// By doing this backwards, I ensure that the first result I get is the best possible one.
				for (int i=rankValues.size()-2; i>=0; i--)
					if (Math.abs(rankValues.get(i) - rankValues.get(i+1)) == 1) {
						inARow++;
						if (inARow == 4) {

							List<Byte> pocketsPositions = selectPocketsPositions(suit, highestRank, pocket.getCards());
							
							Byte bestPocketPosition = null;
							if (!pocketsPositions.isEmpty()) {
								bestPocketPosition = pocketsPositions.get(0);
							}
							
							Byte secondBestPocketPosition = null;
							if (pocketsPositions.size()>1) {
								secondBestPocketPosition = pocketsPositions.get(1);
							}
							
							return new CompletedStraightFlush(CardRank.fromValue(highestRank), suit, bestPocketPosition, secondBestPocketPosition);
						}
					} else {
						highestRank = rankValues.get(i);
						inARow = 0;
					}
				// If we are here, we know there's no possible result because it's not possible to have 5 cards of more than one suit.
				return null;
			}
		}
		return null;
	}

	private static List<Byte> selectPocketsPositions(Suit suit, int highestValue, Collection<Card> pocketCards) {
		List<CardRank> pocketRanks = new ArrayList<CardRank>();
		for (Card card : pocketCards) {
			if (suit.equals(card.getSuit()))
				pocketRanks.add(card.getRank());
		}
		List<Byte> result = new ArrayList<Byte>(2);
		if (pocketRanks.isEmpty())
			return result;
		for (byte i=0; i<5; i++) {
			CardRank currentRank = CardRank.fromValue(highestValue-i);
			if (pocketRanks.contains(currentRank)) {
				result.add(i);
				if (result.size() == 2)
					break;
			}
		}
		return result;
	}

	@Override
	public int compareTo(CompletedHand o) {
		if (o instanceof CompletedStraightFlush) {
			CompletedStraightFlush other = (CompletedStraightFlush) o;
			return CardRank.compare(highestRank, other.highestRank);
		}
		return 1;
	}
	

	
}
