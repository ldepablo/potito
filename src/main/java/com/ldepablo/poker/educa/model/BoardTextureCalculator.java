package com.ldepablo.poker.educa.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class BoardTextureCalculator {
	
	private final Collection<Card> cards;

	private static final Set<Integer> EXCLUDED_CARD_VALUES = new HashSet<Integer>(Arrays.asList(2,3,4,5));

	private static Set<Set<Integer>> EXCLUDED_POCKETS = new HashSet<Set<Integer>>();
	{
		Set<Integer> excludedPocket = new HashSet<Integer>(2);
		excludedPocket.add(9);
		excludedPocket.add(6);
		EXCLUDED_POCKETS.add(excludedPocket);

		excludedPocket = new HashSet<Integer>(2);
		excludedPocket.add(8);
		excludedPocket.add(5);
		EXCLUDED_POCKETS.add(excludedPocket);

		excludedPocket = new HashSet<Integer>(2);
		excludedPocket.add(7);
		excludedPocket.add(4);
		EXCLUDED_POCKETS.add(excludedPocket);

		excludedPocket = new HashSet<Integer>(2);
		excludedPocket.add(6);
		excludedPocket.add(3);
		EXCLUDED_POCKETS.add(excludedPocket);

		excludedPocket = new HashSet<Integer>(2);
		excludedPocket.add(5);
		excludedPocket.add(2);
		EXCLUDED_POCKETS.add(excludedPocket);

		excludedPocket = new HashSet<Integer>(2);
		excludedPocket.add(6);
		excludedPocket.add(4);
		EXCLUDED_POCKETS.add(excludedPocket);

		excludedPocket = new HashSet<Integer>(2);
		excludedPocket.add(5);
		excludedPocket.add(3);
		EXCLUDED_POCKETS.add(excludedPocket);

		excludedPocket = new HashSet<Integer>(2);
		excludedPocket.add(4);
		excludedPocket.add(2);
		EXCLUDED_POCKETS.add(excludedPocket);

		excludedPocket = new HashSet<Integer>(2);
		excludedPocket.add(5);
		excludedPocket.add(4);
		EXCLUDED_POCKETS.add(excludedPocket);

		excludedPocket = new HashSet<Integer>(2);
		excludedPocket.add(4);
		excludedPocket.add(3);
		EXCLUDED_POCKETS.add(excludedPocket);

		excludedPocket = new HashSet<Integer>(2);
		excludedPocket.add(3);
		excludedPocket.add(2);
		EXCLUDED_POCKETS.add(excludedPocket);
	}

	public BoardTextureCalculator(Card ... cards) {
		this.cards = new HashSet<Card>();
		for (Card card : cards)
			this.cards.add(card);
	}

	public BoardTextureCalculator(Collection<Card> cards) {
		this.cards = cards;
	}

	public BoardTexture calculate() {
		if (isExtremelyCoordinated())
			return BoardTexture.EXTREMELY_COORDINATED;
		if (isCoordinated())
			return BoardTexture.COORDINATED;
		if (isSemiCoordinated())
			return BoardTexture.SEMICOORDINATED;
		return BoardTexture.DRY;
	}

	private boolean isExtremelyCoordinated() {
		int missingNForStraightExcludingExtreExceptions = missingNForStraightExcludingExtreExceptions();
		return Utils.suitedCards(cards)>=4 || missingNForStraightExcludingExtreExceptions <=1;

	}

	public int missingNForStraightExcludingExtreExceptions() {
		return missingNForStraight(BoardTexture.EXTREMELY_COORDINATED);
	}

	public int missingNForStraightExcludingCoorExceptions() {
		return missingNForStraight(BoardTexture.COORDINATED);
	}

	private int missingNForStraight(BoardTexture boardTexture) {
		Collection<Integer> numbers = getCardValuesInCollection();

		int missing = 4;
		for (Card current : cards) {
			int value = current.getRank().getValue();
			if (value<11)
				missing = Math.min(missing, calculateBetween(value, value+5, numbers, boardTexture));
			if (value>4)
				missing = Math.min(missing, calculateBetween(value-5,  value, numbers, boardTexture));
		}

		return missing;
	}

	private int calculateBetween(int min, int max, Collection<Integer> numbers, BoardTexture boardTexture) {
		int missing = 4;

		switch(boardTexture) {

		case COORDINATED:
			for (Set<Integer> currentPocket : EXCLUDED_POCKETS) {
				boolean found = false;
				for (Integer value : currentPocket) {
					if (value > min && value < max && !numbers.contains(value))
						if (found)
							return 4;
						else
							found = true;
				}
			}
			break;

		case EXTREMELY_COORDINATED:
			for (int value : EXCLUDED_CARD_VALUES) {
				if (value > min && value < max && !numbers.contains(value))
					return 4;
			}
			break;
		default:

		}

		for (int current : numbers)
			if (current > min && current < max)
				missing--;

		return missing;
	}

	public boolean isCoordinated() {
		return Utils.suitedCards(cards) == 3 || missingNForStraightExcludingCoorExceptions() == 2;
	}



	public boolean isSemiCoordinated() {
		if (cards.size() == 5)
			return false;
		int suitedCards = Utils.suitedCards(cards);
		int smallerGapNumberWithExceptions = smallerGapNumberWithExceptions();
		return (suitedCards == 1 && smallerGapNumberWithExceptions <=2) ||
				(suitedCards == 2 && smallerGapNumberWithExceptions <=3);
	}

	private int smallerGapNumberWithExceptions() {
		List<Integer> values = new ArrayList<Integer>(getCardValuesInCollection());
		values.removeAll(EXCLUDED_CARD_VALUES);
		int result = 25;
		if (values.size()<2)
			return 25;
		else
			for (int i=0; i<values.size()-1; i++)
				for (int j=i+1; j<values.size(); j++) {
					int getI = values.get(i);
					int getJ = values.get(j);
					int currentDistance = Math.abs(getI - getJ)-1;
					if (currentDistance < result)
						result = currentDistance;
				}

		return result;
	}

	private Set<Integer> getCardValuesInCollection() {
		Set<Integer> numbers = new HashSet<Integer>();
		for (Card current : cards) {
			int value = current.getRank().getValue();
			numbers.add(value);
			// If there's an ACE, we include both of its values (they can't interfere AFAIK)
			if (value == 1) {
				numbers.add(14);
			}
		}
		return numbers;
	}

}
