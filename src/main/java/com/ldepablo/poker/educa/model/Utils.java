package com.ldepablo.poker.educa.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class Utils {

	/**
	 * 
	 * @param cards
	 * @return max number of suited cards
	 */
	public static int suitedCards(Collection<Card> cards) {
		int clubs=0;
		int diamonds=0;
		int hearts=0;
		int spades=0;

		for (Card current : cards)
			switch (current.getSuit()) {
			case CLUBS:		clubs++;	break;
			case DIAMONDS:	diamonds++;	break;
			case HEARTS:	hearts++;	break;
			case SPADES:	spades++;	break;
			}
		return Math.max(Math.max(clubs, diamonds), Math.max(hearts, spades));
	}

	public static Collection<CardRank> getPairs(Collection<Card> cards) {
		Collection<CardRank> pairs = new HashSet<CardRank>();
		Collection<CardRank> found = new HashSet<CardRank>();
		
		for (Card card : cards) {
			CardRank number = card.getRank();
			if (!found.add(number))
				pairs.add(number);
		}
		return pairs;
	}

	
	/**
	 * This method should return how many gaps I have to fill to get a straight.
	 * 
	 * 2,3,4,6 returns 1
	 * 3,4,6 returns 2
	 * 3,4,7 returns 2
	 * 
	 * @param candidateRanks
	 * @return
	 * @deprecated Because it's also important to know where are those gaps placed. What would 345 return? and in KQJT, does an Ace have the same value as the 9?
	 * It would be so much better to have some method that directly returns number of outs
	 */
	@Deprecated
	public static int nStraightDraw(Collection<CardRank> candidateRanks) {
		//							 A      2      3      4      5      6      7      8      9      T      J      Q      K      A      
		boolean[] straightValues = { false, false, false, false, false, false, false, false, false, false, false, false, false, false };
										
		for (CardRank current : candidateRanks) {
			if (CardRank.ACE.equals(current))
				straightValues[13] = true;
			straightValues[current.getValue()-1] = true;
		}
			
		int lesserMissing = 5;
		for (int i=0; i<=9; i++) {
			int missing = 0;
			for (int j=i; j<i+5; j++){
				if (!straightValues[j])
					missing++;
			}
			if (missing<lesserMissing)
				lesserMissing = missing;
		}
		
		return lesserMissing;
			
//		HashSet<CardRank> availableRanks = new HashSet<CardRank>(candidateRanksAsCollection);
//		CardRank minRank = Collections.min(availableRanks, new CardRank.RankComparator());
//		boolean[] straightValues = { false, false, false };
//		
//		for (CardRank current : availableRanks) {
//			int distance = current.getValue() - minRank.getValue();
//			if (distance>0 && distance<4)
//				straightValues[distance-1] = true;
//		}
//		
//		boolean missingValueFound = false;
//		for (int i=0; i<3; i++) {
//			if (!straightValues[i])
//				if (missingValueFound)
//					return false;
//				else
//					missingValueFound = true;
//		}
//		return true;
	}

	public static Set<CardRank> getRanksSet(Collection<Card> cards) {
		Set<CardRank> result = new HashSet<CardRank>();
		for (Card card : cards)
			result.add(card.getRank());
		
		return result;
	}

	public static int[] getRanksQuantitiesInArray(Collection<Card> cards) {
		int[] ranks = new int[13];
		for(Card card : cards) {
			int index = card.getRank().getValue()-2;
			ranks[index]++;
		}
		return ranks;
	}

}
