package com.ldepablo.poker.educa.model;

import lombok.Getter;
import lombok.Setter;

public class PlayerAtTable {
	@Getter @Setter private int stack;
	@Getter @Setter private int bet;
	@Getter @Setter private Position position;


	public void substractBet(int bet) {
		stack -= bet;
	}

}
