package com.ldepablo.poker.educa.model;

import lombok.Getter;
import lombok.Setter;

public class Player {

	@Getter private final String name;
	@Getter @Setter private PlayerStats stats;
	
	
	public Player(String name) {
		this.name = name;
	}

}
