package com.ldepablo.poker.educa.model.completedhands;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lombok.Getter;

import com.ldepablo.poker.educa.model.Card;
import com.ldepablo.poker.educa.model.CardRank;
import com.ldepablo.poker.educa.model.GenericHandRank;
import com.ldepablo.poker.educa.model.Pocket;
import com.ldepablo.poker.educa.model.Utils;

public class CompletedStraight implements CompletedHand{

	@Getter private final Byte bestPocketPosition;
	@Getter private final Byte secondBestPocketPosition;
	@Getter private final CardRank highestRank;
	
	/**
	 * Indicates how many pocket cards complete the straight by the top.
	 */
	@Getter private final byte superiors;

	private CompletedStraight(CardRank highestRank, Byte bestPocketPosition, Byte secondBestPocketPosition, byte superiors) {
		this.bestPocketPosition = bestPocketPosition;
		this.secondBestPocketPosition = secondBestPocketPosition;
		this.highestRank = highestRank;
		this.superiors = superiors;
	}

	public static CompletedStraight build(Pocket pocket, Collection<Card> allCards) {

		Set<Integer> ranksOfSameSuitAsSet = new HashSet<Integer>();
		for (Card card : allCards) {
			CardRank rank = card.getRank();
			ranksOfSameSuitAsSet.add(rank.getValue());
			if (rank.equals(CardRank.ACE))
				ranksOfSameSuitAsSet.add(1);
		}
		
		ArrayList<Integer> rankValues = new ArrayList<Integer>(ranksOfSameSuitAsSet);
		Collections.sort(rankValues);

		int inARow = 0;
		int highestRank = rankValues.get(rankValues.size()-1);

		// By doing this backwards, I ensure that the first result I get is the best possible one.
		for (int i=rankValues.size()-2; i>=0; i--)
			if (Math.abs(rankValues.get(i) - rankValues.get(i+1)) == 1) {
				inARow++;
				if (inARow == 4) {

					List<Byte> pocketsPositions = selectPocketsPositions(highestRank, allCards, pocket);

					Byte bestPocketPosition = null;
					if (!pocketsPositions.isEmpty()) {
						bestPocketPosition = pocketsPositions.get(0);
					}

					Byte secondBestPocketPosition = null;
					if (pocketsPositions.size()>1) {
						secondBestPocketPosition = pocketsPositions.get(1);
					}
					
					CardRank rank = CardRank.fromValue(highestRank);
					byte superiors = calculateSuperior(allCards, pocket, rank);
					
					return new CompletedStraight(rank,bestPocketPosition, secondBestPocketPosition, superiors);
				}
			} else {
				highestRank = rankValues.get(i);
				inARow = 0;
			}
		// If we are here, we know there's no possible result because it's not possible to have 5 cards of more than one suit.
		return null;
	}

	private static List<Byte> selectPocketsPositions(int highestValue, Collection<Card> allCards, Pocket pocket) {
		Collection<Card> pocketCards = pocket.getCards();
		Collection<CardRank> pocketRanks = Utils.getRanksSet(pocketCards);
		
		Collection<Card> publicCards = new HashSet<Card>(allCards);
		publicCards.removeAll(pocketCards);
		Collection<CardRank> publicRanks = Utils.getRanksSet(publicCards);
		
		pocketRanks.removeAll(publicRanks);
		
		List<Byte> result = new ArrayList<Byte>(2);
		if (pocketRanks.isEmpty())
			return result;
		
		for (byte i=0; i<5; i++) {
			CardRank currentRank = CardRank.fromValue(highestValue-i);
			if (pocketRanks.contains(currentRank)) {
				result.add(i);
				if (result.size() == 2)
					break;
			}
		}
		return result;
	}

	private static byte calculateSuperior(Collection<Card> allCards, Pocket pocket, CardRank topRank) {
		HashSet<Card> commonCards = new HashSet<Card>();
		commonCards.addAll(allCards);
		commonCards.removeAll(pocket.getCards());
		Set<CardRank> commonRanks = Utils.getRanksSet(commonCards);
		if (commonRanks.contains(topRank))
			return 0;
		
		if (commonRanks.contains(CardRank.fromValue(topRank.getValue()-1)))
			return 1;
		
		return 2;
	}

	@Override
	public GenericHandRank getGenericHandRank() {
		return GenericHandRank.STRAIGHT;
	}


	@Override
	public int compareTo(CompletedHand o) {
		if (o instanceof CompletedStraight) {
			return highestRank.compareTo(((CompletedStraight) o).getHighestRank());
		} else
			return GenericHandRank.STRAIGHT.compareTo(o.getGenericHandRank());

	}

}
