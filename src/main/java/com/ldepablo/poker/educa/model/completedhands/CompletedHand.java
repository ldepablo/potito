package com.ldepablo.poker.educa.model.completedhands;

import com.ldepablo.poker.educa.model.GenericHandRank;


/**
 * It's a made hand, a completed draw.
 * 
 * @author ldepablo
 *
 */
public interface CompletedHand extends Comparable<CompletedHand> {

	public GenericHandRank getGenericHandRank();
	
	public Byte getBestPocketPosition();

	public Byte getSecondBestPocketPosition();

}
