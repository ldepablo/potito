package com.ldepablo.poker.educa.model.completedhands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import lombok.Getter;

import com.ldepablo.poker.educa.model.Card;
import com.ldepablo.poker.educa.model.CardRank;
import com.ldepablo.poker.educa.model.GenericHandRank;
import com.ldepablo.poker.educa.model.Pocket;
import com.ldepablo.poker.educa.model.Suit;

public class CompletedFlush implements CompletedHand{

	@Getter private final Suit suit;
	@Getter private final CardRank rank1;
	@Getter private final Byte bestPocketPosition;
	@Getter private final Byte secondBestPocketPosition;
	@Getter private final byte cardsThatMakeABetterFlush;
	@Getter private final CardRank[] ranks;

	private CompletedFlush(Suit suit, CardRank highestRank, Byte bestPocketPosition, Byte secondBestPocketPosition, byte cardsThatMakeBetterFlush, CardRank[] ranks) {
		this.suit = suit;
		this.rank1 = highestRank;
		this.bestPocketPosition = bestPocketPosition;
		this.secondBestPocketPosition = secondBestPocketPosition;
		this.cardsThatMakeABetterFlush = cardsThatMakeBetterFlush;
		this.ranks = ranks;
	}

	public static CompletedFlush build(Pocket pocket, Collection<Card> allCards) {
		byte[] suits = new byte[4];
		Suit suit = null;
		for (Card card : allCards) {
			int index = 0;
			switch(card.getSuit()) {
			case CLUBS: index = 0; break;
			case DIAMONDS: index = 1; break;
			case HEARTS: index = 2; break;
			case SPADES: index = 3; break;
			}
			if (++suits[index] == 5) {
				suit = card.getSuit();
			}
		}
		if (suit == null)
			return null;

		List<CardRank> ranksForSuit = getRanksForSuit(allCards, suit);
		Collections.sort(ranksForSuit);
		CardRank[] ranks = getRanksFromCollection(ranksForSuit);
		int ranksSize = ranksForSuit.size();
		CardRank highestRank = ranksForSuit.get(ranksSize-1);

		List<CardRank> pocketRanksForSuit = getRanksForSuit(pocket.getCards(), suit);
		Byte[] pocketPositions = getPocketPositions(suit, ranksForSuit, pocketRanksForSuit);
		Byte bestPocketPosition = pocketPositions[0];
		if (bestPocketPosition != null && bestPocketPosition > 4)
			bestPocketPosition = null; 
		Byte secondBestPocketPosition = pocketPositions[1]; 
		if (secondBestPocketPosition != null && secondBestPocketPosition > 4)
			secondBestPocketPosition = null;

		byte cardsThatMakeABetterFlush = calculateCardsThatMakeABetterFlush(ranksForSuit, pocketRanksForSuit);

		return new CompletedFlush(suit, highestRank, bestPocketPosition, secondBestPocketPosition, cardsThatMakeABetterFlush, ranks);

	}

	private static CardRank[] getRanksFromCollection(List<CardRank> ranksForSuit) {
		CardRank[] result = new CardRank[5];
		int size = ranksForSuit.size();
		for (int i=0; i<5; i++)
			result[i] = ranksForSuit.get(size-i-1);
		return result;
	}

	private static byte calculateCardsThatMakeABetterFlush(List<CardRank> allRanks, List<CardRank> pocketRanks) {
		byte result = 0;
		CardRank highestRankInPocketForSuit = getHighestRankInPocketForSuit(pocketRanks);
		List<CardRank> rankValues = Arrays.asList(CardRank.values());
		Collections.sort(rankValues);
		if (pocketRanks.isEmpty()) {
			return calculateCardsThatMakeABetterFlushWithNoPocketCards(allRanks);
		}
		for (int i=rankValues.size()-1; i>=0; i--) {
			CardRank current = rankValues.get(i);
			if (current.equals(highestRankInPocketForSuit))
				return result;

			if (!allRanks.contains(current))
				result++;

		}
		return result;
	}

	private static byte calculateCardsThatMakeABetterFlushWithNoPocketCards(List<CardRank> allRanks) {
		byte result = 8;
		Collections.sort(allRanks);
		int minValue = allRanks.get(0).getValue();
		for (int i=2; i<minValue; i++)
			result--;

		return result;
	}

	private static Byte[] getPocketPositions(Suit suit, List<CardRank> ranks, List<CardRank> pocketRanks) {
		Byte[] result = { null, null };
		Byte bestPocketPosition = null;
		if (!pocketRanks.isEmpty()) {
			int ranksSize = ranks.size();
			for (byte i=0; i<ranksSize; i++) {
				if (pocketRanks.contains(ranks.get(ranksSize-i-1))) {
					if (bestPocketPosition == null) {
						bestPocketPosition = i;

					} else {
						result[1] = i;
						break;
					}
				}
			}
			result[0] = bestPocketPosition;
		}
		return result;
	}

	private static CardRank getHighestRankInPocketForSuit(List<CardRank> pocketRanks) {
		if (pocketRanks.isEmpty())
			return null;

		if (pocketRanks.size() == 1)
			return pocketRanks.get(0);

		int index = CardRank.compare(pocketRanks.get(0), pocketRanks.get(1))>0 ? 0 : 1;
		return pocketRanks.get(index);
	}

	private static List<CardRank> getRanksForSuit(Collection<Card> cards, Suit suit) {
		List<CardRank> ranks = new ArrayList<CardRank>();
		for (Card card : cards) {
			if (suit.equals(card.getSuit()))
				ranks.add(card.getRank());
		}
		return ranks;
	}

	public GenericHandRank getGenericHandRank() {
		return GenericHandRank.FLUSH;
	}


	@Override
	public int compareTo(CompletedHand o) {
		if (o instanceof CompletedFlush) {
			CompletedFlush other = (CompletedFlush) o;
			int result = 0;
			int i = 0;
			do
				result = ranks[i].compareTo(other.ranks[i]);
			while (++i<5 && result == 0);
			return result;
		} else
			return getGenericHandRank().compareTo(o.getGenericHandRank());

	}


}
