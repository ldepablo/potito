package com.ldepablo.poker.educa.model;

import java.util.Collection;
import java.util.HashSet;

import lombok.Getter;

/**
 * Represents the two pocket cards
 * 
 * @author ldepablo
 *
 */
public class Pocket {
	@Getter private final Card card1;
	@Getter private final Card card2;
	@Getter private final boolean pair;
	@Getter private final boolean suited;
	@Getter private final PreFlopHand preFlopHand;
	private final Collection<Card> cards = new HashSet<Card>(2);
	
	public Pocket(Card card1, Card card2) {
		this.card1 = card1;
		this.card2 = card2;
		pair = card1.isPairWith(card2);
		suited = card1.isSuitedWith(card2);
		cards.add(card1);
		cards.add(card2);
		preFlopHand = new PreFlopHand(card1.getRank(), card2.getRank(), suited);
	}
	
	public Collection<Card> getCards(){
		return new HashSet<Card>(cards);
	}
	
	@Override
	public String toString() {
		return cards.toString();
	}

	public boolean isIn(HandList list) {
		return list.contains(preFlopHand);
	}
	 
}
