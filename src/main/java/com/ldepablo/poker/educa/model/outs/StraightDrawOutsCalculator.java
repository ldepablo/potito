package com.ldepablo.poker.educa.model.outs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.ldepablo.poker.educa.model.Board;
import com.ldepablo.poker.educa.model.Card;
import com.ldepablo.poker.educa.model.CardRank;
import com.ldepablo.poker.educa.model.Pocket;
import com.ldepablo.poker.educa.model.Utils;

/**
 * Calculates adjusted outs for straight draws 
 * 
 * @author ldepablo
 *
 */
public class StraightDrawOutsCalculator {


	private final Pocket pocket;
	private final Board board;
	
	public StraightDrawOutsCalculator(Pocket pocket, Board board){
		this.pocket = pocket;
		this.board = board;
	}



	/**
	 * Returns adjusted outs for s8 draw. Written outs are for OESDs. Gutshot or equivalent are half of OESD.
	 * 
	 * 2+2 no FD AND no paired board = 8
	 * 2+2 FD xOR paired board = 6
	 * 2+2 3 of a suit OR (FD AND paired board) = 4
	 * 2+2 2P OR 3 = 3
	 * 
	 * 1+3 no FD AND no paired board = 6
	 * 1+3 FD xOR paired board = 5
	 * 1+3 FD AND paired board = 3
	 //TODO Check out whether 3 of a suit rule applies in 2+2 applies for 1+3 too
	 * 
	 * 2+1 no FD AND no paired board AND 3 in a row = 1
	 * 
	 * @param pocket
	 * @param commonCards
	 * @return
	 */
	public float calculate() {
		/**
		 *  TODO This has to be done using bitwise operations and getting the best (first) result.
		 *  We have to avoid getting false positives in the sense of having a straight and getting a positive for a SD
		 *  caused by the first four items of the actual straight. If we have a straight, we don't have to look for any S8 draw.
		 *  
		 *  This is why this development is momentarily blocked here and will continue with 
		 *  the complete hands that we actually have. 
		 */
		/**
		 * If 2+2 possible, then return it
		 * else if 1+3 possible, the return it
		 * else if 2+1 possible, then return it
		 * return 0;
		 */

		Set<CardRank> ranks = new HashSet<CardRank>();
		for(Card card : pocket.getCards())
			ranks.add(card.getRank());

		for(Card card : board.getCards())
			ranks.add(card.getRank());


		int straightValues = 0;
		for (CardRank rank : ranks) {
			int value = rank.getValue();
			double res = Math.pow(2, value-1);
			straightValues += res;
		}

		Collection<Integer> patterns = new ArrayList<Integer>();
		patterns.add(0b1111);
		//		for (Integer pattern : patterns)
		//			boolean found
		return 0;
	}

	@Deprecated
	public double getOutsFor22S8Draw() {
		//FIXME This is where I should continue. Don't even know which path I wanted the code to follow...
		int distance = pocket.getCard1().distanceTo(pocket.getCard2());
		if (distance > 4)
			return 0;
		//		
		//		for (int i = 0; i<board.getCards().size()-1; i++)
		//			for (int j = i+1; j<board.getCards().size(); j++) {
		//				HashSet<CardRank> candidateRanksForStraightDraw = new HashSet<CardRank>(4);
		//				candidateRanksForStraightDraw.add(pocket.getCard1().getRank());
		//				candidateRanksForStraightDraw.add(pocket.getCard2().getRank());
		//				candidateRanksForStraightDraw.add(board.getCards().get(i).getRank());
		//				candidateRanksForStraightDraw.add(board.getCards().get(j).getRank());
		//				
		//				if (Utils.nStraightDraw(candidateRanksForStraightDraw) == 2)
		//					return 8;
		//				
		//			}
		//		
		//		
		//		Set<CardRank> candidateRanksForStraightDraw = new HashSet<CardRank>(4);
		//		candidateRanksForStraightDraw.add(pocket.getCard1().getRank());
		//		candidateRanksForStraightDraw.add(pocket.getCard2().getRank());
		//		for (Card card : board.getCards())
		//			candidateRanksForStraightDraw.add(card.getRank());

		//		int nStraightDraw = Utils.nStraightDraw(candidateRanksForStraightDraw);
		//		if (nStraightDraw > 2)
		//			return 0;
		// TODO Board.isDoublePaired
		if (board.isPaired())
			return 6;
		if (Utils.suitedCards(board.getCards()) >=2)
			return 6;
		return 8;	
	}
}
