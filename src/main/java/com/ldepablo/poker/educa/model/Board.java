package com.ldepablo.poker.educa.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lombok.Getter;

/**
 * Represents the common cards.
 * 
 * @author ldepablo
 *
 */
public class Board {

	@Getter private final Flop flop;
	@Getter private final Card turn;	
	@Getter private final Card river;
	@Getter private final BoardTexture texture;
	private final Collection<Card> cards = new HashSet<Card>();
	@Getter private boolean paired;
	@Getter private boolean doublePaired;
	@Getter private boolean threeOfAKind;
	@Getter private CardRank highestRank;
	@Getter private CardRank secondHighestRank;
	@Getter private Map<Suit, Collection<CardRank>> suitsMap = new HashMap<Suit, Collection<CardRank>>();
	@Getter private int suitedCards;
	@Getter private byte maxConnectedCards;

	//TODO To be tested. It may only be valid for tests
	public Board(Card... newCards) {
		flop = new Flop(newCards[0], newCards[1], newCards[2]);
		cards.addAll(flop.getCards());
		switch (newCards.length) {
		case 3:	
			turn = null;
			river = null;
			break;
		case 4: 
			turn = newCards[3];
			cards.add(turn);
			river = null;
			break;
		default:
			turn = newCards[3];
			cards.add(turn);
			river = newCards[4];
			cards.add(river);
		}
		texture = calculateTexture();
		findOutHighestAndSecondRank();
		initiliseDoublePairedAndThreeOfAKind();
		initialiseSuitsMap();
		calculateMaxConnectedCards();
	}

	public Board(Flop flop) {
		this.flop = flop;
		this.turn = null;
		this.river = null;
		cards.addAll(flop.getCards());
		texture = calculateTexture();
		findOutHighestAndSecondRank();
		initiliseDoublePairedAndThreeOfAKind();
		initialiseSuitsMap();
		calculateMaxConnectedCards();
	}

	public Board(Flop flop, Card turn) {
		this.flop = flop;
		this.turn = turn;
		this.river = null;
		cards.addAll(flop.getCards());
		cards.add(turn);
		texture = calculateTexture();
		findOutHighestAndSecondRank();
		initiliseDoublePairedAndThreeOfAKind();
		initialiseSuitsMap();
		calculateMaxConnectedCards();
	}

	public Board(Flop flop, Card turn, Card river) {
		this.flop = flop;
		this.turn = turn;
		this.river = river;
		cards.addAll(flop.getCards());
		cards.add(turn);
		cards.add(river);
		texture = calculateTexture();
		findOutHighestAndSecondRank();
		initiliseDoublePairedAndThreeOfAKind();
		
		initialiseSuitsMap();
		calculateMaxConnectedCards();
	}

	private BoardTexture calculateTexture() {
		return new BoardTextureCalculator(cards).calculate();
	}
	
	public List<Card> getCards(){
		return new ArrayList<Card>(cards);
	}

	private void findOutHighestAndSecondRank() {
		List<Card> cardsInList = new ArrayList<Card>(cards);
		Collections.sort(cardsInList);
		highestRank = cardsInList.get(cardsInList.size()-1).getRank();
		secondHighestRank = cardsInList.get(cardsInList.size()-2).getRank();
	}

	public void initiliseDoublePairedAndThreeOfAKind() {
		Set<CardRank> foundOnce = new HashSet<CardRank>();
		Set<CardRank> foundTwice = new HashSet<CardRank>();
		Set<CardRank> foundThird = new HashSet<CardRank>();
		for(Card current : cards) {
			CardRank currentRank = current.getRank();
			if (!foundOnce.add(currentRank))
				if (!foundTwice.add(currentRank))
					foundThird.add(currentRank);
		}
		paired = !foundTwice.isEmpty();
		doublePaired = foundTwice.size()>1;
		threeOfAKind = !foundThird.isEmpty();
	}

	public void initialiseSuitsMap() {
		suitedCards = 0;
		for (Card card : cards) {
			Suit suit = card.getSuit();
			Collection<CardRank> ranks = suitsMap.get(suit);
			if (ranks == null)
				ranks = new HashSet<CardRank>();
			ranks.add(card.getRank());
			suitsMap.put(suit, ranks);
			suitedCards = Math.max(suitedCards, ranks.size());
		}
		
	}

	private void calculateMaxConnectedCards() {
		byte maxConnectedCardsFound = 0;
		Set<CardRank> ranks = Utils.getRanksSet(cards);
		for (CardRank rank : CardRank.values()) {
			if (ranks.contains(rank)) {
				maxConnectedCardsFound++;
				if (rank.equals(CardRank.TWO) && ranks.contains(CardRank.ACE) && ranks.contains(CardRank.TWO))
					maxConnectedCardsFound++;
				if (maxConnectedCardsFound > maxConnectedCards)
					maxConnectedCards = maxConnectedCardsFound;
			}
			else 
				maxConnectedCardsFound = 0;
			
		}
		
	}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("[ ").append(flop).append("] ");
		if (turn != null)
			buffer.append(turn);
		if (river != null)
			buffer.append(" ").append(river);
		
		return buffer.toString();
	}

}
