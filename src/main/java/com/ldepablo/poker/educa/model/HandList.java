package com.ldepablo.poker.educa.model;

import java.util.HashSet;
import java.util.Set;

public class HandList {

	private Set<PreFlopHand> set = new HashSet<PreFlopHand>();

	public void add(PreFlopHand hand) {
		set.add(hand);
	}

	/**
	 * This metod is only thought to be used once in Pocket.java, in a method called isIn or something like that.
	 * It makes more sense syntactically.
	 * 
	 * @param hand
	 * @return
	 */
	public boolean contains(PreFlopHand hand) {
		return set.contains(hand);
	}

}
