package com.ldepablo.poker.educa.model.outs;


public class OutsCalculator {
	private final OvercardsOutsCalculator overcardsOutsCalculator;
	private final FlushDrawOutsCalculator flushDrawOutsCalculator;
	private final StraightDrawOutsCalculator straightDrawOutsCalculator;
	
	public OutsCalculator(OvercardsOutsCalculator overcardsOutsCalculator, 
			FlushDrawOutsCalculator flushDrawOutsCalculator, 
			StraightDrawOutsCalculator straightDrawOutsCalculator) {
		
		this.overcardsOutsCalculator = overcardsOutsCalculator;
		this.flushDrawOutsCalculator = flushDrawOutsCalculator;
		this.straightDrawOutsCalculator = straightDrawOutsCalculator;
	}

	public double calculate() {
		return getOutsForOvercards() + getOutsForFlushDraw() + getOutsForS8Draw();
	}

	public double getOutsForOvercards() {
		return overcardsOutsCalculator.calculate();
	}

	private double getOutsForFlushDraw() {
		return flushDrawOutsCalculator.calculate();
	}
	public float getOutsForS8Draw() {
		return straightDrawOutsCalculator.calculate();	
	}

}
