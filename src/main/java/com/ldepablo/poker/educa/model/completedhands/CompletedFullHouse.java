package com.ldepablo.poker.educa.model.completedhands;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import lombok.Getter;

import com.ldepablo.poker.educa.model.Card;
import com.ldepablo.poker.educa.model.CardRank;
import com.ldepablo.poker.educa.model.GenericHandRank;
import com.ldepablo.poker.educa.model.Pocket;
import com.ldepablo.poker.educa.model.Utils;

public class CompletedFullHouse implements CompletedHand {

	@Getter private final CardRank firstRank;
	@Getter private final CardRank secondRank;
	@Getter private final Byte bestPocketPosition;
	@Getter private final Byte secondBestPocketPosition;
	@Getter private final boolean atLeastBestFullHouse;

	private CompletedFullHouse(CardRank firstRank, CardRank secondRank, Byte bestPocketPosition, Byte secondBestPocketPosition, boolean atLeastBestFullHouse) {
		this.firstRank = firstRank;
		this.secondRank = secondRank;
		this.bestPocketPosition = bestPocketPosition;
		this.secondBestPocketPosition = secondBestPocketPosition;
		this.atLeastBestFullHouse = atLeastBestFullHouse;
	}

	@Override
	public GenericHandRank getGenericHandRank() {
		return GenericHandRank.FULL_HOUSE;
	}

	public static CompletedFullHouse build(Pocket pocket, Collection<Card> allCards) {

		int[] ranks = Utils.getRanksQuantitiesInArray(allCards);
		Set<CardRank> pocketRanks = Utils.getRanksSet(pocket.getCards());

		CardRank highestThreeOfAKindRank = null;
		CardRank highestPairRank = null;
		boolean atLeastBestFullHouse = true;
		int i=13;
		while (--i>=0 && (highestThreeOfAKindRank == null || highestPairRank == null)) {

			if (ranks[i] == 3) {
				if (highestThreeOfAKindRank == null)
					highestThreeOfAKindRank = CardRank.fromValue(i+2);
				else
					highestPairRank = CardRank.fromValue(i+2);
			}

			if (ranks[i] == 2 && highestPairRank == null) 
				highestPairRank = CardRank.fromValue(i+2);

			if (ranks[i] == 1 && (highestThreeOfAKindRank == null || highestPairRank == null)) {
				boolean isPocketRank = pocketRanks.contains(CardRank.fromValue(i+2));
				if (!isPocketRank)
					atLeastBestFullHouse = false;
			}

		}

		if (i == -1)
			return null;
		if (CardRank.compare(highestThreeOfAKindRank, highestPairRank) < 0)
			atLeastBestFullHouse = false;

		Byte bestPocketPosition = null;
		Byte secondBestPocketPosition = null;
		CardRank pocketRank1 = pocket.getCard1().getRank();
		CardRank pocketRank2 = pocket.getCard2().getRank();

		if (pocketRank1.equals(highestThreeOfAKindRank) || pocketRank2.equals(highestThreeOfAKindRank)) {
			bestPocketPosition = 0;
			if (pocketRank1.equals(highestThreeOfAKindRank) && pocketRank2.equals(highestThreeOfAKindRank))
				secondBestPocketPosition = 0;
		}

		Collection<Card> publicCards = new HashSet<Card>(allCards);
		publicCards.removeAll(pocket.getCards());
		int[] publicRanks = Utils.getRanksQuantitiesInArray(publicCards);
		int publicCardsOfPairRank = publicRanks[highestPairRank.getValue()-2];

		//If there are two common cards of the rank of the pair, we don't have any valuable card for that rank, so we would be done.
		// if (publicCardsOfPairRank == 2)
		//		return new CompletedFullHouse(highestThreeOfAKindRank, highestPairRank, bestPocketPosition, secondBestPocketPosition);

		if (publicCardsOfPairRank == 1)
			//If there's one common card of the rank of the pair, we have the other one.
			if (bestPocketPosition == null)
				bestPocketPosition = 3;
			else
				secondBestPocketPosition = 3;

		else if (publicCardsOfPairRank == 0) {
			//If there are no common cards of the rank of the pair, we have both of them.
			bestPocketPosition = 3;
			secondBestPocketPosition = 3;
		}


		return new CompletedFullHouse(highestThreeOfAKindRank, highestPairRank, bestPocketPosition, secondBestPocketPosition, atLeastBestFullHouse);
	}

	@Override
	public int compareTo(CompletedHand o) {
		if (o instanceof CompletedFullHouse) {
			CompletedFullHouse other = (CompletedFullHouse) o;
			if (getFirstRank().equals(other.getFirstRank())) 
				return getSecondRank().compareTo(other.getSecondRank());
			return getFirstRank().compareTo(other.getFirstRank());
		} else
			return getGenericHandRank().compareTo(o.getGenericHandRank());
	}
}
