package com.ldepablo.poker.educa.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import lombok.Getter;

/**
 * A card
 * 
 * @author ldepablo
 *
 */
public class Card implements Comparable<Card>{	

	@Getter private final Suit suit;
	@Getter private final CardRank rank;
	
	public Card(CardRank rank, Suit suit) {
		if (rank == null)
			throw new IllegalArgumentException("Card can't be created with null number");
		
		if (suit == null)
			throw new IllegalArgumentException("Card can't be created with null suit");
		
		
		this.rank = rank;
		this.suit = suit;
	}

	/**
	 * 
	 * @param card
	 * @return 2.distanceTo(2) = 0; 2.distanceTo(4) = 2; 4.distanceTo(2) = 2
	 */
	public int distanceTo(Card card) {
		return rank.distanceTo(card.getRank());
	}
	
	@Override
	public boolean equals(Object other) {
		return EqualsBuilder.reflectionEquals(this, other);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	
	@Override
	public String toString() {
		return rank.toString() + suit.toString(); 
	}

	public boolean isPairWith(Card card) {
		return rank.equals(card.rank);
	}

	public boolean isSuitedWith(Card card) {
		return suit.equals(card.suit);
	}
	
	public static Card build(String cards) {
		Suit suit = null;
		CardRank number = null;
		
		switch(cards.charAt(0)) {
		case 'A': number = CardRank.ACE; break;
		case '2': number = CardRank.TWO; break;
		case '3': number = CardRank.THREE; break;
		case '4': number = CardRank.FOUR; break;
		case '5': number = CardRank.FIVE; break;
		case '6': number = CardRank.SIX; break;
		case '7': number = CardRank.SEVEN; break;
		case '8': number = CardRank.EIGHT; break;
		case '9': number = CardRank.NINE; break;
		case 'T': number = CardRank.TEN; break;
		case 'J': number = CardRank.JACK; break;
		case 'Q': number = CardRank.QUEEN; break;
		case 'K': number = CardRank.KING; break;
		}

		switch(cards.charAt(1)) {
		case 'c': suit = Suit.CLUBS; break;
		case 'd': suit = Suit.DIAMONDS; break;
		case 'h': suit = Suit.HEARTS; break;
		case 's': suit = Suit.SPADES; break;
		}
		
		return new Card(number, suit);
	}

	public int compareTo(Card o) {
		return CardRank.compare(this.getRank(), o.getRank());
	}

	public int compareTo(CardRank otherRank) {
		return CardRank.compare(rank, otherRank);
	}
	
}
