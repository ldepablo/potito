package com.ldepablo.poker.educa.model.completedhands;

import java.util.Collection;
import java.util.Set;

import lombok.Getter;

import com.ldepablo.poker.educa.model.Card;
import com.ldepablo.poker.educa.model.CardRank;
import com.ldepablo.poker.educa.model.GenericHandRank;
import com.ldepablo.poker.educa.model.Pocket;
import com.ldepablo.poker.educa.model.Utils;

public class CompletedHighCard implements CompletedHand {
	
	@Getter private final CardRank rank1;
	@Getter private final CardRank rank2;
	@Getter private final CardRank rank3;
	@Getter private final CardRank rank4;
	@Getter private final CardRank rank5;
	@Getter private final Byte bestPocketPosition;
	@Getter private final Byte secondBestPocketPosition;
	
	private CompletedHighCard(CardRank rank1, CardRank rank2, CardRank rank3, CardRank rank4, CardRank rank5, Byte bestPocket, Byte secondBestPocket) {
		this.rank1 = rank1;
		this.rank2 = rank2;
		this.rank3 = rank3;
		this.rank4 = rank4;
		this.rank5 = rank5;
		this.bestPocketPosition = bestPocket;
		this.secondBestPocketPosition = secondBestPocket;
	}

	public static CompletedHighCard build(Pocket pocket, Collection<Card> cards) {
		int[] ranksValues = Utils.getRanksQuantitiesInArray(cards);
		Set<CardRank> pocketRanks = Utils.getRanksSet(pocket.getCards());
		
		CardRank rank1 = null;
		CardRank rank2 = null;
		CardRank rank3 = null;
		CardRank rank4 = null;
		CardRank rank5 = null;
		Byte bestPocket = null;
		Byte secondBestPocket = null;
		int i=12;
		do {
			if (ranksValues[i] == 1) {
				if (rank1 == null) {
					rank1 = CardRank.fromValue(i+2);
					if (pocketRanks.contains(rank1)) {
						if (bestPocket == null)
							bestPocket = 0;
						else
							secondBestPocket = 0;
					}
				} else if (rank2 == null) {
					rank2 = CardRank.fromValue(i+2);
					if (pocketRanks.contains(rank2)) {
						if (bestPocket == null)
							bestPocket = 1;
						else
							secondBestPocket = 1;
					}
				} else if (rank3 == null) {
					rank3 = CardRank.fromValue(i+2);
					if (pocketRanks.contains(rank3)) {
						if (bestPocket == null)
							bestPocket = 2;
						else
							secondBestPocket = 2;
					}
				} else if (rank4 == null) {
					rank4 = CardRank.fromValue(i+2);
					if (pocketRanks.contains(rank4)) {
						if (bestPocket == null)
							bestPocket = 3;
						else
							secondBestPocket = 3;
					}
				} else if (rank5 == null) {
					rank5 = CardRank.fromValue(i+2);
					if (pocketRanks.contains(rank5)) {
						if (bestPocket == null)
							bestPocket = 4;
						else
							secondBestPocket = 4;
					}	
				}
			}
			
		} while (--i>0 && rank5 == null);
		
		if (i<0 && rank5 == null)
			throw new RuntimeException("i<0 and not found fifth rank");
		
		return new CompletedHighCard(rank1, rank2, rank3, rank4, rank5, bestPocket, secondBestPocket);
	}

	@Override
	public GenericHandRank getGenericHandRank() {
		return GenericHandRank.HIGH_CARD;
	}

	@Override
	public int compareTo(CompletedHand o) {
		if (o instanceof CompletedHighCard) {
			CompletedHighCard other= (CompletedHighCard) o;
			int result = rank1.compareTo(other.rank1);
			if (result == 0) {
				result = rank2.compareTo(other.rank2);
				if (result == 0) {
					result = rank3.compareTo(other.rank3);
					if (result == 0) {
						result = rank4.compareTo(other.rank4);
						if (result == 0)
							result = rank5.compareTo(other.rank5);
					}
				}
			}
			return result;
		}
		return GenericHandRank.HIGH_CARD.compareTo(o.getGenericHandRank());
	}

	

}
