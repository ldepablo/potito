package com.ldepablo.poker.educa.model;

public enum Position {
	BUTTON,
	CUT_OFF,
	MIDDLE_POSITION,
	EARLY_POSITION,
	BIG_BLIND,
	SMALL_BLIND;
}
