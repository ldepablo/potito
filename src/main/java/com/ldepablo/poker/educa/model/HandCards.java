package com.ldepablo.poker.educa.model;

import java.util.Collection;
import java.util.HashSet;

import lombok.Getter;

import com.ldepablo.poker.educa.model.completedhands.CompletedFlush;
import com.ldepablo.poker.educa.model.completedhands.CompletedFourOfAKind;
import com.ldepablo.poker.educa.model.completedhands.CompletedFullHouse;
import com.ldepablo.poker.educa.model.completedhands.CompletedHand;
import com.ldepablo.poker.educa.model.completedhands.CompletedHighCard;
import com.ldepablo.poker.educa.model.completedhands.CompletedPair;
import com.ldepablo.poker.educa.model.completedhands.CompletedStraight;
import com.ldepablo.poker.educa.model.completedhands.CompletedStraightFlush;
import com.ldepablo.poker.educa.model.completedhands.CompletedThreeOfAKind;
import com.ldepablo.poker.educa.model.completedhands.CompletedTwoPair;

/**
 * Represents all the hands involved in the hand
 * 
 * @author ldepablo
 *
 */
public class HandCards {

	@Getter private final Pocket pocket;
	private final Board board;
	private final Collection<Card> allCards = new HashSet<Card>();
	@Getter private CompletedHand completedHand;
	@Getter HandStrength strength;

	public HandCards(Pocket pocket, Board board) {
		this.pocket = pocket;
		allCards.addAll(pocket.getCards());

		if (board == null)
			this.board = null;
		else {
			this.board = board;
			allCards.addAll(board.getCards());
		}

		if (board != null) {
			setBestPokerHand();
			calculateStrength(board, completedHand);
		}
	}

	public Collection<Card> getCards(){
		return new HashSet<Card>(allCards);
	}

	public Collection<Card> getPocketCards() {
		return pocket.getCards();
	}

	public Collection<Card> getPublicCards(){
		return board.getCards();
	}

	public void setBestPokerHand() {
		
		completedHand = CompletedStraightFlush.build(pocket, allCards);
		if (completedHand != null)
			return;
		
		completedHand = CompletedFourOfAKind.build(pocket, allCards);
		if (completedHand != null)
			return;
		
		completedHand = CompletedFullHouse.build(pocket, allCards);
		if (completedHand != null)
			return;
		
		completedHand = CompletedFlush.build(pocket, allCards);
		if (completedHand != null)
			return;
		
		completedHand = CompletedStraight.build(pocket, allCards);
		if (completedHand != null)
			return;
		
		completedHand = CompletedThreeOfAKind.build(pocket, allCards);
		if (completedHand != null)
			return;
		
		completedHand = CompletedTwoPair.build(pocket, allCards);
		if (completedHand != null)
			return;
		
		completedHand = CompletedPair.build(pocket, allCards);
		if (completedHand != null)
			return;
		
		completedHand = CompletedHighCard.build(pocket, allCards);
	}
	
	public void calculateStrength(Board board, CompletedHand hand) {
		strength = new StrengthCalculator().calculate(board, hand);
	}
}
