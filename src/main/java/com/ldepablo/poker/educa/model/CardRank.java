package com.ldepablo.poker.educa.model;

import java.util.Comparator;

import lombok.Getter;

/**
 * The rank of a card.
 * 
 * ACE will be the highest value because the only case 
 * in which Ace value can be 1 is a straight and, in that case too, Ace value must be top too.
 * @author ldepablo
 *
 */
public enum CardRank {
	
	

	TWO(2, "2"),
	THREE(3, "3"),
	FOUR(4, "4"),
	FIVE(5, "5"),
	SIX(6, "6"),
	SEVEN(7, "7"),
	EIGHT(8, "8"),
	NINE(9, "9"),
	TEN(10, "T"),
	JACK(11, "J"),
	QUEEN(12, "Q"),
	KING(13, "K"),
	ACE(14, "A");

	private static RankComparator comparator = new RankComparator();
	
	@Getter private int value;
	@Getter private String myChar;
	
	private CardRank(int value, String myChar) {
		this.value = value;
		this.myChar = myChar;
	}

	/**
	 * assertEquals(1, CardRank.ACE.distanceTo(CardRank.KING));
	 * 
	 * @param other
	 * @return
	 */
	public int distanceTo(CardRank other) {
		if (!this.equals(ACE) && !other.equals(ACE))
			return Math.abs(value - other.value);
		
		if (this.equals(ACE)) 
			return Math.min(other.value - 1, 14 - other.value);
		
		return Math.min(value - 1, 14 - value);
	}
	
	@Override
	public String toString() {
		return myChar;
	}
	
	public static int compare(CardRank r1, CardRank r2) {
		return comparator.compare(r1, r2);
	}
	
	public static class RankComparator implements Comparator<CardRank>
	{
	    public int compare(CardRank o1, CardRank o2)
	    {
			if (o1.equals(o2))
				return 0;
			if (o1.equals(CardRank.ACE))
				return 1;
			if (o2.equals(CardRank.ACE))
				return -1;
			if (o1.getValue()>o2.getValue())
				return 1;
			return -1;
	    }
	}

	public static CardRank fromValue(int value) {
		for (CardRank rank : values())
			if (rank.getValue() == value)
				return rank;
		if (value == 1)
			return CardRank.ACE;
		return null;
	}
}
