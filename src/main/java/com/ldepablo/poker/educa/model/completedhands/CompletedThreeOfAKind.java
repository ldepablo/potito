package com.ldepablo.poker.educa.model.completedhands;

import java.util.Collection;

import lombok.Getter;

import com.ldepablo.poker.educa.model.Card;
import com.ldepablo.poker.educa.model.CardRank;
import com.ldepablo.poker.educa.model.GenericHandRank;
import com.ldepablo.poker.educa.model.Pocket;
import com.ldepablo.poker.educa.model.Utils;

public class CompletedThreeOfAKind implements CompletedHand{

	@Getter private CardRank rank;
	@Getter private Byte bestPocketPosition;
	@Getter private Byte secondBestPocketPosition;
	@Getter private CardRank kicker1;
	@Getter private CardRank kicker2;


	private CompletedThreeOfAKind(CardRank rank, Byte bestPocketPosition, Byte secondBestPocketPosition, CardRank kicker1, CardRank kicker2) {
		this.rank = rank;
		this.bestPocketPosition = bestPocketPosition;
		this.secondBestPocketPosition = secondBestPocketPosition;
		this.kicker1 = kicker1;
		this.kicker2 = kicker2;
	}


	public static CompletedThreeOfAKind build(Pocket pocket, Collection<Card> allCards) {

		int[] ranks = Utils.getRanksQuantitiesInArray(allCards);
		CardRank rank = null;
		int i = 12;
		Byte bestPocketPosition = null;
		Byte secondBestPocketPosition = null;
		Byte cardsOnBoardBetterThanPocket = 0;
		CardRank kicker1 = null;
		CardRank kicker2 = null;

		do{
			switch (ranks[i]) {
			case 3:
				rank = CardRank.fromValue(i+2);
				if (pocket.getCard1().getRank().equals(rank))
					bestPocketPosition = 0;
				if (pocket.getCard2().getRank().equals(rank)) {
					if (bestPocketPosition == null)
						bestPocketPosition = 0;
					else 
						secondBestPocketPosition = 0;
				}
				break;
				
			case 1:
				if (kicker1 == null)
					kicker1 = CardRank.fromValue(i+2);
				else if (kicker2 == null)
					kicker2 = CardRank.fromValue(i+2);
				
			}
		} while ((rank == null || kicker1==null || kicker2==null) && --i>=0);

		i = 12;
		do{
			if (ranks[i] ==1) {
				CardRank currentRank = CardRank.fromValue(i+2);
				if (Utils.getRanksSet(pocket.getCards()).contains(currentRank)){
					if (bestPocketPosition == null)
						//FIXME painfull
						bestPocketPosition = (byte) (cardsOnBoardBetterThanPocket+3);
					else if (secondBestPocketPosition == null)
						//FIXME painfull
						secondBestPocketPosition = (byte) (cardsOnBoardBetterThanPocket+3);
				}
				cardsOnBoardBetterThanPocket ++;
			}
			i--;
		} while ((rank == null || bestPocketPosition==null || secondBestPocketPosition==null) && i>=0 && cardsOnBoardBetterThanPocket<2);
		if (rank == null)
			return null;

		return new CompletedThreeOfAKind(rank, bestPocketPosition, secondBestPocketPosition, kicker1, kicker2);
	}


	@Override
	public GenericHandRank getGenericHandRank() {
		return GenericHandRank.THREE_OF_A_KIND;
	}



	@Override
	public int compareTo(CompletedHand o) {
		if (o instanceof CompletedThreeOfAKind) {
			CompletedThreeOfAKind other = (CompletedThreeOfAKind) o;
			int result = rank.compareTo(other.getRank());
			if (result != 0)
				return result;
			
			result = kicker1.compareTo(other.getKicker1());
			if (result != 0)
				return result;

			return kicker2.compareTo(other.getKicker2());

		} else
			return GenericHandRank.THREE_OF_A_KIND.compareTo(o.getGenericHandRank());

	}

}
