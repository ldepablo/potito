package com.ldepablo.poker.educa.model.completedhands;

import java.util.Collection;
import java.util.HashSet;

import lombok.Getter;

import com.ldepablo.poker.educa.model.Card;
import com.ldepablo.poker.educa.model.CardRank;
import com.ldepablo.poker.educa.model.GenericHandRank;
import com.ldepablo.poker.educa.model.Pocket;
import com.ldepablo.poker.educa.model.Utils;

public class CompletedFourOfAKind implements CompletedHand {

	@Getter private final Byte bestPocketPosition;
	@Getter private final Byte secondBestPocketPosition;
	@Getter private final CardRank rank;
	@Getter private final CardRank fifthCardRank;
	
	
	private CompletedFourOfAKind(CardRank rank, CardRank fifthCardRank, Byte bestPocketPosition, Byte secondBestPocketPosition) {
		this.rank = rank;
		this.fifthCardRank = fifthCardRank;
		this.bestPocketPosition = bestPocketPosition;
		this.secondBestPocketPosition = secondBestPocketPosition;
	}

	@Override
	public GenericHandRank getGenericHandRank() {
		return GenericHandRank.FOUR_OF_A_KIND;
	}

	public static CompletedFourOfAKind build(Pocket pocket, Collection<Card> allCards) {
		
		int[] ranks = new int[13];
		for(Card card : allCards) {
			int index = card.getRank().getValue()-2;
			ranks[index]++;
		}
		
		for (int i=12; i>=0; i--) {
			if (ranks[i] == 4) {
				CardRank rank = CardRank.fromValue(i+2);
				CardRank highestRank = CardRank.TWO;
				for(Card card : allCards) {
					CardRank currentRank = card.getRank();
					if (!currentRank.equals(rank) && CardRank.compare(currentRank, highestRank)>0) {
						highestRank = currentRank;
					}
				}

				Byte bestPocketPosition = null;
				Byte secondBestPocketPosition = null;
				CardRank pocketRank1 = pocket.getCard1().getRank();
				CardRank pocketRank2 = pocket.getCard2().getRank();
				
				if (pocketRank1.equals(rank) || pocketRank2.equals(rank)) {
					bestPocketPosition = 0;
					if (pocketRank1.equals(rank) && pocketRank2.equals(rank))
						secondBestPocketPosition = 0;
				}
				
				Collection<Card> publicCards = new HashSet<Card>(allCards);
				publicCards.removeAll(pocket.getCards());
				if (!Utils.getRanksSet(publicCards).contains(highestRank) && (pocketRank1.equals(highestRank) || pocketRank2.equals(highestRank))) {
					if (bestPocketPosition == null)
						bestPocketPosition = 4;
					else
						secondBestPocketPosition = 4;
				}
				
				return new CompletedFourOfAKind(rank, highestRank, bestPocketPosition, secondBestPocketPosition);
			}
		}
		return null;
	}

	@Override
	public int compareTo(CompletedHand o) {
		if (o instanceof CompletedFourOfAKind) {
			CompletedFourOfAKind other = (CompletedFourOfAKind) o;
			if (getRank().equals(other.getRank())) 
				return getFifthCardRank().compareTo(other.getFifthCardRank());
			return getRank().compareTo(other.getRank());
		} else
			return getGenericHandRank().compareTo(o.getGenericHandRank());
	}

}
