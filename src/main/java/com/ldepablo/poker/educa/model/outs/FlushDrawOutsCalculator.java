package com.ldepablo.poker.educa.model.outs;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.ldepablo.poker.educa.model.Board;
import com.ldepablo.poker.educa.model.Card;
import com.ldepablo.poker.educa.model.CardRank;
import com.ldepablo.poker.educa.model.Pocket;
import com.ldepablo.poker.educa.model.Suit;

/**
 * Calculates outs for flush draws
 * @author ldepablo
 *
 */
public class FlushDrawOutsCalculator {


	private final Pocket pocket;
	private final Board board;
	
	public FlushDrawOutsCalculator(Pocket pocket, Board board){
		this.pocket = pocket;
		this.board = board;
	}

	public double calculate() {
		if (!pocket.isSuited()) {
			Map<Suit, Collection<CardRank>> suitsMap = board.getSuitsMap();
			for (Map.Entry<Suit, Collection<CardRank>> current : suitsMap.entrySet()) 
				if (current.getValue().size() == 2) {
					Suit suit = current.getKey();
					for (Card card : pocket.getCards())
						if (card.getSuit().equals(suit))
							return getOutsForFD12(current.getValue(), card.getRank());
					// FIXME podriamos tener un corazon y una espada, y llegados al turn dos corazones y dos espadas en board, podria evaluer solo la primera y no la segunda? COMPROBAR
				} else if (current.getValue().size() == 3) {
					Suit suit = current.getKey();
					for (Card currentPocketCard : pocket.getCards())
						if (currentPocketCard.getSuit().equals(suit)) {
							int missingOvercards = missingOvercards(currentPocketCard.getRank(), current.getValue());
							int potentialOuts = board.isPaired() ? 7 : 9;
							return potentialOuts - missingOvercards;
						}
				}
		} else {

			Suit suit = pocket.getCard1().getSuit();
			int suitedCards = 0;
			for (Card current : board.getCards())
				if (suit.equals(current.getSuit()))
					suitedCards++;
			if (suitedCards == 1) {
				// 2 + 1 FD
				if (!board.isPaired() && is2p1NutsFD(suit))
					return 2;
				return 1;
			}
			if (suitedCards == 2)
				// 2 + 2 FD
				if (board.isPaired())
					if (board.isDoublePaired() || board.isThreeOfAKind())
						return 4;
					else
						return 7;
				else
					return 9;
		}
		return 0;
	}

	private double getOutsForFD12(Collection<CardRank> boardRanks, CardRank pocket) {
		if (CardRank.ACE.equals(pocket))
			return 1;

		Set<CardRank> allRanks = new HashSet<CardRank>(Arrays.asList(CardRank.values()));
		allRanks.removeAll(boardRanks);
		CardRank maxRankToMatch = Collections.max(allRanks);
		if (CardRank.compare(pocket, maxRankToMatch) >= 0)
			return 1;

		return 0;

	}

	// 
	/**
	 * 
	 * @param pocketCardRank
	 * @param value
	 * @return given A, [ z, y, z ] = 0, given K, [ A, x, z ] = 0, given Q [A, 5, 2] = 1, given Q, [ J, 4, 7 ] = 2
	 */
	private int missingOvercards(CardRank pocketCardRank, Collection<CardRank> value) {
		int result = 0;
		for (CardRank currentRank : CardRank.values()) 
			if (CardRank.compare(currentRank, pocketCardRank)>0 && !value.contains(currentRank))
				result ++;

		return Math.max(result, 0);
	}

	/**
	 * will return true if:
	 * - Ace is in pocket OR
	 * - Ace is in board & King in pocket
	 * @param cards
	 * @param cards2
	 * @param suit
	 * @return
	 */
	private boolean is2p1NutsFD(Suit suit) {
		Card suitedAce = new Card(CardRank.ACE, suit);

		Collection<Card> pocketCards = pocket.getCards();
		if (pocketCards.contains(suitedAce))
			return true;

		Card suitedKing = new Card(CardRank.KING, suit);
		Collection<Card> allCards = new HashSet<Card>();
		allCards.addAll(pocketCards);
		allCards.addAll(board.getCards());
		return allCards.contains(suitedKing) && allCards.contains(suitedAce);
	}
}
