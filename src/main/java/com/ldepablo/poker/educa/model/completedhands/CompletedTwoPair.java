package com.ldepablo.poker.educa.model.completedhands;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import lombok.Getter;

import com.ldepablo.poker.educa.model.Card;
import com.ldepablo.poker.educa.model.CardRank;
import com.ldepablo.poker.educa.model.GenericHandRank;
import com.ldepablo.poker.educa.model.Pocket;
import com.ldepablo.poker.educa.model.Utils;

public class CompletedTwoPair implements CompletedHand {

	@Getter private final CardRank topRank;
	@Getter private final CardRank secondRank;
	@Getter private final CardRank kickerRank;
	@Getter private final Byte bestPocketPosition;
	@Getter private final Byte secondBestPocketPosition;
	@Getter private final boolean atLeastBestTwoPair;
	

	private CompletedTwoPair(CardRank topRank, CardRank secondRank, CardRank kicker, Byte bestPocketPosition, Byte secondBestPocketPosition, boolean atLeastBestTwoPair) {
		this.topRank = topRank;
		this.secondRank = secondRank;
		this.kickerRank = kicker;
		this.bestPocketPosition = bestPocketPosition;
		this.secondBestPocketPosition = secondBestPocketPosition;
		this.atLeastBestTwoPair = atLeastBestTwoPair;
	}

	public static CompletedTwoPair build(Pocket pocket, Collection<Card> allCards) {
		Byte bestPocketPosition = null;
		Byte secondBestPocketPosition = null;
		Set<CardRank> pocketRanks = Utils.getRanksSet(pocket.getCards());
		int[] ranks = Utils.getRanksQuantitiesInArray(allCards);
		int i=12;
		CardRank topPairRank = null;
		CardRank secondPairRank = null;
		CardRank kicker = null;
		do{
			if (ranks[i] == 2) {
				if (topPairRank == null) {
					topPairRank = CardRank.fromValue(i+2);
					if (pocketRanks.contains(topPairRank)) {
						bestPocketPosition = 0;
						if (pocket.isPair()) {
							secondBestPocketPosition = 0;
						}
					}
				} else if (secondPairRank == null) {
					secondPairRank = CardRank.fromValue(i+2);
					if (pocketRanks.contains(secondPairRank)) {
						if (bestPocketPosition == null) {
							bestPocketPosition = 2;
							if (pocket.isPair())
								secondBestPocketPosition = 2;

						} else if (secondBestPocketPosition == null)
							secondBestPocketPosition = 2;

					}
				}
			}
			
			if (ranks[i] == 1) {
				if (kicker == null)
					kicker = CardRank.fromValue(i+2);
			}

		}while (--i>=0 && (secondPairRank == null || topPairRank == null || kicker == null));
		if (i<0)
			return null;

		if (pocketRanks.contains(kicker))
			if (bestPocketPosition == null)
				bestPocketPosition = 4;
			else if (secondBestPocketPosition == null)
				secondBestPocketPosition = 4;
		
		boolean atLeastBestToPair = calculateIsAtLeastBestTopPair(pocket, allCards, topPairRank, secondPairRank);
		
		return new CompletedTwoPair(topPairRank, secondPairRank, kicker, bestPocketPosition, secondBestPocketPosition, atLeastBestToPair); 
	}

	private static boolean calculateIsAtLeastBestTopPair(Pocket pocket, Collection<Card> allCards, CardRank topPairRank, CardRank secondPairRank) {
		Collection<Card> commonCards = new HashSet<Card>(allCards);
		commonCards.removeAll(pocket.getCards());
		Collection<CardRank> commonRanks = Utils.getRanksSet(commonCards);
		commonRanks.remove(topPairRank);
		commonRanks.remove(secondPairRank);
		
		for (CardRank currentRank : commonRanks) {
			if (CardRank.compare(currentRank, secondPairRank) > 0)
				return false;
		}
		return true;
	}

	@Override
	public GenericHandRank getGenericHandRank() {
		return GenericHandRank.TWO_PAIR;
	}


	@Override
	public int compareTo(CompletedHand o) {
		if (o instanceof CompletedTwoPair) {
			CompletedTwoPair other = (CompletedTwoPair) o;
			int result = topRank.compareTo(other.topRank);
			if (result == 0) {
				result = secondRank.compareTo(other.secondRank);
				if (result == 0)
					result = kickerRank.compareTo(other.kickerRank);
			}
			return result;
		} else
			return GenericHandRank.TWO_PAIR.compareTo(o.getGenericHandRank());

	}

}
